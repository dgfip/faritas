/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.component.SegmenterUnSejour;
import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import fr.gouv.finances.faritas_os.dto.AnomalieValideEnum;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.dto.SejourDeclare;
import jakarta.ws.rs.core.Response;

@ExtendWith(MockitoExtension.class)
class SejourControllerTest
{
    @Mock
    SegmenterUnSejour segmenterUnSejour;

    @InjectMocks
    SejourController sejourController;

    @Test
    void testControleSejour()
    {
        final AnomalieDto ano1 = AnomalieDto.creerAnomalie(AnomalieValideEnum.RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE);
        final AnomalieDto ano2 = AnomalieDto.creerAnomalie(AnomalieValideEnum.TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE);

        final SejourAnalyse sejourAnalyse = SejourAnalyse.builder()
            .dateDebutSejour(LocalDate.of(2024, 2, 5))
            .dateFinSejour(LocalDate.of(2024, 2, 10))
            .datePerception(LocalDate.of(2024, 2, 5))
            .adresse("38 impasse Picasso")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .nombreNuitees(15)
            .nombreNuiteesAssujetties(10)
            .prixParNuitee(30.0)
            .collectiviteDeliberanteSiren("244400503")
            .collectiviteDeliberanteAnnee("2024")
            .collectiviteDeliberanteNom("CC ERDRE ET GESVRES")
            .collectiviteDeliberanteCodePostal("44119")
            .listDAnomalies(Arrays.asList(ano1, ano2))
            .regime("Réel")
            .sirenRattachement("244400503")
            .montantTaxeDeSejourCalculee(114.08)
            .build();

        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .adresse("25 route de Carquefou")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .categorieLogement(12)
            .natureLogement(5)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(100.0)
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .build();

        when(segmenterUnSejour.traitementSejourDeclare(sejour)).thenReturn(sejourAnalyse);

        final Response rep = sejourController.controleSejour(sejour);

        assertNotNull(rep);
        assertEquals(200, rep.getStatus());
        assertInstanceOf(SejourAnalyse.class, rep.getEntity());
        final SejourAnalyse sejourRep = (SejourAnalyse) rep.getEntity();
        assertEquals(114.08, sejourRep.getMontantTaxeDeSejourCalculee());
    }

    @Test
    void testControleListeSejours()
    {
        final AnomalieDto ano1 = AnomalieDto.creerAnomalie(AnomalieValideEnum.RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE);
        final AnomalieDto ano2 = AnomalieDto.creerAnomalie(AnomalieValideEnum.TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE);

        final SejourAnalyse sejourAnalyse = SejourAnalyse.builder()
            .dateDebutSejour(LocalDate.of(2024, 2, 5))
            .dateFinSejour(LocalDate.of(2024, 2, 10))
            .datePerception(LocalDate.of(2024, 2, 5))
            .adresse("38 impasse Picasso")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .nombreNuitees(15)
            .nombreNuiteesAssujetties(10)
            .prixParNuitee(30.0)
            .collectiviteDeliberanteSiren("244400503")
            .collectiviteDeliberanteAnnee("2024")
            .collectiviteDeliberanteNom("CC ERDRE ET GESVRES")
            .collectiviteDeliberanteCodePostal("44119")
            .listDAnomalies(Arrays.asList(ano1, ano2))
            .regime("Réel")
            .sirenRattachement("244400503")
            .montantTaxeDeSejourCalculee(114.08)
            .build();

        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .adresse("25 route de Carquefou")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .categorieLogement(12)
            .natureLogement(5)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(100.0)
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .build();

        when(segmenterUnSejour.traitementSejourDeclare(sejour)).thenReturn(sejourAnalyse);

        final Response rep = sejourController.controleSejours(Arrays.asList(sejour));

        assertNotNull(rep);
        assertEquals(200, rep.getStatus());
        assertInstanceOf(List.class, rep.getEntity());
        @SuppressWarnings("unchecked")
        final List<SejourAnalyse> listeRep = (List<SejourAnalyse>) rep.getEntity();
        assertEquals(114.08, listeRep.get(0).getMontantTaxeDeSejourCalculee());
    }
}
