/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.component.ChargementOcsitan;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@ExtendWith(MockitoExtension.class)
class OcsitanControllerTest
{
    @Mock
    private ChargementOcsitan chargementOcsitan;

    @InjectMocks
    OcsitanController ocsitanController;

    @Test
    void testCherchementOcsitanOK() throws IOException
    {
        final InputStream isOcsitan = getClass().getClassLoader().getResourceAsStream("ocsitanfile.xml");
        final FormDataBodyPart bodyPart = new FormDataBodyPart("file", "ocsitanfile", MediaType.APPLICATION_XML_TYPE);
        bodyPart.setFormDataContentDisposition(FormDataContentDisposition
            .name("ocsitanDetails")
            .fileName("ocsitan.xml")
            .build());

        final Response rep = ocsitanController.remplirOcsitan("2022", isOcsitan, bodyPart.getFormDataContentDisposition());

        assertThat(rep).isNotNull();
        assertThat(rep.getStatus()).isEqualTo(Status.ACCEPTED.getStatusCode());
        assertThat(rep.getEntity()).isNotNull();
        final String resultat = (String) rep.getEntity();
        assertThat(resultat).isNotEmpty().isEqualTo("Chargement du fichier OCSITAN pour l'année 2022");
    }

    @Test
    void testCherchementOcsitanVide() throws IOException
    {
        final InputStream isVide = InputStream.nullInputStream();
        final FormDataBodyPart bodyPart = new FormDataBodyPart("file", "ocsitanfile", MediaType.APPLICATION_XML_TYPE);
        bodyPart.setFormDataContentDisposition(FormDataContentDisposition
            .name("ocsitanDetails")
            .fileName("ocsitanfile.xml")
            .size(0)
            .build());

        final Response rep = ocsitanController.remplirOcsitan("2022", isVide, bodyPart.getFormDataContentDisposition());

        assertThat(rep).isNotNull();
        assertThat(rep.getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
        assertThat(rep.getEntity()).isNotNull();
        final String resultat = (String) rep.getEntity();
        assertThat(resultat).isNotEmpty().isEqualTo("Le fichier OCSITAN est vide");
    }

    @Test
    void chargementOcsitanInternalServerError() throws IOException, ReadOcsitanException
    {
        // arrange
        final InputStream isOcsitan = getClass().getClassLoader().getResourceAsStream("ocsitanfile.xml");
        final FormDataBodyPart bodyPart = new FormDataBodyPart("file", "ocsitanfile", MediaType.APPLICATION_XML_TYPE);
        bodyPart.setFormDataContentDisposition(FormDataContentDisposition.name("ocsitanDetails").fileName("ocsitan.xml").build());

        Mockito.doThrow(new ReadOcsitanException("Erreur lors du traitement du fichier OCSITAN de l'année 2022"))
            .when(chargementOcsitan).chargerDonneesOcsitan(Mockito.anyString(), Mockito.any(Path.class));

        // when
        final Response response = ocsitanController.remplirOcsitan("2022", isOcsitan, bodyPart.getFormDataContentDisposition());
        // then assert
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
        assertThat(response.getEntity()).isEqualTo("Erreur lors du chargement du fichier OCSITAN.");
    }

}
