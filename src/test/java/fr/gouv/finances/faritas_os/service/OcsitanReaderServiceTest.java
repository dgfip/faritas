/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.dto.RegimeTaxation;
import fr.gouv.finances.faritas_os.entity.ocsitan.Periode;
import fr.gouv.finances.faritas_os.entity.ocsitan.Tarif;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import fr.gouv.finances.faritas_os.service.impl.OcsitanReaderServiceImpl;

class OcsitanReaderServiceTest
{
    private OcsitanReaderService ocsitanReaderService;

    String path = "ocsitanfile.xml";

    @BeforeEach()
    public void beforeEach()
    {
        ocsitanReaderService = new OcsitanReaderServiceImpl();
    }

    @Test
    void recupererDeliberation200029999() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());

        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> reelRegimesTaxation = Collections.nCopies(10, RegimeTaxation.REGIME_REEL.getLabel());
        final List<String> regimesTaxation = new ArrayList<>(reelRegimesTaxation);
        regimesTaxation.add("Non rattachée à un tarif");

        // Assert
        assertThat(listeDeCollectivitesDeliberantes).hasSize(25);

        final List<PeriodeTarifaireDto> periodeTarifaireList = listeDeCollectivitesDeliberantes
            .stream()
            .map(CollectiviteDeliberanteDto::getListePeriodesTarifaires)
            .collect(Collectors.toList())
            .stream()
            .flatMap(List::stream)
            .collect(Collectors.toList());

        assertThat(periodeTarifaireList).hasSize(47);

        final CollectiviteDeliberanteDto collectiviteDeliberante200029999 = listeDeCollectivitesDeliberantes.get(0);
        assertThat(collectiviteDeliberante200029999.getSiren()).isEqualTo("200029999");
        assertThat(collectiviteDeliberante200029999.getNom()).isEqualTo("CC RIVES AIN PAYS CERDON");
        assertThat(collectiviteDeliberante200029999.getCodePostal()).isEqualTo("01640");
        assertThat(collectiviteDeliberante200029999.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(collectiviteDeliberante200029999.getListePeriodesTarifaires().get(0).getTarifPourcentageCategorie19()).isEqualTo(4);
        assertThat(collectiviteDeliberante200029999.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).containsExactly(
            4.00, 3.00, 0.91, 0.64, 0.45, 0.45, 0.36, 0.20);
        assertThat(collectiviteDeliberante200029999.getRegimesTaxation()).isEqualTo(regimesTaxation);
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites()).hasSize(14);

        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(0).getNom()).isEqualTo("SERRIERES-SUR-AIN");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(0).getSiren()).isEqualTo("210104048");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(0).isTaxeDepartementaleApplicable()).isTrue();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(0).isTaxeRegionaleApplicable()).isFalse();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(0).isTaxeGrandeVitesseApplicable()).isFalse();

        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(1).getNom()).isEqualTo("CHALLES LA MONTAGNE");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(1).getSiren()).isEqualTo("210100772");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(1).isTaxeDepartementaleApplicable()).isTrue();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(1).isTaxeRegionaleApplicable()).isFalse();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(1).isTaxeGrandeVitesseApplicable()).isFalse();

        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(2).getNom()).isEqualTo("NEUVILLE-SUR-AIN");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(2).getSiren()).isEqualTo("210102737");
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(2).isTaxeDepartementaleApplicable()).isTrue();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(2).isTaxeRegionaleApplicable()).isFalse();
        assertThat(collectiviteDeliberante200029999.getListeDeCollectivites().get(2).isTaxeGrandeVitesseApplicable()).isFalse();

    }

    @Test
    void recupererDeliberation211300041() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());

        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> reelRegimesTaxation = Collections.nCopies(10, RegimeTaxation.REGIME_REEL.getLabel());
        final List<String> regimesTaxation = new ArrayList<>(reelRegimesTaxation);
        regimesTaxation.add("Non rattachée à un tarif");
        final List<String> regimesTaxation2 = Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel());

        // Assert
        assertThat(listeDeCollectivitesDeliberantes).hasSize(25);

        final List<PeriodeTarifaireDto> periodeTarifaireList = listeDeCollectivitesDeliberantes
            .stream()
            .map(CollectiviteDeliberanteDto::getListePeriodesTarifaires)
            .collect(Collectors.toList())
            .stream()
            .flatMap(List::stream)
            .collect(Collectors.toList());

        assertThat(periodeTarifaireList).hasSize(47);

        final CollectiviteDeliberanteDto deliberante211300041 = listeDeCollectivitesDeliberantes.get(1);

        assertThat(deliberante211300041.getSiren()).isEqualTo("211300041");
        assertThat(deliberante211300041.getNom()).isEqualTo("ARLES");
        assertThat(deliberante211300041.getCodePostal()).isEqualTo("13200");
        assertThat(deliberante211300041.getRegimesTaxation()).isEqualTo(regimesTaxation2);
        assertThat(deliberante211300041.getListeDeCollectivites()).hasSize(1);
        assertThat(deliberante211300041.getListePeriodesTarifaires()).hasSize(1);
        assertThat(deliberante211300041.getListePeriodesTarifaires().get(0).getDateDebut())
            .isEqualTo(LocalDate.of(2023, 1, 1));
        assertThat(deliberante211300041.getListePeriodesTarifaires().get(0).getDateFin())
            .isEqualTo(LocalDate.of(2023, 12, 31));
        assertThat(deliberante211300041.getListePeriodesTarifaires().get(0).getTarifPourcentageCategorie19())
            .isEqualTo(2.0);
        assertThat(deliberante211300041.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(deliberante211300041.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).containsExactly(
            4.2, 3.0, 2.3, 1.0, 0.6, 0.5, 0.6, 0.2);
    }

    @Test
    void recupererDeliberation215501248() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());

        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> reelRegimesTaxation = Collections.nCopies(10, RegimeTaxation.REGIME_REEL.getLabel());
        final List<String> regimesTaxation = new ArrayList<>(reelRegimesTaxation);
        regimesTaxation.add("Non rattachée à un tarif");
        final List<String> regimesTaxation2 = Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel());

        // Assert
        assertThat(listeDeCollectivitesDeliberantes).hasSize(25);

        final List<PeriodeTarifaireDto> periodeTarifaireList = listeDeCollectivitesDeliberantes
            .stream()
            .map(CollectiviteDeliberanteDto::getListePeriodesTarifaires)
            .collect(Collectors.toList())
            .stream()
            .flatMap(List::stream)
            .collect(Collectors.toList());

        assertThat(periodeTarifaireList).hasSize(47);

        assertThat(listeDeCollectivitesDeliberantes.get(2).getSiren()).isEqualTo("215501248");
        assertThat(listeDeCollectivitesDeliberantes.get(2).getNom()).isEqualTo("CONSENVOYE");
        assertThat(listeDeCollectivitesDeliberantes.get(2).getCodePostal()).isEqualTo("55110");
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListePeriodesTarifaires().get(0).getDateDebut())
            .isEqualTo(LocalDate.of(2023, 1, 1));
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListePeriodesTarifaires().get(0).getDateFin())
            .isEqualTo(LocalDate.of(2023, 12, 31));
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListePeriodesTarifaires().get(0).getTarifPourcentageCategorie19())
            .isEqualTo(3.49);
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).containsExactly(
            2.65, 1.85, 1.36, 0.95, 0.69, 0.50, 0.45, 0.20);
        assertThat(listeDeCollectivitesDeliberantes.get(2).getRegimesTaxation()).isEqualTo(regimesTaxation2);
        assertThat(listeDeCollectivitesDeliberantes.get(2).getListeDeCollectivites()).hasSize(1);

        assertThat(listeDeCollectivitesDeliberantes.get(3).getSiren()).isEqualTo("240200501");
        assertThat(listeDeCollectivitesDeliberantes.get(3).getNom()).isEqualTo("CC DU VAL DE L'AISNE");
        assertThat(listeDeCollectivitesDeliberantes.get(3).getCodePostal()).isEqualTo("02370");
        assertThat(listeDeCollectivitesDeliberantes.get(3).getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(listeDeCollectivitesDeliberantes.get(3).getListePeriodesTarifaires().get(0).getTarifPourcentageCategorie19())
            .isEqualTo(5.0);
        assertThat(listeDeCollectivitesDeliberantes.get(3).getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).containsExactly(
            0.00, 0.00, 1.30, 0.70, 0.65, 0.60, 0.50, 0.20);
        assertThat(listeDeCollectivitesDeliberantes.get(3).getRegimesTaxation()).isEqualTo(regimesTaxation2);
        assertThat(listeDeCollectivitesDeliberantes.get(3).getListeDeCollectivites()).hasSize(58);
    }

    @Test
    void recupererDeliberation240200501() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());

        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> reelRegimesTaxation = Collections.nCopies(10, RegimeTaxation.REGIME_REEL.getLabel());
        final List<String> regimesTaxation = new ArrayList<>(reelRegimesTaxation);
        regimesTaxation.add("Non rattachée à un tarif");
        final List<String> regimesTaxation2 = Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel());

        final CollectiviteDeliberanteDto deliberante240200501 = listeDeCollectivitesDeliberantes.get(3);
        // Assert
        assertThat(deliberante240200501.getSiren()).isEqualTo("240200501");
        assertThat(deliberante240200501.getNom()).isEqualTo("CC DU VAL DE L'AISNE");
        assertThat(deliberante240200501.getCodePostal()).isEqualTo("02370");
        assertThat(deliberante240200501.getRegimesTaxation()).isEqualTo(regimesTaxation2);
        assertThat(deliberante240200501.getListeDeCollectivites()).hasSize(58);
        assertThat(deliberante240200501.getListePeriodesTarifaires()).hasSize(1);
        assertThat(deliberante240200501.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(deliberante240200501.getListePeriodesTarifaires().get(0).getTarifPourcentageCategorie19())
            .isEqualTo(5.0);
        assertThat(deliberante240200501.getListePeriodesTarifaires().get(0).getTarifsFixesCategories()).containsExactly(
            0.00, 0.00, 1.30, 0.70, 0.65, 0.60, 0.50, 0.20);
    }

    @Test
    void recupererCollectivitesDeliberantesPeriodesInversees() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());

        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> reelRegimesTaxation = Collections.nCopies(10, RegimeTaxation.REGIME_REEL.getLabel());
        final List<String> regimesTaxation = new ArrayList<>(reelRegimesTaxation);
        regimesTaxation.add("Non rattachée à un tarif");

        // Assert
        assertThat(listeDeCollectivitesDeliberantes).hasSize(25);

        final List<PeriodeTarifaireDto> periodeTarifaireList = listeDeCollectivitesDeliberantes
            .stream()
            .map(CollectiviteDeliberanteDto::getListePeriodesTarifaires)
            .collect(Collectors.toList())
            .stream()
            .flatMap(List::stream)
            .collect(Collectors.toList());

        assertThat(periodeTarifaireList).hasSize(47);
        final CollectiviteDeliberanteDto collectiviteDeliberante245400171 = listeDeCollectivitesDeliberantes.get(21);
        assertThat(collectiviteDeliberante245400171.getSiren()).isEqualTo("245400171");
        assertThat(collectiviteDeliberante245400171.getNom()).isEqualTo("CC MOSELLE ET MADON");
        assertThat(collectiviteDeliberante245400171.getCodePostal()).isEqualTo("54230");
        assertThat(collectiviteDeliberante245400171.getListeDeCollectivites()).hasSize(19);

        final List<PeriodeTarifaireDto> listePeriodesTarifaires = collectiviteDeliberante245400171.getListePeriodesTarifaires();
        assertThat(listePeriodesTarifaires).hasSize(3);
        assertThat(listePeriodesTarifaires.stream()
            .allMatch(periodeTarifaire -> periodeTarifaire.getDateDebut().isBefore(periodeTarifaire.getDateFin()))).isTrue();

        assertThat(listePeriodesTarifaires.get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(listePeriodesTarifaires.get(0).getTarifPourcentageCategorie19()).isEqualTo(4.19);
        assertThat(listePeriodesTarifaires.get(0).getTarifsFixesCategories()).containsExactly(
            3.11, 1.22, 1.31, 1.41, 2.51, 2.61, 3.71, 4.18);

        final CollectiviteDeliberanteDto collectiviteDeliberante242504116 = listeDeCollectivitesDeliberantes.get(20);
        assertThat(collectiviteDeliberante242504116.getSiren()).isEqualTo("242504116");
        assertThat(collectiviteDeliberante242504116.getNom()).isEqualTo("CC VAL DE MORTEAU");
        assertThat(collectiviteDeliberante242504116.getCodePostal()).isEqualTo("25503");
        final List<PeriodeTarifaireDto> listePeriodesTarifaires242504116 = collectiviteDeliberante242504116.getListePeriodesTarifaires();

        assertThat(listePeriodesTarifaires242504116).hasSize(2);
        assertThat(listePeriodesTarifaires242504116.stream()
            .allMatch(periodeTarifaire -> periodeTarifaire.getDateDebut().isBefore(periodeTarifaire.getDateFin()))).isTrue();

        assertThat(listePeriodesTarifaires242504116.get(0).getTarifsFixesCategories()).hasSize(8);
        assertThat(listePeriodesTarifaires242504116.get(0).getTarifPourcentageCategorie19()).isEqualTo(3.0);
        assertThat(listePeriodesTarifaires242504116.get(0).getTarifsFixesCategories()).containsExactly(
            2.2, 1.65, 1.3, 1.05, 0.95, 0.8, 0.55, 0.2);
    }

    @Test
    void recupererDeliberanteAvecAucuneCollectivite() throws Exception
    {
        final File ocsitan = new File(getClass().getClassLoader().getResource(path).getPath());
        // Act
        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes =
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", new FileInputStream(ocsitan));

        final List<String> regimesTaxation = Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel());

        /*
         * si déliberante ne contient aucune collectivité, on ajoute une nouvelle collectivité avec les mêmes
         * informations de la délibérante
         */
        assertThat(listeDeCollectivitesDeliberantes.get(1).getSiren()).isEqualTo("211300041");
        assertThat(listeDeCollectivitesDeliberantes.get(1).getNom()).isEqualTo("ARLES");
        assertThat(listeDeCollectivitesDeliberantes.get(1).getCodePostal()).isEqualTo("13200");
        assertThat(listeDeCollectivitesDeliberantes.get(1).getRegimesTaxation()).isEqualTo(regimesTaxation);
        assertThat(listeDeCollectivitesDeliberantes.get(1).getListeDeCollectivites()).hasSize(1);
        assertThat(listeDeCollectivitesDeliberantes.get(1).getListeDeCollectivites().get(0).getNom()).isEqualTo("ARLES");
        assertThat(listeDeCollectivitesDeliberantes.get(1).getListeDeCollectivites().get(0).getSiren()).isEqualTo("211300041");
        assertThat(listeDeCollectivitesDeliberantes.get(1).getListeDeCollectivites().get(0).getCodePostal()).isEqualTo("13200");
        assertThat(listeDeCollectivitesDeliberantes.get(0).getListeDeCollectivites().get(0).isTaxeDepartementaleApplicable()).isTrue();
        assertThat(listeDeCollectivitesDeliberantes.get(0).getListeDeCollectivites().get(0).isTaxeRegionaleApplicable()).isFalse();
        assertThat(listeDeCollectivitesDeliberantes.get(0).getListeDeCollectivites().get(0).isTaxeGrandeVitesseApplicable()).isFalse();
    }

    @Test
    void recupererCollectivitesDeliberantesTestException()
    {

        try
        {
            ocsitanReaderService.recupererCollectivitesDeliberantes("2023", Mockito.mock(InputStream.class));
        }
        catch (final ReadOcsitanException e)
        {
            assertThat(e.getMessage()).isEqualTo("Erreur lors du traitement du fichier OCSITAN de l'année 2023");
        }
    }

    @Test
    void convertTarifsFrom2018Test_cas1()
    {

        // les données de la catégorie 7 et 8 deviennt catégorie 19 avec une valeur fixe ( 0.0 )
        final Periode periode = new Periode();
        final List<Tarif> tarifs = getListTarifs2018();

        periode.setTarifs(tarifs);

        assertThat(tarifs).hasSize(10);

        final List<Tarif> tarifsConvertis = ocsitanReaderService.convertTarifsFrom2018(periode.getTarifs());

        assertThat(tarifsConvertis).hasSize(10);

        final Tarif tarifCat11 = tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_11)
            .findFirst()
            .get();
        assertObjectTarif(tarifCat11, Constantes.MILLESIME_2019_CATEGORIE_11, 4.0);

        final Tarif tarifCat12 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_12).findFirst().get();
        assertObjectTarif(tarifCat12, Constantes.MILLESIME_2019_CATEGORIE_12, 3.00);

        final Tarif tarifCat13 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_13).findFirst().get();
        assertObjectTarif(tarifCat13, Constantes.MILLESIME_2019_CATEGORIE_13, 2.30);

        final Tarif tarifCat14 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_14).findFirst().get();
        assertObjectTarif(tarifCat14, Constantes.MILLESIME_2019_CATEGORIE_14, 1.00);

        final Tarif tarifCat15 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_15).findFirst().get();
        assertObjectTarif(tarifCat15, Constantes.MILLESIME_2019_CATEGORIE_15, 0.80);

        final Tarif tarifCat16 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_16).findFirst().get();
        assertObjectTarif(tarifCat16, Constantes.MILLESIME_2019_CATEGORIE_16, 0.70);

        final Tarif tarifCat17 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_17).findFirst().get();
        assertObjectTarif(tarifCat17, Constantes.MILLESIME_2019_CATEGORIE_17, 0.60);

        final Tarif tarifCat18 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_18).findFirst().get();
        assertObjectTarif(tarifCat18, Constantes.MILLESIME_2019_CATEGORIE_18, 0.20);

        final Tarif tarifCat19 =
            tarifsConvertis.stream().filter(tarif -> tarif.getCategorieId() == Constantes.MILLESIME_2019_CATEGORIE_19).findFirst().get();
        assertObjectTarif(tarifCat19, Constantes.MILLESIME_2019_CATEGORIE_19, 0.0);

    }

    @Test
    void testConvertTarifsFrom2018Null()
    {
        final List<Tarif> tarifsConvertis = ocsitanReaderService.convertTarifsFrom2018(new ArrayList<>());

        assertThat(tarifsConvertis).isEmpty();
    }

    private static List<Tarif> getListTarifs2018()
    {
        final List<Tarif> tarifs = new ArrayList<>();
        final Tarif tarifCategorie_1 = new Tarif();
        tarifCategorie_1.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_1);
        tarifCategorie_1.setValue(4.00);

        final Tarif tarifCategorie_2 = new Tarif();
        tarifCategorie_2.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_2);
        tarifCategorie_2.setValue(3.00);

        final Tarif tarifCategorie_3 = new Tarif();
        tarifCategorie_3.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_3);
        tarifCategorie_3.setValue(2.30);

        final Tarif tarifCategorie_4 = new Tarif();
        tarifCategorie_4.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_4);
        tarifCategorie_4.setValue(1.00);

        final Tarif tarifCategorie_5 = new Tarif();
        tarifCategorie_5.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_5);
        tarifCategorie_5.setValue(0.80);

        final Tarif tarifCategorie_6 = new Tarif();
        tarifCategorie_6.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_6);
        tarifCategorie_6.setValue(0.70);

        final Tarif tarifCategorie_7 = new Tarif();
        tarifCategorie_7.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_7);
        tarifCategorie_7.setValue(0.0);

        final Tarif tarifCategorie_8 = new Tarif();
        tarifCategorie_8.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_8);
        tarifCategorie_8.setValue(0.0);

        final Tarif tarifCategorie_9 = new Tarif();
        tarifCategorie_9.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_9);
        tarifCategorie_9.setValue(0.60);

        final Tarif tarifCategorie_10 = new Tarif();
        tarifCategorie_10.setCategorieId(Constantes.MILLESIME_2018_CATEGORIE_10);
        tarifCategorie_10.setValue(0.20);

        tarifs.add(tarifCategorie_1);
        tarifs.add(tarifCategorie_2);
        tarifs.add(tarifCategorie_3);
        tarifs.add(tarifCategorie_4);
        tarifs.add(tarifCategorie_5);
        tarifs.add(tarifCategorie_6);
        tarifs.add(tarifCategorie_7);
        tarifs.add(tarifCategorie_8);
        tarifs.add(tarifCategorie_9);
        tarifs.add(tarifCategorie_10);

        return tarifs;
    }

    private void assertObjectTarif(Tarif result, int expectedCategorieId, double expectedValue)
    {
        assertThat(result).isNotNull();
        assertThat(result.getCategorieId()).isEqualTo(expectedCategorieId);
        assertThat(result.getValue()).isEqualTo(expectedValue);
    }
}
