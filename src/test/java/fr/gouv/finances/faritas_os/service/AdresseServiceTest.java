/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClientResponseException.NotFound;

import fr.gouv.finances.faritas_os.client.APIAdresseFeature;
import fr.gouv.finances.faritas_os.client.APIAdresseProperties;
import fr.gouv.finances.faritas_os.client.APIAdresseResponse;
import fr.gouv.finances.faritas_os.client.ApiAdresseHttpClient;
import fr.gouv.finances.faritas_os.service.impl.AdresseServiceImpl;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class AdresseServiceTest
{
    @Mock
    private ApiAdresseHttpClient apiAdresseHttpClient;

    @InjectMocks
    private AdresseServiceImpl adresseService;

    /**
     * Création d'un élément de réponse de l'API adresse
     *
     * @param label Label
     * @param score Score
     * @param id Identifiant
     * @param type Type
     * @param name Nom
     * @param postcode Code postal
     * @param citycode Code INSEE
     * @param x X
     * @param y Y
     * @param population Population
     * @param city Ville
     * @param context Contexte
     * @param importance Importance
     * @return Element de réponse
     */
    APIAdresseFeature createApiAdresseFeature(String label, double score, String id, String type, String name, String postcode,
        String citycode, double x, double y, int population, String city, String context, double importance)
    {
        final APIAdresseFeature feature = new APIAdresseFeature();

        final APIAdresseProperties prop = new APIAdresseProperties();
        prop.setLabel(label);
        prop.setScore(score);
        prop.setId(id);
        prop.setType(type);
        prop.setName(name);
        prop.setPostcode(postcode);
        prop.setCitycode(citycode);
        prop.setX(x);
        prop.setY(y);
        prop.setPopulation(population);
        prop.setCity(city);
        prop.setContext(context);
        prop.setImportance(importance);
        feature.setProperties(prop);

        return feature;
    }

    @Test
    @DisplayName("Quand on reçoit plusieurs réponses de l'API adresse, doit retourner la ville information avec la plus grosse population")
    void testAvecVilleCodePostalPlusieursReponseVillePlusGrossePopulation()
    {
        // Arrange
        final APIAdresseResponse repAdresse = new APIAdresseResponse();
        final APIAdresseFeature feature1 =
            createApiAdresseFeature("Label", 0.4, "id", "type", "name", "75000", "75056", 3.34, 3.14, 10000, "Paris", "context", 2.1);
        final APIAdresseFeature feature2 =
            createApiAdresseFeature("Label2", 0.4, "id2", "type2", "name2", "75005", "75005", 3.24, 3.04, 5000,
                "Pas Paris", "context", 2.1);
        repAdresse.setFeatures(Arrays.asList(feature2, feature1));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 10ème", "75010")).thenReturn(Mono.just(repAdresse));

        // Act
        final String codeInsee = adresseService.recupererCodeInsee("Paris 10ème", "75010").block();
        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("Si le codeInsee correspond au code postal de la recherche, doit retourner la ville information")
    void quand_ville_et_code_postal_ne_suffisent_pas_recherche_par_query_et_retourne_ville_si_code_insee_correspondant()
    {
        // Arrange
        // Adresse vide
        final APIAdresseResponse repAdressePourVilleCodePostal = new APIAdresseResponse();
        repAdressePourVilleCodePostal.setFeatures(new ArrayList<>());

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        final APIAdresseFeature feature = createApiAdresseFeature("Label", 0.87, "id", "type", "name", "75002", "75056", 3.34, 3.14,
            10000, "Paris", "context", 2.1);
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75002"))
            .thenReturn(Mono.just(repAdressePourVilleCodePostal));
        when(apiAdresseHttpClient.searchByQuery("75002 Paris 2ème")).thenReturn(Mono.just(repAdressePourQuery));

        // Act
        final String codeInsee = adresseService.recupererCodeInsee("Paris 2ème", "75002").block();

        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("si le code Insee ne correspond pas, que le score est assez bon et que le résultat est dans le même département, doit retourner la ville information")
    void quand_ville_et_code_postal_ne_suffisent_pas_recherche_par_query_et_retourne_ville_si_bon_score_et_meme_departement()
    {
        // Arrange
        final APIAdresseResponse repAdressePourVilleCodePostal = new APIAdresseResponse();
        repAdressePourVilleCodePostal.setFeatures(new ArrayList<>());

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        final APIAdresseFeature feature = createApiAdresseFeature("Label", 0.54, "id", "type", "name", "75002", "75056", 3.34, 3.14,
            10000, "Paris", "context", 2.1);
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75125"))
            .thenReturn(Mono.just(repAdressePourVilleCodePostal));
        when(apiAdresseHttpClient.searchByQuery("75125 Paris 2ème")).thenReturn(Mono.just(repAdressePourQuery));

        // Act
        final String codeInsee = adresseService.recupererCodeInsee("Paris 2ème", "75125").block();

        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("Quand la recherche n'a jamais abouti, doit renvoyer des informations de ville par défaut")
    void quand_recherche_aboutit_pas_doit_renvoyer_donnees_par_defaut()
    {
        // Arrange
        final APIAdresseResponse repAdresse = new APIAdresseResponse();
        repAdresse.setFeatures(new ArrayList<>());

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        final APIAdresseFeature feature =
            createApiAdresseFeature("Label", 0.54, "id", "type", "name", "80013", "75056", 3.34, 3.14, 10000, "Paris", "context", 2.1);
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75002")).thenReturn(Mono.just(repAdresse));
        when(apiAdresseHttpClient.searchByQuery("75002 Paris 2ème")).thenReturn(Mono.just(repAdressePourQuery));

        // Act
        final String codeInsee = adresseService.recupererCodeInsee("Paris 2ème", "75002").block();

        // Assert
        final InOrder inOrder = inOrder(apiAdresseHttpClient);

        inOrder.verify(apiAdresseHttpClient, times(1)).searchByCityAndPostCode("Paris 2ème", "75002");
        inOrder.verify(apiAdresseHttpClient, times(1)).searchByQuery("75002 Paris 2ème");

        assertEquals("", codeInsee);
    }

    @Test
    @DisplayName("Quand la recherche par ville et code postal suffit, doit retourner la ville information")
    void testAvecAdresseVilleCodePostalVilleSuffisante()
    {
        // Arrange
        final APIAdresseResponse jsonMapAPIAdresse = new APIAdresseResponse();
        final APIAdresseFeature feature1 =
            createApiAdresseFeature("Label", 0.87, "id", "type", "name", "75002", "75056", 3.34, 3.14, 10000, "Paris", "context", 2.1);
        final APIAdresseFeature feature2 = createApiAdresseFeature("Label2", 0.44, "id2", "type2", "name2", "75005", "75005", 3.24, 3.04,
            5000, "Pas Paris", "context", 2.1);

        jsonMapAPIAdresse.setFeatures(Arrays.asList(feature1, feature2));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75002")).thenReturn(Mono.just(jsonMapAPIAdresse));

        // Act
        final String codeInsee = adresseService.recupererCodeInsee("Paris 2ème", "75002", "34 Avenue de l'opéra").block();

        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("Si le codeInsee correspond au code postal de la recherche, doit retourner la ville information")
    void testRechercheQueryRetourneVillePourCodeInsee()
    {
        // Arrange
        final APIAdresseResponse repAdresseVide = new APIAdresseResponse();
        repAdresseVide.setFeatures(new ArrayList<>());

        final APIAdresseFeature feature = createApiAdresseFeature("Label", 0.87, "id", "type", "name", "75002", "75056", 3.34,
            3.14, 10000, "Paris", "context", 2.1);

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75056")).thenReturn(Mono.just(repAdressePourQuery));

        // Act
        final String codeInsee =
            adresseService.recupererCodeInsee("Paris 2ème", "75056", "34 avenue de l'opéra").block();

        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("si le code Insee ne correspond pas, que le score est assez bon et que le résultat est dans le même " +
        "département, doit retourner la ville information")
    void testRechercheQueryRetourneVilleScoreOKEtDDepartement()
    {
        // Arrange
        final APIAdresseFeature feature = createApiAdresseFeature("Label", 0.54, "id", "type", "name", "75002", "75056", 3.34,
            3.14, 10000, "Paris", "context", 2.1);

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75125")).thenReturn(Mono.just(repAdressePourQuery));

        // Act
        final String codeInsee =
            adresseService.recupererCodeInsee("Paris 2ème", "75125", "34 avenue de l'opéra").block();

        // Assert
        assertEquals("75056", codeInsee);
    }

    @Test
    @DisplayName("Quand la recherche n'a jamais abouti, doit renvoyer des informations de ville par défaut")
    void testEnchainementRecherche()
    {
        // Arrange
        final APIAdresseResponse repAPIAdresseVide = new APIAdresseResponse();
        repAPIAdresseVide.setFeatures(new ArrayList<>());

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75002")).thenReturn(Mono.just(repAPIAdresseVide));

        // Act
        final String codeInsee =
            adresseService.recupererCodeInsee("Paris 2ème", "75002", "34 Avenue de l'opéra").block();

        // Assert
        final InOrder inOrder = inOrder(apiAdresseHttpClient);

        inOrder.verify(apiAdresseHttpClient, times(1)).searchByCityAndPostCode("Paris 2ème", "75002");

        assertEquals("", codeInsee);
    }

    @Test
    @DisplayName("Quand la recherche n'a jamais abouti, doit renvoyer des informations de ville par défaut")
    void testRechercheEnErreur()
    {
        // Arrange
        final APIAdresseResponse repAdresse = new APIAdresseResponse();
        repAdresse.setFeatures(new ArrayList<>());

        final APIAdresseResponse repAdressePourQuery = new APIAdresseResponse();
        final APIAdresseFeature feature =
            createApiAdresseFeature("Label", 0.54, "id", "type", "name", "80013", "75056", 3.34, 3.14, 10000, "Paris", "context", 2.1);
        repAdressePourQuery.setFeatures(Arrays.asList(feature));

        when(apiAdresseHttpClient.searchByCityAndPostCode("Paris 2ème", "75002")).thenThrow(NotFound.class);

        // Act
        String codeInsee = null;
        try
        {
            codeInsee = adresseService.recupererCodeInsee("Paris 2ème", "75002").block();
        }
        catch (final Exception e)
        {
            // Assert
            final InOrder inOrder = inOrder(apiAdresseHttpClient);
            inOrder.verify(apiAdresseHttpClient, times(1)).searchByCityAndPostCode("Paris 2ème", "75002");

            assertNull(codeInsee);
            assertNotNull(e);
            assertInstanceOf(NotFound.class, e);
        }
    }
}
