/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.dto.RegimeTaxation;
import fr.gouv.finances.faritas_os.entity.Collectivite;
import fr.gouv.finances.faritas_os.entity.CollectiviteDeliberante;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.exception.CollectiviteNonTrouveeException;
import fr.gouv.finances.faritas_os.repository.CollectiviteRepository;
import fr.gouv.finances.faritas_os.service.impl.CollectiviteServiceImpl;

@ExtendWith(MockitoExtension.class)
class CollectiviteServiceTest
{
    private static final String SIREN_NANTES = "244400404";

    private static final String CODE_POSTAL_NANTES = "44000";

    private static final String SIREN_BRAINS = "214400244";

    private static final String CODE_INSEE_BRAINS = "44024";

    private static final String CODE_POSTAL_BRAINS = "44830";

    private static final String MILLESIME_2022 = "2022";

    @Mock
    private CollectiviteRepository collectiviteRepository;

    @InjectMocks
    private CollectiviteServiceImpl collectiviteService;

    @Test
    @DisplayName("Vérifier que listerParAnneeEtCodePostal renvoie une liste de collectivités.")
    void listerParAnneeEtCodePostalOk()
    {
        // Arrange
        final CollectiviteDeliberante deliberante = creationCollectiviteDeleberante();
        final Collectivite collectiviteModel = creationCollectiviteModel(deliberante);
        final List<Collectivite> liste = new ArrayList<>();
        liste.add(collectiviteModel);

        when(collectiviteRepository.findAllByAnneeAndCodePostalOrderByNom(MILLESIME_2022, CODE_POSTAL_BRAINS)).thenReturn(liste);

        // Act
        final List<CollectiviteDto> collectivites = collectiviteService.listerParAnneeEtCodePostal(MILLESIME_2022, CODE_POSTAL_BRAINS);

        // Assert
        assertNotNull(collectivites);
        assertNotNull(collectivites.get(0));
        assertEquals(SIREN_BRAINS, collectivites.get(0).getSiren());
    }

    @Test
    @DisplayName("Vérifier que listerParAnneeEtCodePostal renvoie une liste vide.")
    void listerParAnneeEtCodePostalVide()
    {
        // Arrange
        final CollectiviteDeliberante deliberante = creationCollectiviteDeleberante();
        final Collectivite collectiviteModel = creationCollectiviteModel(deliberante);
        final List<Collectivite> liste = new ArrayList<>();
        liste.add(collectiviteModel);

        when(collectiviteRepository.findAllByAnneeAndCodePostalOrderByNom(MILLESIME_2022, CODE_POSTAL_BRAINS))
            .thenReturn(new ArrayList<>());

        // Act
        final List<CollectiviteDto> collectivites = collectiviteService.listerParAnneeEtCodePostal(MILLESIME_2022, CODE_POSTAL_BRAINS);

        // Assert
        assertNotNull(collectivites);
        assertEquals(0, collectivites.size());
    }

    @Test
    @DisplayName("Vérifier que trouverParAnneeEtCodeInsee renvoie une collectivité.")
    void testTrouverParAnneeEtCodeInseeOk() throws CollectiviteNonTrouveeException
    {
        final CollectiviteDeliberante deliberante = creationCollectiviteDeleberante();
        final Collectivite collectiviteModel = creationCollectiviteModel(deliberante);

        when(collectiviteRepository.findByAnneeAndCodeInsee(MILLESIME_2022, CODE_INSEE_BRAINS))
            .thenReturn(Optional.ofNullable(collectiviteModel));

        // Act
        final CollectiviteDto collectivite = collectiviteService.trouverParAnneeEtCodeInsee(MILLESIME_2022, CODE_INSEE_BRAINS);

        // Assert
        assertNotNull(collectivite);
        assertNotNull(collectivite.isTaxeGrandeVitesseApplicable());
        assertTrue(collectivite.isTaxeGrandeVitesseApplicable());
        assertEquals(SIREN_BRAINS, collectivite.getSiren());
        assertEquals(MILLESIME_2022, collectivite.getAnnee());
    }

    @Test
    @DisplayName("Verifier trouverParAnneeEtCodeInsee renvoie l'exception CollectiviteNonTrouveeException.")
    void testTrouverParAnneeEtCodeInseeAbsente()
    {
        // Arrange
        when(collectiviteRepository.findByAnneeAndCodeInsee(MILLESIME_2022, CODE_INSEE_BRAINS)).thenReturn(Optional.empty());

        // Assert
        assertThrows(CollectiviteNonTrouveeException.class,
            () -> collectiviteService.trouverParAnneeEtCodeInsee(MILLESIME_2022, CODE_INSEE_BRAINS));
    }

    @Test
    @DisplayName("Vérifier que getDeliberanteFromCommuneAndAnnee renvoie le SIREN de la collectivité délibérante.")
    void testGetDeliberanteFromSirenCommuneAndAnneeOk()
    {
        final CollectiviteDeliberanteDto deliberanteDto = creationCollectiviteDeliberanteDto();
        final CollectiviteDto collectiviteDto = creationCollectivite(deliberanteDto);

        final CollectiviteDeliberante deliberante = CollectiviteDeliberante.builder()
            .siren(SIREN_NANTES)
            .annee(MILLESIME_2022)
            .build();

        final Collectivite collectivite = Collectivite.builder()
            .siren(SIREN_BRAINS)
            .annee(MILLESIME_2022)
            .collectiviteDeliberante(deliberante)
            .build();

        final CompositePrimaryKey cpk = new CompositePrimaryKey(SIREN_BRAINS, MILLESIME_2022);

        when(collectiviteRepository.findById(cpk)).thenReturn(Optional.of(collectivite));

        // Act
        final String deliberanteSiren = collectiviteService.getDeliberanteFromSirenCommuneAndAnnee(collectiviteDto.getSiren(), 2022);

        // Assert
        assertEquals(SIREN_NANTES, deliberanteSiren);
    }

    @Test
    void testTrouverCollectiviteParSirenEtAnneeOk()
    {
        final CollectiviteDeliberante deliberante = creationCollectiviteDeleberante();
        final Collectivite collectiviteModel = creationCollectiviteModel(deliberante);
        when(collectiviteRepository.findBySirenAndAnnee(anyString(), anyString())).thenReturn(Optional.of(collectiviteModel));
        // when
        final CollectiviteDto collectivite = collectiviteService.trouverCollectiviteParSirenEtAnnee(SIREN_BRAINS, 2022);
        // then Assert
        assertNotNull(collectivite);
        assertNotNull(collectivite.isTaxeDepartementaleApplicable());
        assertFalse(collectivite.isTaxeDepartementaleApplicable());
        assertEquals(SIREN_BRAINS, collectivite.getSiren());
        assertEquals(MILLESIME_2022, collectivite.getAnnee());
        assertEquals(SIREN_NANTES, collectivite.getCollectiviteDeliberante().getSiren());
        assertEquals(MILLESIME_2022, collectivite.getCollectiviteDeliberante().getAnnee());
    }

    @Test
    void testTrouverCollectiviteParSirenEtAnneeKo()
    {
        when(collectiviteRepository.findBySirenAndAnnee(anyString(), anyString())).thenReturn(Optional.empty());
        assertNull(collectiviteService.trouverCollectiviteParSirenEtAnnee(SIREN_BRAINS, 2022));
    }

    private Collectivite creationCollectiviteModel(CollectiviteDeliberante deliberante)
    {
        final Collectivite collectiviteModel = Collectivite.builder()
            .siren(SIREN_BRAINS).annee(MILLESIME_2022)
            .nom("BRAINS")
            .codePostal(CODE_POSTAL_BRAINS)
            .codeInsee(CODE_INSEE_BRAINS)
            .taxeDepartementaleApplicable(false)
            .taxeRegionaleApplicable(false)
            .taxeGrandeVitesseApplicable(true)
            .collectiviteDeliberante(deliberante)
            .build();
        return collectiviteModel;
    }

    private CollectiviteDeliberante creationCollectiviteDeleberante()
    {
        final List<Double> tarifs = Arrays.asList(2.25, 2.25, 2.25, 1.3, 0.75, 0.65, 0.5, 0.2);
        final List<String> regimes = new ArrayList<>(Collections.nCopies(5, RegimeTaxation.REGIME_REEL.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_FORFAITAIRE.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_REEL.getLabel()));

        final PeriodeTarifaire periodeTarifaire = PeriodeTarifaire.builder()
            .siren(SIREN_NANTES)
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(String.join(";", tarifs.stream().map(Object::toString).collect(Collectors.toList())))
            .build();

        final CollectiviteDeliberante deliberante = CollectiviteDeliberante.builder()
            .siren(SIREN_NANTES)
            .annee(MILLESIME_2022)
            .nom("NANTES METROPOLE")
            .codePostal(CODE_POSTAL_NANTES)
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxationParNature(String.join(";", regimes))
            .build();
        return deliberante;
    }

    private CollectiviteDto creationCollectivite(CollectiviteDeliberanteDto deliberante)
    {
        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren(SIREN_BRAINS)
            .annee(MILLESIME_2022)
            .nom("BRAINS")
            .codePostal(CODE_POSTAL_BRAINS)
            .codeInsee(CODE_INSEE_BRAINS)
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .collectiviteDeliberante(deliberante)
            .build();
        return collectivite;
    }

    private CollectiviteDeliberanteDto creationCollectiviteDeliberanteDto()
    {
        final List<Double> tarifs = Arrays.asList(2.25, 2.25, 2.25, 1.3, 0.75, 0.65, 0.5, 0.2);

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_NANTES)
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(tarifs)
            .build();

        final List<String> regimes = new ArrayList<>(Collections.nCopies(5, RegimeTaxation.REGIME_REEL.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_FORFAITAIRE.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_REEL.getLabel()));

        final CollectiviteDeliberanteDto deliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_NANTES)
            .annee(MILLESIME_2022)
            .nom("NANTES METROPOLE")
            .codePostal(CODE_POSTAL_NANTES)
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(regimes)
            .listeDeCollectivites(new ArrayList<>())
            .build();

        return deliberante;
    }
}
