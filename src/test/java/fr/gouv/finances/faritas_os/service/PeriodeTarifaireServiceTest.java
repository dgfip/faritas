/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.repository.PeriodeTarifaireRepository;
import fr.gouv.finances.faritas_os.service.impl.PeriodeTarifaireServiceImpl;

@ExtendWith(MockitoExtension.class)
class PeriodeTarifaireServiceTest
{
    @Mock
    private PeriodeTarifaireRepository periodeTarifaireRepository;

    @InjectMocks
    private PeriodeTarifaireServiceImpl periodeTarifaireService;

    @Test
    void testRecupererPeriodeTarifaireUnique()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        final PeriodeTarifaire periode1 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 4, 1))
            .dateFin(LocalDate.of(2023, 6, 30))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode1);

        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 5, 11),
                LocalDate.of(2023, 5, 21));

        // assert collectiviteDeliberante
        assertNotNull(listePeriodeTarifaire);

        final PeriodeTarifaireDto periode = listePeriodeTarifaire.get(0);
        assertEquals(periode1.getSiren(), periode.getSiren());
        assertEquals(periode1.getDateDebut(), periode.getDateDebut());
        assertEquals(periode1.getDateFin(), periode.getDateFin());
        assertEquals(periode1.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.7, periode.getTarifsFixesCategories().get(0), 0.0);
    }

    @Test
    void testRecupererPeriodeTarifaireLimiteDebut()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        final PeriodeTarifaire periode1 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 4, 1))
            .dateFin(LocalDate.of(2023, 6, 30))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode1);

        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 4, 1),
                LocalDate.of(2023, 4, 11));

        // assert collectiviteDeliberante
        assertNotNull(listePeriodeTarifaire);

        final PeriodeTarifaireDto periode = listePeriodeTarifaire.get(0);
        assertEquals(periode1.getSiren(), periode.getSiren());
        assertEquals(periode1.getDateDebut(), periode.getDateDebut());
        assertEquals(periode1.getDateFin(), periode.getDateFin());
        assertEquals(periode1.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.7, periode.getTarifsFixesCategories().get(0), 0.0);
    }

    @Test
    void testRecupererPeriodeTarifaireLimiteFin()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        final PeriodeTarifaire periode1 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 4, 1))
            .dateFin(LocalDate.of(2023, 6, 30))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode1);

        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 6, 21),
                LocalDate.of(2023, 7, 1));

        // assert Période tarifaire
        assertNotNull(listePeriodeTarifaire);
        assertEquals(1, listePeriodeTarifaire.size());

        final PeriodeTarifaireDto periode = listePeriodeTarifaire.get(0);
        assertEquals(periode1.getSiren(), periode.getSiren());
        assertEquals(periode1.getDateDebut(), periode.getDateDebut());
        assertEquals(periode1.getDateFin(), periode.getDateFin());
        assertEquals(periode1.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.7, periode.getTarifsFixesCategories().get(0), 0.0);
    }

    @Test
    void testRecupererDeuxPeriodesTarifaires()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        final PeriodeTarifaire periode1 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 4, 1))
            .dateFin(LocalDate.of(2023, 6, 30))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode1);
        final PeriodeTarifaire periode2 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 7, 1))
            .dateFin(LocalDate.of(2023, 8, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories("0.8;0.8;0.8;0.6;0.4;0.3;0.3;0.3")
            .build();
        periodes.add(periode2);

        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 6, 25),
                LocalDate.of(2023, 7, 5));

        // assert collectiviteDeliberante
        assertNotNull(listePeriodeTarifaire);
        assertEquals(2, listePeriodeTarifaire.size());

        PeriodeTarifaireDto periode = listePeriodeTarifaire.get(0);
        assertEquals(periode1.getSiren(), periode.getSiren());
        assertEquals(periode1.getDateDebut(), periode.getDateDebut());
        assertEquals(periode1.getDateFin(), periode.getDateFin());
        assertEquals(periode1.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.7, periode.getTarifsFixesCategories().get(0), 0.0);

        periode = listePeriodeTarifaire.get(1);
        assertEquals(periode2.getSiren(), periode.getSiren());
        assertEquals(periode2.getDateDebut(), periode.getDateDebut());
        assertEquals(periode2.getDateFin(), periode.getDateFin());
        assertEquals(periode2.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.8, periode.getTarifsFixesCategories().get(1), 0.0);
    }

    @Test
    void testRecupererToutesPeriodesTarifaires()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        final PeriodeTarifaire periode1 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 4, 1))
            .dateFin(LocalDate.of(2023, 6, 30))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode1);
        final PeriodeTarifaire periode2 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 7, 1))
            .dateFin(LocalDate.of(2023, 8, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories("0.8;0.8;0.8;0.6;0.4;0.3;0.3;0.3")
            .build();
        periodes.add(periode2);
        final PeriodeTarifaire periode3 = PeriodeTarifaire.builder()
            .siren("212002695")
            .dateDebut(LocalDate.of(2023, 9, 1))
            .dateFin(LocalDate.of(2023, 10, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories("0.7;0.7;0.7;0.5;0.3;0.2;0.2;0.2")
            .build();
        periodes.add(periode3);

        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 03, 25),
                LocalDate.of(2023, 11, 05));

        // assert liste des periodes contient les trois periodes
        assertNotNull(listePeriodeTarifaire);
        assertEquals(3, listePeriodeTarifaire.size());

        PeriodeTarifaireDto periode = listePeriodeTarifaire.get(0);
        assertEquals(periode1.getSiren(), periode.getSiren());
        assertEquals(periode1.getDateDebut(), periode.getDateDebut());
        assertEquals(periode1.getDateFin(), periode.getDateFin());
        assertEquals(periode1.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.7, periode.getTarifsFixesCategories().get(0), 0.0);

        periode = listePeriodeTarifaire.get(1);
        assertEquals(periode2.getSiren(), periode.getSiren());
        assertEquals(periode2.getDateDebut(), periode.getDateDebut());
        assertEquals(periode2.getDateFin(), periode.getDateFin());
        assertEquals(periode2.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.8, periode.getTarifsFixesCategories().get(1), 0.0);

        periode = listePeriodeTarifaire.get(2);
        assertEquals(periode3.getSiren(), periode.getSiren());
        assertEquals(periode3.getDateDebut(), periode.getDateDebut());
        assertEquals(periode3.getDateFin(), periode.getDateFin());
        assertEquals(periode3.getTarifPourcentageCategorie19(), periode.getTarifPourcentageCategorie19());
        assertEquals(0.5, periode.getTarifsFixesCategories().get(3), 0.0);
    }

    @Test
    void testRecupererPeriodeTarifaireVide()
    {
        final List<PeriodeTarifaire> periodes = new ArrayList<>();
        when(periodeTarifaireRepository.findAllBySirenAndDatesSejour(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(periodes);

        final List<PeriodeTarifaireDto> listePeriodeTarifaire =
            periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("212002695", LocalDate.of(2023, 02, 01),
                LocalDate.of(2023, 02, 11));

        // assert liste des periodes est vide
        assertNotNull(listePeriodeTarifaire);
    }
}