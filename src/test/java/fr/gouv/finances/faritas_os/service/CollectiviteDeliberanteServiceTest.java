/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.dto.RegimeTaxation;
import fr.gouv.finances.faritas_os.entity.CollectiviteDeliberante;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.repository.CollectiviteDeliberanteRepository;
import fr.gouv.finances.faritas_os.service.impl.CollectiviteDeliberanteServiceImpl;

@ExtendWith(MockitoExtension.class)
class CollectiviteDeliberanteServiceTest
{
    @Mock
    private CollectiviteDeliberanteRepository collectiviteDeliberanteRepository;

    @InjectMocks
    private CollectiviteDeliberanteServiceImpl collectiviteDeliberanteService;

    @Captor
    ArgumentCaptor<ArrayList<CollectiviteDeliberante>> collectiviteDeliberanteCaptor;

    @Test
    @DisplayName("Vérifie que l'enregistrement d'une liste de collectivites ")
    void testEnregistrer()
    {
        // Arrange
        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("243301221")
            .annee("2021")
            .nom("Ma collectivite")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .build();
        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(5, RegimeTaxation.REGIME_REEL.getLabel()));
        regimesTaxation.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_FORFAITAIRE.getLabel()));
        regimesTaxation.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_REEL.getLabel()));

        final PeriodeTarifaireDto periodeTarifaireDto = PeriodeTarifaireDto.builder()
            .siren("243301223")
            .dateDebut(LocalDate.of(2021, 01, 01))
            .dateFin(LocalDate.of(2021, 12, 31))
            .tarifPourcentageCategorie19(5.7)
            .tarifsFixesCategories(Arrays.asList(8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0))
            .build();

        final CollectiviteDeliberanteDto nouvelleCollectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren("243301223")
            .annee("2021")
            .nom("Ma collectivite deliberante")
            .codePostal("02021")
            .listeDeCollectivites(Arrays.asList(collectivite))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaireDto))
            .regimesTaxation(regimesTaxation)
            .build();

        final List<CollectiviteDeliberanteDto> listeCollectivitesDeliberantes = Arrays.asList(nouvelleCollectiviteDeliberante);
        // Act
        collectiviteDeliberanteService.enregistrer(listeCollectivitesDeliberantes);
        // verify and assert
        verify(collectiviteDeliberanteRepository, times(1)).saveAll(collectiviteDeliberanteCaptor.capture());
        assertEquals("2021", collectiviteDeliberanteCaptor.getValue().get(0).getListeCollectivite().get(0).getAnnee());
        assertEquals("243301221", collectiviteDeliberanteCaptor.getValue().get(0).getListeCollectivite().get(0).getSiren());
        assertFalse(collectiviteDeliberanteCaptor.getValue().get(0).getListeCollectivite().get(0).getTaxeGrandeVitesseApplicable());
    }

    @Test
    void testIsDeliberanteFound()
    {
        // Arrange
        final CollectiviteDeliberante collectivite1 = creationCollectiviteDeleberante();
        when(collectiviteDeliberanteRepository.findBySiren("123456780")).thenReturn(Arrays.asList(collectivite1));
        // Act
        // Veriy
        assertTrue(collectiviteDeliberanteService.isDeliberanteFound("123456780"));

        // Arrange
        when(collectiviteDeliberanteRepository.findBySiren("234567890")).thenReturn(new ArrayList<>());
        // Act
        // Veriy
        assertFalse(collectiviteDeliberanteService.isDeliberanteFound("234567890"));
    }

    @Test
    void testSupprimerTout()
    {
        collectiviteDeliberanteService.supprimerTout();
        verify(collectiviteDeliberanteRepository, times(1)).deleteAll();
    }

    @Test
    void testGetDeliberanteFromSirenAndAnnee()
    {
        // Arrange
        final CollectiviteDeliberante deliberante = creationCollectiviteDeleberante();
        // Optional<CollectiviteDeliberante> optionDeliberante = Optional.of(deliberanteModel);
        // CompositePrimaryKey pk1 = new CompositePrimaryKey("123456780", "2022");
        when(collectiviteDeliberanteRepository.findById(any(CompositePrimaryKey.class))).thenReturn(Optional.ofNullable(deliberante));
        // Act
        final CollectiviteDeliberanteDto collectivite = collectiviteDeliberanteService.getDeliberanteFromSirenAndAnnee("123456780", 2022);
        // Veriy
        assertNotNull(collectivite);
        assertEquals("2022", collectivite.getAnnee());
        assertEquals("123456780", collectivite.getSiren());
        assertEquals("COLL DELIBERANTE", collectivite.getNom());

        // Arrange
        when(collectiviteDeliberanteRepository.findById(any(CompositePrimaryKey.class))).thenReturn(Optional.empty());
        // Act
        final CollectiviteDeliberanteDto collectivite2 = collectiviteDeliberanteService.getDeliberanteFromSirenAndAnnee("123456780", 2022);
        // Veriy
        assertNull(collectivite2);
    }

    private CollectiviteDeliberante creationCollectiviteDeleberante()
    {
        final List<Double> tarifs = Arrays.asList(2.25, 2.25, 2.25, 1.3, 0.75, 0.65, 0.5, 0.2);
        final List<String> regimes = new ArrayList<>(Collections.nCopies(5, RegimeTaxation.REGIME_REEL.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_FORFAITAIRE.getLabel()));
        regimes.addAll(Collections.nCopies(3, RegimeTaxation.REGIME_REEL.getLabel()));

        final PeriodeTarifaire periodeTarifaireDto = PeriodeTarifaire.builder()
            .siren("123456780")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(String.join(";", tarifs.stream().map(Object::toString).collect(Collectors.toList())))
            .build();

        final CollectiviteDeliberante deliberante = CollectiviteDeliberante.builder()
            .siren("123456780")
            .annee("2022")
            .nom("COLL DELIBERANTE")
            .codePostal("44000")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaireDto))
            .regimesTaxationParNature(String.join(";", regimes))
            .build();
        return deliberante;
    }

}
