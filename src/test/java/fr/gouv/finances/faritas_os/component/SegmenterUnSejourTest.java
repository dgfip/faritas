/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import fr.gouv.finances.faritas_os.dto.AnomalieValideEnum;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.dto.SejourDeclare;
import fr.gouv.finances.faritas_os.exception.CalculTaxeSejourImpossibleException;
import fr.gouv.finances.faritas_os.service.AdresseService;
import fr.gouv.finances.faritas_os.service.CollectiviteDeliberanteService;
import fr.gouv.finances.faritas_os.service.CollectiviteService;
import fr.gouv.finances.faritas_os.service.PeriodeTarifaireService;

@ExtendWith(MockitoExtension.class)
class SegmenterUnSejourTest
{
    @Mock
    private CollectiviteService collectiviteService;

    @Mock
    private PeriodeTarifaireService periodeTarifaireService;

    @Mock
    private CollectiviteDeliberanteService collectiviteDeliberanteService;

    @Mock
    private AdresseService adresseService;

    @Mock
    private MoteurDeCalculDeLaTaxeDeSejour moteurDeCalculDeLaTaxeDeSejour;

    @InjectMocks
    SegmenterUnSejour segmenterUnSejour;

    @Test
    void testTraitementSejourDeclare()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .adresse("25 route de Carquefou")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .categorieLogement(12)
            .natureLogement(5)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(100.0)
            .sirenCommune("214402018")
            .sirenCollectiviteDeliberante("244500503")
            .build();

        final CollectiviteDeliberanteDto delib1 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("244400503")
            .codePostal("44119")
            .nom("CC ERDRE ET GESVRES")
            .regimesTaxation(Collections.nCopies(11, "Réel"))
            .build();

        final CollectiviteDeliberanteDto delib2 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("244400404")
            .codePostal("44923")
            .nom("MET NANTES METROLPOLE")
            .regimesTaxation(Collections.nCopies(11, "Réel"))
            .build();

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .siren("214400350")
            .nom("CHAPELLE-SUR-ERDRE (LA)")
            .codePostal("44240")
            .codeInsee("44035")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib2)
            .build();

        final CollectiviteDto col2 = CollectiviteDto.builder()
            .siren("214402018")
            .nom("SUCE-SUR-ERDRE")
            .codePostal("44240")
            .codeInsee("44201")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib1)
            .build();

        when(collectiviteService.listerParAnneeEtCodePostal(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Arrays.asList(col1, col2));

        when(moteurDeCalculDeLaTaxeDeSejour.calculer(Mockito.any(SejourAnalyse.class), Mockito.any(CollectiviteDto.class)))
            .thenReturn(114.08);

        final SejourAnalyse resultat = segmenterUnSejour.traitementSejourDeclare(sejour);

        assertThat(resultat.getMontantTaxeDeSejourCalculee()).isEqualTo(114.08);
        assertThat(resultat.getPrixParNuitee()).isEqualTo(30.0);
        assertThat(resultat.getListDAnomalies()).hasSize(2);
        assertThat(resultat.getListDAnomalies()).extracting(AnomalieDto::getMessage)
            .containsExactlyInAnyOrder(
                AnomalieValideEnum.RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE,
                AnomalieValideEnum.TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE);

    }

    @Test
    void testTraitementSejourDeclareForfait()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .codePostal("73640")
            .ville("Sainte Foy Tarentaise")
            .categorieLogement(12)
            .natureLogement(2)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresSocial(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(12.0)
            .sirenCommune("217302322")
            .sirenCollectiviteDeliberante("217302322")
            .build();

        final List<String> regimes = new ArrayList<>();
        regimes.add("Réel");
        regimes.addAll(Collections.nCopies(2, "Forfaitaire"));
        regimes.addAll(Collections.nCopies(8, "Réel"));

        final CollectiviteDeliberanteDto delib1 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("217302322")
            .codePostal("73640")
            .nom("SAINTE-FOY-TARENTAISE")
            .regimesTaxation(regimes)
            .build();

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .siren("217302322")
            .nom("SAINTE-FOY-TARENTAISE")
            .codePostal("73640")
            .codeInsee("73232")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib1)
            .build();

        when(collectiviteService.listerParAnneeEtCodePostal(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Arrays.asList(col1));

        when(moteurDeCalculDeLaTaxeDeSejour.calculer(Mockito.any(SejourAnalyse.class), Mockito.any(CollectiviteDto.class)))
            .thenReturn(0.0);

        final SejourAnalyse resultat = segmenterUnSejour.traitementSejourDeclare(sejour);

        assertThat(resultat.getMontantTaxeDeSejourCalculee()).isEqualTo(0.0);
        assertThat(resultat.getPrixParNuitee()).isEqualTo(30.0);
        assertThat(resultat.getListDAnomalies()).hasSize(1);
        assertThat(resultat.getListDAnomalies()).extracting(AnomalieDto::getMessage)
            .containsExactlyInAnyOrder(AnomalieValideEnum.TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE);

    }

    @Test
    void testTraitementSejourDeclareException()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .codePostal("73640")
            .ville("Sainte Foy Tarentaise")
            .categorieLogement(12)
            .natureLogement(2)
            .nombreNuits(5)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(12.0)
            .sirenCommune("217302322")
            .sirenCollectiviteDeliberante("217302322")
            .build();

        final List<String> regimes = new ArrayList<>();
        regimes.add("Réel");
        regimes.addAll(Collections.nCopies(2, "Forfaitaire"));
        regimes.addAll(Collections.nCopies(8, "Réel"));

        final CollectiviteDeliberanteDto delib1 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("217302322")
            .codePostal("73640")
            .nom("SAINTE-FOY-TARENTAISE")
            .regimesTaxation(regimes)
            .build();

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .siren("217302322")
            .nom("SAINTE-FOY-TARENTAISE")
            .codePostal("73640")
            .codeInsee("73232")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib1)
            .build();

        when(collectiviteService.listerParAnneeEtCodePostal(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Arrays.asList(col1));

        final List<AnomalieDto> anos = new ArrayList<>();
        anos.add(AnomalieDto.creerAnomalie(AnomalieValideEnum.NOMBRE_DE_VOYAGEURS_MANQUANT));

        when(moteurDeCalculDeLaTaxeDeSejour.calculer(Mockito.any(SejourAnalyse.class), Mockito.any(CollectiviteDto.class)))
            .thenThrow(new CalculTaxeSejourImpossibleException(anos));

        final SejourAnalyse resultat = segmenterUnSejour.traitementSejourDeclare(sejour);

        assertThat(resultat.getMontantTaxeDeSejourCalculee()).isNull();
        assertThat(resultat.getPrixParNuitee()).isNull();
        assertThat(resultat.getListDAnomalies()).hasSize(1);
        assertThat(resultat.getListDAnomalies()).extracting(AnomalieDto::getMessage)
            .containsExactlyInAnyOrder(AnomalieValideEnum.NOMBRE_DE_VOYAGEURS_MANQUANT);

    }

    @Test
    void testTraitementSejourDeclareCategorie19Erreur()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .codePostal("73640")
            .ville("Sainte Foy Tarentaise")
            .categorieLogement(19)
            .natureLogement(5)
            .nombreVoyageurs(5)
            .nombreVoyageursExoneresUrgence(1)
            .nombreVoyageursExoneresAutres(1)
            .nombreVoyageursExoneresSaisonniers(1)
            .nombreNuits(5)
            .montantTotalTaxeSejour(12.0)
            .sirenCommune("217302322")
            .sirenCollectiviteDeliberante("217302322")
            .build();

        final CollectiviteDeliberanteDto delib1 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("217302322")
            .codePostal("73640")
            .nom("SAINTE-FOY-TARENTAISE")
            .regimesTaxation(Collections.nCopies(11, "Réel"))
            .build();

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .siren("217302322")
            .nom("SAINTE-FOY-TARENTAISE")
            .codePostal("73640")
            .codeInsee("73232")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib1)
            .build();

        when(collectiviteService.listerParAnneeEtCodePostal(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Arrays.asList(col1));

        when(moteurDeCalculDeLaTaxeDeSejour.calculer(Mockito.any(SejourAnalyse.class), Mockito.any(CollectiviteDto.class)))
            .thenReturn(12.0);

        final SejourAnalyse resultat = segmenterUnSejour.traitementSejourDeclare(sejour);

        assertThat(resultat.getMontantTaxeDeSejourCalculee()).isEqualTo(12.0);
        assertThat(resultat.getPrixParNuitee()).isNull();
        assertThat(resultat.getListDAnomalies()).hasSize(1);
        assertThat(resultat.getListDAnomalies()).extracting(AnomalieDto::getMessage)
            .containsExactlyInAnyOrder(AnomalieValideEnum.CATEGORIE_19_ET_PRIX_MANQUANT);

    }

    @Test
    void testTraitementSejourDeclareNombreNuitIncoherentErreur()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("10/02/2024")
            .datePerception("10/02/2024")
            .codePostal("73640")
            .ville("Sainte Foy Tarentaise")
            .categorieLogement(12)
            .natureLogement(5)
            .nombreNuits(7)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(100.0)
            .montantTotalTaxeSejour(12.0)
            .sirenCommune("217302322")
            .sirenCollectiviteDeliberante("217302322")
            .build();

        final CollectiviteDeliberanteDto delib1 = CollectiviteDeliberanteDto.builder()
            .annee("2024")
            .siren("217302322")
            .codePostal("73640")
            .nom("SAINTE-FOY-TARENTAISE")
            .regimesTaxation(Collections.nCopies(11, "Réel"))
            .build();

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .siren("217302322")
            .nom("SAINTE-FOY-TARENTAISE")
            .codePostal("73640")
            .codeInsee("73232")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .collectiviteDeliberante(delib1)
            .build();

        when(collectiviteService.listerParAnneeEtCodePostal(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(Arrays.asList(col1));

        when(moteurDeCalculDeLaTaxeDeSejour.calculer(Mockito.any(SejourAnalyse.class), Mockito.any(CollectiviteDto.class)))
            .thenReturn(12.0);

        final SejourAnalyse resultat = segmenterUnSejour.traitementSejourDeclare(sejour);

        assertThat(resultat.getMontantTaxeDeSejourCalculee()).isEqualTo(12.0);
        assertThat(resultat.getPrixParNuitee()).isEqualTo(30.0);
        assertThat(resultat.getListDAnomalies()).hasSize(1);
        assertThat(resultat.getListDAnomalies()).extracting(AnomalieDto::getMessage)
            .containsExactlyInAnyOrder(AnomalieValideEnum.NOMBRE_DE_NUITS_INCOHERENT);

    }
}
