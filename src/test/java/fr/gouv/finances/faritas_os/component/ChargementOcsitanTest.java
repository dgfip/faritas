/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import fr.gouv.finances.faritas_os.service.AdresseService;
import fr.gouv.finances.faritas_os.service.CollectiviteDeliberanteService;
import fr.gouv.finances.faritas_os.service.OcsitanReaderService;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class ChargementOcsitanTest
{
    @Mock
    OcsitanReaderService ocsitanReader;

    @Mock
    private AdresseService adresseService;

    @Mock
    private CollectiviteDeliberanteService collectiviteDeliberanteService;

    @InjectMocks
    private ChargementOcsitan chargementOcsitan;

    @TempDir
    File tmpDir;

    @Test
    void testChargerDonneesOcsitan() throws ReadOcsitanException, IOException, URISyntaxException
    {
        // final File file = new File(tmpDir, pathOcsitanFile);
        // Files.copy(is, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        final Path pathOcsitanFile = Paths.get(getClass().getClassLoader().getResource("ocsitanfile.xml").toURI());

        final CollectiviteDto col1 = CollectiviteDto.builder()
            .annee("2021")
            .codePostal("13190")
            .nom("ALLAUCH")
            .siren("211300025")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(true)
            .taxeAdditionnelleIdfm(false)
            .build();

        final CollectiviteDto col2 = CollectiviteDto.builder()
            .annee("2021")
            .codePostal("25570")
            .nom("GRAND COMBE-CHATELEU")
            .siren("212502850")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .build();

        final CollectiviteDto col3 = CollectiviteDto.builder()
            .annee("2021")
            .codePostal("25500")
            .nom("MONTLEBON")
            .siren("212504039")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(false)
            .build();

        final List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes = Arrays.asList(
            CollectiviteDeliberanteDto.builder()
                .siren("211300025")
                .annee("2021")
                .nom("ALLAUCH")
                .codePostal("13190")
                .listeDeCollectivites(Arrays.asList(col1))
                .build(),
            CollectiviteDeliberanteDto.builder()
                .siren("242504116")
                .annee("2021")
                .nom("CC VAL DE MORTEAU")
                .codePostal("25503")
                .listeDeCollectivites(Arrays.asList(col2, col3))
                .build());

        when(ocsitanReader.recupererCollectivitesDeliberantes(Mockito.any(String.class), Mockito.any(InputStream.class)))
            .thenReturn(listeDeCollectivitesDeliberantes);

        when(adresseService.recupererCodeInsee("ALLAUCH", "13190")).thenReturn(Mono.just("13002"));
        when(adresseService.recupererCodeInsee("GRAND COMBE-CHATELEU", "25570")).thenReturn(Mono.just("25285"));
        when(adresseService.recupererCodeInsee("MONTLEBON", "25500")).thenReturn(Mono.just("25403"));

        // Act
        chargementOcsitan.chargerDonneesOcsitan("2021", pathOcsitanFile);

        // Assert
        verify(collectiviteDeliberanteService, times(1)).enregistrer(listeDeCollectivitesDeliberantes);
    }
}
