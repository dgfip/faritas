/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.dto.RegimeTaxation;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.exception.CalculTaxeSejourImpossibleException;
import fr.gouv.finances.faritas_os.service.impl.CollectiviteDeliberanteServiceImpl;
import fr.gouv.finances.faritas_os.service.impl.CollectiviteServiceImpl;
import fr.gouv.finances.faritas_os.service.impl.PeriodeTarifaireServiceImpl;

@ExtendWith(MockitoExtension.class)
class MoteurDeCalculDeLaTaxeDeSejourTest
{
    private static final Double TAXE_REGION = 0.15;

    private static final Double TAXE_DEPARTEMENT = 0.1;

    private static final Double TAXE_GV = 0.34;

    private static final Double TAXE_IDFM = 2.00;

    private static final String DEPARTEMENTS_GV = "06,13,83";

    private static final String DEPARTEMENTS_IDFM = "75,77,78,91,92,93,94,95";

    private static final String SIREN_DELIBERANTE = "200040210";

    @Mock
    private CollectiviteServiceImpl collectiviteService;

    @Mock
    private CollectiviteDeliberanteServiceImpl collectiviteDeliberanteService;

    @Mock
    private PeriodeTarifaireServiceImpl periodeTarifaireService;

    @InjectMocks
    MoteurDeCalculDeLaTaxeDeSejour moteurDeCalculDeLaTaxeDeSejour;

    @BeforeEach
    public void setUp()
    {
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "taxeDepartementale", TAXE_DEPARTEMENT);
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "taxeRegionale", TAXE_REGION);
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "taxeGrandeVitesse", TAXE_GV);
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "taxeIdfm", TAXE_IDFM);
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "listeDepartementsLGV", DEPARTEMENTS_GV);
        ReflectionTestUtils.setField(moteurDeCalculDeLaTaxeDeSejour, "listeDepartementsIdfm", DEPARTEMENTS_IDFM);
    }

    @Test
    void testCalculerTarifAUtiliserCategorie19()
    {
        final SejourAnalyse sejour = SejourAnalyse.builder()
            .categorieLogement(null)
            .prixParNuitee(50.0)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(Arrays.asList(2.25, 2.25, 2.25, 1.3, 0.75, 0.65, 0.5, 0.2))
            .build();

        final Double montant = moteurDeCalculDeLaTaxeDeSejour.calculerTarifAUtiliser(sejour, periodeTarifaire);
        assertEquals(1.5, montant);
    }

    @Test
    void testCalculerTarifAUtiliserCategories()
    {
        final SejourAnalyse ligneSegmentee = SejourAnalyse.builder()
            .categorieLogement(15)
            .prixParNuitee(50.0)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(Arrays.asList(2.25, 2.25, 2.25, 1.3, 0.75, 0.65, 0.5, 0.2))
            .build();

        final Double montant = moteurDeCalculDeLaTaxeDeSejour.calculerTarifAUtiliser(ligneSegmentee, periodeTarifaire);
        assertEquals(0.75, montant);
    }

    @Test
    @DisplayName("Unit test on feature testCalculateTaxe. Case: taxe LGV applicable")
    void testCalculateTaxeLGVApplicable() throws CalculTaxeSejourImpossibleException
    {
        final LocalDate debutSejour = LocalDate.of(2023, 4, 1);
        final LocalDate finSejour = LocalDate.of(2023, 4, 11);

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2023")
            .nom("VILLECROZE")
            .codePostal("83690")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(true)
            .codeInsee("83149")
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2023")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2023)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(3)
            .dateDebutSejour(debutSejour)
            .ville("VILLECROZE")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuit(120.0)
            .datePerception(debutSejour)
            .categorieLogement(13)
            .nombreNuits(10)
            .natureLogement(4)
            .codePostal("83000")
            .dateFinSejour(finSejour)
            .montantTotalTaxeSejour(86.4)
            .build();

        final Double taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(99.3, taxe);
    }

    @Test
    @DisplayName("Unit test on feature testCalculateTaxe. Case: taxe LGV applicable car ocsitan >= 2024")
    void testCalculateTaxeLGVNonApplicableOcsitan() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2024")
            .nom("VILLECROZE")
            .codePostal("83690")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(true)
            .codeInsee("83149")
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2024, 01, 01))
            .dateFin(LocalDate.of(2024, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2024")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2024)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(3)
            .dateDebutSejour(LocalDate.of(2024, 3, 1))
            .ville("VILLECROZE")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuit(120.0)
            .datePerception(LocalDate.of(2024, 3, 1))
            .categorieLogement(13)
            .nombreNuits(10)
            .natureLogement(4)
            .codePostal("83000")
            .dateFinSejour(LocalDate.of(2024, 3, 11))
            .montantTotalTaxeSejour(86.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(99.3, taxe);
    }

    @Test
    @DisplayName("Unit test on feature testCalculateTaxe. Case: taxe LGV non applicable car année < 2023")
    void testCalculateTaxeLGVNonApplicableAnnee() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2022")
            .nom("VILLECROZE")
            .codePostal("83690")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .codeInsee("83149")
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2022")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2022)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(3)
            .dateDebutSejour(LocalDate.of(2022, 10, 1))
            .ville("VILLECROZE")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuit(120.0)
            .datePerception(LocalDate.of(2022, 10, 1))
            .categorieLogement(13)
            .nombreNuits(10)
            .natureLogement(4)
            .codePostal("83000")
            .dateFinSejour(LocalDate.of(2022, 10, 11))
            .montantTotalTaxeSejour(86.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(75.9, taxe);
    }

    @Test
    @DisplayName("Unit test on feature testCalculateTaxe. Case: taxe LGV non applicable car dept 59")
    void testCalculateTaxeLGVNonApplicableDept() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("200093201")
            .annee("2023")
            .nom("MOUVAUX")
            .codePostal("59420")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .codeInsee("59421")
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2023")
            .nom("MOUVAUX")
            .codePostal("59420")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);
        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("200093201", 2023)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .collectiviteDeliberanteSiren(SIREN_DELIBERANTE)
            .collectiviteDeliberanteNom("Valloire")
            .sirenCommune("200093201")
            .ville("MOUVAUX")
            .codePostal("59420")
            .dateDebutSejour(LocalDate.of(2023, 4, 1))
            .datePerception(LocalDate.of(2023, 4, 1))
            .dateFinSejour(LocalDate.of(2023, 4, 11))
            .nombreVoyageurs(3)
            .nombreNuits(10)
            .categorieLogement(13)
            .prixParNuit(120.0)
            .montantTotalTaxeSejour(66.0)
            .natureLogement(4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(75.9, taxe);
    }

    @Test
    @DisplayName("Unit test on feature testCalculateTaxe. Case: taxe LGV non applicable car année < 2023 et departement 59")
    void testCalculateTaxeLGVNonApplicableDeptAnnee() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("200093201")
            .annee("2022")
            .nom("MOUVAUX")
            .codePostal("59420")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .codeInsee("59421")
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2022")
            .nom("MOUVAUX")
            .codePostal("59420")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);
        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("200093201", 2022)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .collectiviteDeliberanteSiren(SIREN_DELIBERANTE)
            .collectiviteDeliberanteNom("Valloire")
            .sirenCommune("200093201")
            .ville("MOUVAUX")
            .codePostal("59420")
            .dateDebutSejour(LocalDate.of(2022, 10, 1))
            .datePerception(LocalDate.of(2022, 10, 1))
            .dateFinSejour(LocalDate.of(2022, 10, 11))
            .nombreVoyageurs(3)
            .nombreNuits(10)
            .categorieLogement(13)
            .prixParNuit(120.0)
            .montantTotalTaxeSejour(66.0)
            .natureLogement(4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(75.9, taxe);
    }

    @Test
    @DisplayName("taxe de séjour calculée selon la nature du régime appliqué")
    void calculerTaxeAvecRegimesDeTaxation() throws CalculTaxeSejourImpossibleException
    {
        final CollectiviteDto collectivite1 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .build();

        final CollectiviteDto collectivite2 = CollectiviteDto.builder()
            .siren("098765432")
            .annee("2022")
            .nom("les belles villes")
            .codePostal("01234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(true)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren("123456789")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.64, 2.28, 1.82, 1.37, 0.82, 0.73, 0.55, 0.2))
            .build();

        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()));
        regimesTaxation.add(RegimeTaxation.REGIME_NON_ATTACHE.getLabel());
        regimesTaxation.add(RegimeTaxation.REGIME_FORFAITAIRE.getLabel());
        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren("123456789")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite1, collectivite2))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite1.setCollectiviteDeliberante(collectiviteDeliberante);
        collectivite2.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("123456789");
        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("098765432", 2022)).thenReturn("123456789");
        // when(collectiviteDeliberanteService.getDeliberanteFromSirenAndAnnee("123456789",
        // 2022)).thenReturn(collectiviteDeliberante);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(19)
            .prixParNuitee(45.70)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(2)
            .build();

        final SejourAnalyse sejour2 = SejourAnalyse.builder()
            .ville("les belles villes")
            .codePostal("01234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(13)
            .prixParNuitee(300.00)
            .montantTotalTaxeSejour(127.12)
            .natureLogement(2)
            .build();

        final SejourAnalyse sejour3 = SejourAnalyse.builder()
            .ville("les belles villes")
            .codePostal("01234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(13)
            .prixParNuitee(300.00)
            .montantTotalTaxeSejour(127.12)
            .natureLogement(10)
            .build();

        final SejourAnalyse sejour4 = SejourAnalyse.builder()
            .ville("les belles villes")
            .codePostal("01234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(13)
            .prixParNuitee(300.00)
            .montantTotalTaxeSejour(127.12)
            .natureLogement(11)
            .build();

        // when case : only taxe departementale + régime réel
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite1);
        final Double actualTaxe1 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite1);
        // then assert
        assertEquals(42.21, actualTaxe1);

        // when case : taxe departementale + regionale + régime réel
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite2);
        final Double actualTaxe2 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour2, collectivite2);
        // then assert
        assertEquals(47.67, actualTaxe2);

        // when case : regime forfaitaire
        final Double actualTaxe3 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour3, collectivite2);
        // then assert
        assertEquals(0.0, actualTaxe3);

        // Non rattachée à un tarif
        final Double actualTaxe4 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour4, collectivite2);
        // then assert
        assertEquals(0.0, actualTaxe4);
    }

    @Test
    @DisplayName("taxe de séjour non null si Categorie et/ou Prix par nuite sont vides")
    void calculerTaxeAvecCategorieEtPrixParNuite() throws CalculTaxeSejourImpossibleException
    {
        final CollectiviteDto collectivite1 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren("123456789")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.64, 2.28, 1.82, 1.37, 0.82, 0.73, 0.55, 0.2))
            .build();

        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()));
        regimesTaxation.add(RegimeTaxation.REGIME_NON_ATTACHE.getLabel());
        regimesTaxation.add(RegimeTaxation.REGIME_FORFAITAIRE.getLabel());
        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren("123456789")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite1))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite1.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("123456789");
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite1);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(null)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(2)
            .build();

        final SejourAnalyse sejour2 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(19)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(30.54)
            .natureLogement(2)
            .build();

        final SejourAnalyse sejour3 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .dateFinSejour(LocalDate.of(2022, 5, 28))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(3)
            .nombreNuits(7)
            .categorieLogement(null)
            .prixParNuitee(80.5)
            .montantTotalTaxeSejour(30.54)
            .natureLogement(2)
            .build();

        // when case : categorie_du_logement est vide et prix_par_nuitee est vide
        final Double actualTaxe1 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite1);
        // then assert
        assertEquals(27.12, actualTaxe1);

        // when case : categorie_du_logement = 19 et prix_par_nuitee est vide
        final Double actualTaxe2 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour2, collectivite1);
        // then assert
        assertEquals(30.54, actualTaxe2);

        // when case : categorie_du_logement est vide et prix_par_nuitee > 0
        final Double actualTaxe3 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour3, collectivite1);
        // then assert
        assertEquals(74.34, actualTaxe3);
    }

    @Test
    @DisplayName("taxe de séjour basée sur ocsitan d'année n avec date fin de séjour = 1/1/n+1, date début et fin sur l'année n")
    void calculerTaxeSurAnneeN() throws CalculTaxeSejourImpossibleException
    {
        final LocalDate debutSejour1 = LocalDate.of(2022, 12, 15);
        final LocalDate finSejour1 = LocalDate.of(2023, 1, 1);
        final LocalDate debutSejour2 = LocalDate.of(2023, 1, 1);
        final LocalDate finSejour2 = LocalDate.of(2023, 1, 15);
        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel()));

        final CollectiviteDto collectivite2022 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2022 = PeriodeTarifaireDto.builder()
            .siren("123456789")
            .dateDebut(LocalDate.of(2022, 1, 1))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.50, 2.20, 1.80, 1.30, 0.80, 0.70, 0.50, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2022 = CollectiviteDeliberanteDto.builder()
            .siren("123456789")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite2022))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2022))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2022.setCollectiviteDeliberante(collectiviteDeliberante2022);

        final CollectiviteDto collectivite2023 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2023 = PeriodeTarifaireDto.builder()
            .siren("123456789")
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(4.5)
            .tarifsFixesCategories(Arrays.asList(3.70, 2.40, 2.0, 1.50, 1.0, 0.90, 0.70, 0.4))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2023 = CollectiviteDeliberanteDto.builder()
            .siren("123456789")
            .annee("2023")
            .nom("valloire")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite2023))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2023))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2023.setCollectiviteDeliberante(collectiviteDeliberante2023);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("123456789");

        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("123456789", debutSejour1, finSejour1))
            .thenReturn(Arrays.asList(periodeTarifaire2022));

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2023)).thenReturn("123456789");

        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("123456789", debutSejour2, finSejour2))
            .thenReturn(Arrays.asList(periodeTarifaire2023));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2022)).thenReturn(collectivite2022);
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2023)).thenReturn(collectivite2023);

        // Séjour du 15/12/2022 au 01/01/2023 -> 17 jours x 2 voyageurs
        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(debutSejour1)
            .dateFinSejour(finSejour1)
            .datePerception(debutSejour1)
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(2)
            .nombreNuits(17)
            .categorieLogement(13)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(4)
            .build();

        // Séjour du 01/01/2023 au 15/01/2023 -> 14 jours x 2 voyageurs
        final SejourAnalyse sejour2 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(debutSejour2)
            .dateFinSejour(finSejour2)
            .datePerception(finSejour2)
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(2)
            .nombreNuits(17)
            .categorieLogement(13)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(4)
            .build();

        final Double actualTaxe1 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite2022);
        // then assert
        assertEquals(61.2, actualTaxe1);

        final Double actualTaxe2 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour2, collectivite2023);

        // then assert
        assertEquals(56.0, actualTaxe2);
    }

    @Test
    @DisplayName("taxe de séjour basée sur ocsitan d'année n et n+1, date fin de séjour > 1/1/n+1")
    void calculerTaxeSurMillesimesDifferents() throws CalculTaxeSejourImpossibleException
    {
        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel()));

        final CollectiviteDto collectivite2022 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2022 = PeriodeTarifaireDto.builder()
            .siren("243500725")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.50, 2.20, 1.80, 1.30, 0.80, 0.70, 0.50, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2022 = CollectiviteDeliberanteDto.builder()
            .siren("243500725")
            .annee("2022")
            .nom("collectiviteDeliberante")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite2022))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2022))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2022.setCollectiviteDeliberante(collectiviteDeliberante2022);

        final CollectiviteDto collectivite2023 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2023")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2023 = PeriodeTarifaireDto.builder()
            .siren("243500725")
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(4.5)
            .tarifsFixesCategories(Arrays.asList(3.70, 2.40, 2.0, 1.50, 1.0, 0.90, 0.70, 0.4))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2023 = CollectiviteDeliberanteDto.builder()
            .siren("243500725")
            .annee("2023")
            .nom("collectiviteDeliberante")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite2022))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2022))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2023.setCollectiviteDeliberante(collectiviteDeliberante2023);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("243500725");
        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2023)).thenReturn("243500725");

        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("243500725", LocalDate.of(2022, 12, 15),
            LocalDate.of(2023, 1, 15))).thenReturn(Arrays.asList(periodeTarifaire2022, periodeTarifaire2023));

        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2023)).thenReturn(collectivite2023);
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2022)).thenReturn(collectivite2022);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 12, 15))
            .dateFinSejour(LocalDate.of(2023, 1, 15))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(2)
            .nombreNuits(17)
            .categorieLogement(13)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(4)
            .build();

        final Double actualTaxe1 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite2022);

        // then assert
        assertEquals(117.2, actualTaxe1);
    }

    @Test
    @DisplayName("Taxe de séjour basée sur ocsitan d'année n et n+1, date fin de séjour > 1/1/n+1 avec changement délibérante")
    void calculerTaxeSurMillesimesDifferentsDeliberanteDifferente() throws CalculTaxeSejourImpossibleException
    {
        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(11, RegimeTaxation.REGIME_REEL.getLabel()));

        final CollectiviteDto collectivite2022 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2022 = PeriodeTarifaireDto.builder()
            .siren("243500725")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.5, 2.2, 1.8, 1.3, 0.8, 0.7, 0.5, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2022 = CollectiviteDeliberanteDto.builder()
            .siren("243500725")
            .annee("2022")
            .nom("collectiviteDeliberante2022")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite2022))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2022))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2022.setCollectiviteDeliberante(collectiviteDeliberante2022);

        final CollectiviteDto collectivite2023 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2023")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire2023 = PeriodeTarifaireDto.builder()
            .siren("200068989")
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(3.0)
            .tarifsFixesCategories(Arrays.asList(3.6, 2.3, 1.8, 1.4, 0.9, 0.8, 0.6, 0.5))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante2023 = CollectiviteDeliberanteDto.builder()
            .siren("200068989")
            .annee("2023")
            .nom("collectiviteDeliberante2023")
            .codePostal("02235")
            .listeDeCollectivites(Arrays.asList(collectivite2023))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire2023))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite2023.setCollectiviteDeliberante(collectiviteDeliberante2023);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("243500725");
        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2023)).thenReturn("200068989");

        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("243500725", LocalDate.of(2022, 12, 15),
            LocalDate.of(2023, 1, 1))).thenReturn(Arrays.asList(periodeTarifaire2022));
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate("200068989", LocalDate.of(2023, 1, 1),
            LocalDate.of(2023, 1, 15))).thenReturn(Arrays.asList(periodeTarifaire2023));

        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2022)).thenReturn(collectivite2022);
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee("987654321", 2023)).thenReturn(collectivite2023);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 12, 15))
            .dateFinSejour(LocalDate.of(2023, 1, 15))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(2)
            .nombreNuits(17)
            .categorieLogement(13)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(4)
            .build();

        final Double actualTaxe1 = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite2022);

        // then assert
        assertEquals(111.6, actualTaxe1);
    }

    @DisplayName("Exception si nombre de voyageurs est vide")
    void testNombreDeVoyageursAbsent() throws CalculTaxeSejourImpossibleException
    {
        final CollectiviteDto collectivite1 = CollectiviteDto.builder()
            .siren("987654321")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .codeInsee("75056")
            .taxeAdditionnelleDepartementale(true)
            .taxeAdditionnelleRegionale(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren("123456789")
            .dateDebut(LocalDate.of(2022, 01, 01))
            .dateFin(LocalDate.of(2022, 12, 31))
            .tarifPourcentageCategorie19(4.0)
            .tarifsFixesCategories(Arrays.asList(3.64, 2.28, 1.82, 1.37, 0.82, 0.73, 0.55, 0.2))
            .build();

        final List<String> regimesTaxation = new ArrayList<>(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()));
        regimesTaxation.add(RegimeTaxation.REGIME_NON_ATTACHE.getLabel());
        regimesTaxation.add(RegimeTaxation.REGIME_FORFAITAIRE.getLabel());
        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren("123456789")
            .annee("2022")
            .nom("valloire")
            .codePostal("02234")
            .listeDeCollectivites(Arrays.asList(collectivite1))
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(regimesTaxation)
            .build();

        collectivite1.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("987654321", 2022)).thenReturn("123456789");
        when(collectiviteDeliberanteService.getDeliberanteFromSirenAndAnnee("123456789", 2022)).thenReturn(collectiviteDeliberante);

        final SejourAnalyse sejour1 = SejourAnalyse.builder()
            .ville("valloire")
            .codePostal("02234")
            .dateDebutSejour(LocalDate.of(2022, 5, 21))
            .datePerception(LocalDate.of(2022, 5, 22))
            .collectiviteDeliberanteSiren("123456789")
            .collectiviteDeliberanteNom("valloire")
            .nombreVoyageurs(null)
            .nombreNuits(5)
            .categorieLogement(null)
            .prixParNuitee(null)
            .montantTotalTaxeSejour(27.12)
            .natureLogement(2)
            .build();

        // when case : categorie_du_logement est vide et prix_par_nuitee est vide
        Assertions.assertThrows(CalculTaxeSejourImpossibleException.class,
            () -> moteurDeCalculDeLaTaxeDeSejour.calculer(sejour1, collectivite1));
    }

    @Test
    @DisplayName("Taxe IDFM applicable car ocsitan >= 2024 et département concerné")
    void testCalculerTaxeIdfmApplicableAPartirDe2024() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2024")
            .nom("CLICHY")
            .codePostal("92110")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2024, 01, 01))
            .dateFin(LocalDate.of(2024, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2024")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2024)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(4)
            .dateDebutSejour(LocalDate.of(2024, 3, 1))
            .dateFinSejour(LocalDate.of(2024, 3, 11))
            .ville("CLICHY")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuitee(90.0)
            .categorieLogement(19)
            .nombreNuits(10)
            .natureLogement(4)
            .codePostal("92110")
            .montantTotalTaxeSejour(100.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(480.0, taxe);
    }

    @Test
    @DisplayName("Taxe IDFM non applicable car ocsitan >= 2024 et département non concerné")
    void testCalculerTaxeIdfmNonApplicableAPartirDe2024() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2024")
            .nom("MIREPOIX-SUR-TARN")
            .codePostal("31340")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2024, 01, 01))
            .dateFin(LocalDate.of(2024, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2024")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2024)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(4)
            .dateDebutSejour(LocalDate.of(2024, 3, 1))
            .dateFinSejour(LocalDate.of(2024, 3, 11))
            .ville("MIREPOIX-SUR-TARN")
            .codePostal("31340")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuitee(90.0)
            .categorieLogement(19)
            .nombreNuits(10)
            .natureLogement(4)
            .montantTotalTaxeSejour(100.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(160.0, taxe);
    }

    @Test
    @DisplayName("Taxe IDFM non applicable car ocsitan < 2024")
    void testCalculerTaxeIdfmNonApplicable() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2023")
            .nom("MIREPOIX-SUR-TARN")
            .codePostal("31340")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2023, 01, 01))
            .dateFin(LocalDate.of(2023, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2023")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2023)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(4)
            .dateDebutSejour(LocalDate.of(2023, 3, 1))
            .dateFinSejour(LocalDate.of(2023, 3, 11))
            .ville("MIREPOIX-SUR-TARN")
            .codePostal("31340")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuitee(90.0)
            .categorieLogement(19)
            .nombreNuits(10)
            .natureLogement(4)
            .montantTotalTaxeSejour(100.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(160.0, taxe);
    }

    @Test
    @DisplayName("Taxe IDFM applicable car ocsitan >= 2024 et chargée en bdd")
    void testCalculerTaxeIdfmApplicable() throws CalculTaxeSejourImpossibleException
    {
        Double taxe = null;

        final CollectiviteDto collectivite = CollectiviteDto.builder()
            .siren("218301497")
            .annee("2024")
            .nom("CLICHY")
            .codePostal("92110")
            .taxeAdditionnelleDepartementale(false)
            .taxeAdditionnelleRegionale(false)
            .taxeAdditionnelleGrandeVitesse(false)
            .taxeAdditionnelleIdfm(true)
            .build();

        final PeriodeTarifaireDto periodeTarifaire = PeriodeTarifaireDto.builder()
            .siren(SIREN_DELIBERANTE)
            .dateDebut(LocalDate.of(2024, 01, 01))
            .dateFin(LocalDate.of(2024, 12, 31))
            .tarifPourcentageCategorie19(5.0)
            .tarifsFixesCategories(Arrays.asList(4.0, 3.0, 2.3, 1.5, 0.9, 0.8, 0.6, 0.2))
            .build();

        final CollectiviteDeliberanteDto collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
            .siren(SIREN_DELIBERANTE)
            .annee("2024")
            .nom("CC LACS ET GORGES DU VERDON")
            .codePostal("83630")
            .listePeriodesTarifaires(Arrays.asList(periodeTarifaire))
            .regimesTaxation(Collections.nCopies(9, RegimeTaxation.REGIME_REEL.getLabel()))
            .listeDeCollectivites(Arrays.asList(collectivite))
            .build();

        collectivite.setCollectiviteDeliberante(collectiviteDeliberante);

        when(collectiviteService.getDeliberanteFromSirenCommuneAndAnnee("218301497", 2024)).thenReturn(SIREN_DELIBERANTE);
        when(periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(Mockito.anyString(), Mockito.any(LocalDate.class),
            Mockito.any(LocalDate.class))).thenReturn(Arrays.asList(periodeTarifaire));
        when(collectiviteService.trouverCollectiviteParSirenEtAnnee(anyString(), anyInt())).thenReturn(collectivite);

        final SejourAnalyse sejour = SejourAnalyse.builder()
            .nombreVoyageurs(4)
            .dateDebutSejour(LocalDate.of(2024, 3, 1))
            .dateFinSejour(LocalDate.of(2024, 3, 11))
            .ville("CLICHY")
            .sirenCommune("218301497")
            .sirenCollectiviteDeliberante(SIREN_DELIBERANTE)
            .prixParNuitee(90.0)
            .categorieLogement(19)
            .nombreNuits(10)
            .natureLogement(4)
            .codePostal("92110")
            .montantTotalTaxeSejour(100.4)
            .build();

        taxe = moteurDeCalculDeLaTaxeDeSejour.calculer(sejour, collectivite);

        assertInstanceOf(Double.class, taxe);
        assertEquals(480.0, taxe);
    }
}
