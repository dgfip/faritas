package fr.gouv.finances.faritas_os.client;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.client.WebClientResponseException.NotFound;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

@DirtiesContext
public class ApiAdresseHttpClientTest
{
    public static MockWebServer mockBackEnd;

    private static String REP_COMPLETE =
        "{\"type\":\"FeatureCollection\",\"version\":\"draft\",\"features\":[" +
            "{\"type\":\"Feature\",\"geometry\":{\"type\":\"Point\",\"coordinates\":[2.347687,48.848881]}," +
            "\"properties\":{\"label\":\"Rue des Ecoles 75005 Paris\",\"score\":0.7707361157024794,\"id\":\"75105_3098\"," +
            "\"name\":\"Rue des Ecoles\",\"postcode\":\"75005\",\"citycode\":\"75105\",\"x\":652130.81,\"y\":6861179.7," +
            "\"city\":\"Paris\",\"district\":\"Paris 5e Arrondissement\",\"context\":\"75, Paris, Île-de-France\"," +
            "\"type\":\"street\",\"importance\":0.70537,\"street\":\"Rue des Ecoles\"}}],\"attribution\":\"BAN\"," +
            "\"licence\":\"ETALAB-2.0\",\"query\":\"32 rue des ecoles paris\"," +
            "\"filters\":{\"postcode\":\"75005\",\"type\":\"street\"},\"limit\":1}";

    private static String REP_VILLE =
        "{\"type\":\"FeatureCollection\",\"version\":\"draft\",\"features\":[{\"type\":\"Feature\",\"geometry\":{" +
            "\"type\":\"Point\",\"coordinates\":[2.347,48.859]},\"properties\":{\"label\":\"Paris\",\"score\":0.9703654545454544," +
            "\"id\":\"75056\",\"type\":\"municipality\",\"name\":\"Paris\",\"postcode\":\"75001\",\"citycode\":\"75056\"," +
            "\"x\":652089.7,\"y\":6862305.26,\"population\":2145906,\"city\":\"Paris\",\"context\":\"75, Paris, Île-de-France\"," +
            "\"importance\":0.67402,\"municipality\":\"Paris\"}},{\"type\":\"Feature\",\"geometry\":{\"type\":\"Point\"," +
            "\"coordinates\":[2.350554,48.845277]},\"properties\":{\"label\":\"Paris 5e Arrondissement\",\"score\":0.8629945454545455," +
            "\"id\":\"75105\",\"type\":\"municipality\",\"name\":\"Paris 5e Arrondissement\",\"postcode\":\"75005\"," +
            "\"citycode\":\"75105\",\"x\":652337.9,\"y\":6860777.24,\"population\":57380,\"city\":\"Paris 5e Arrondissement\"," +
            "\"context\":\"75, Paris, Île-de-France\",\"importance\":0.49294,\"municipality\":\"Paris 5e Arrondissement\"}}]," +
            "\"attribution\":\"BAN\",\"licence\":\"ETALAB-2.0\",\"query\":\"paris\"," +
            "\"filters\":{\"postcode\":\"75005\",\"type\":\"municipality\"},\"limit\":3}";

    private static String REP_QUERY = "{" +
        "\"type\": \"FeatureCollection\"," +
        "\"version\": \"draft\"," +
        "\"features\": [{" +
        "\"type\": \"Feature\"," +
        "\"geometry\": {\"type\": \"Point\",\"coordinates\": [2.347,48.859]}," +
        "\"properties\": {" +
        "\"label\": \"Paris\",\"score\": 0.7306960330578512,\"id\": \"75056\",\"type\": \"municipality\"," +
        "\"name\": \"Paris\",\"postcode\": \"75001\",\"citycode\": \"75056\",\"x\": 652089.7,\"y\": 6862305.26," +
        "\"population\": 2145906,\"city\": \"Paris\",\"context\": \"75, Paris, Île-de-France\"," +
        "\"importance\": 0.67402,\"municipality\": \"Paris\"}}]," +
        "\"attribution\": \"BAN\",\"licence\": \"ETALAB-2.0\"," +
        "\"query\": \"Paris 75005\",\"filters\": {\"type\": \"municipality\"},\"limit\": 1}";

    private ApiAdresseHttpClient apiClient;

    @BeforeAll
    public static void setUp() throws IOException
    {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }

    @AfterAll
    public static void tearDown() throws IOException
    {
        mockBackEnd.shutdown();
    }

    @BeforeEach
    public void initialize()
    {
        final String baseUrl = String.format("http://localhost:%s", mockBackEnd.getPort());
        apiClient = new ApiAdresseHttpClient(baseUrl);

        ReflectionTestUtils.setField(apiClient, "searchUrl", baseUrl + "/search/?q=");
    }

    @Test
    void testRechercheAdresseCompleteOK()
    {
        // Arrange
        mockBackEnd.enqueue(new MockResponse()
            .setBody(REP_COMPLETE)
            .addHeader("Content-Type", "application/json"));

        // Act
        final APIAdresseResponse reponse = apiClient.searchByFullAddress("32 rue des écoles", "Paris", "75005").block();

        // Assert
        assertThat(reponse).isNotNull();
        assertThat(reponse.getFeatures().get(0).getProperties().getCity()).isEqualTo("Paris");
        assertThat(reponse.getFeatures().get(0).getProperties().getPostcode()).isEqualTo("75005");
        assertThat(reponse.getFeatures().get(0).getProperties().getName()).isEqualTo("Rue des Ecoles");
    }

    @Test
    void testRechercheVilleCodePostalOK() throws IOException
    {
        // Arrange
        mockBackEnd.enqueue(new MockResponse()
            .setBody(REP_VILLE)
            .addHeader("Content-Type", "application/json"));
        // Act
        final APIAdresseResponse reponse = apiClient.searchByCityAndPostCode("Paris", "75005").block();

        // Assert
        assertThat(reponse).isNotNull();
        assertThat(reponse.getFeatures().get(0).getProperties().getCity()).isEqualTo("Paris");
        assertThat(reponse.getFeatures().get(0).getProperties().getPostcode()).isEqualTo("75001");
    }

    @Test
    void testRechercheQueryOK()
    {
        // Arrange
        mockBackEnd.enqueue(new MockResponse()
            .setBody(REP_QUERY)
            .addHeader("Content-Type", "application/json"));

        // Act
        final APIAdresseResponse reponse = apiClient.searchByQuery("Paris 75005").block();

        // Assert
        assertThat(reponse).isNotNull();
        assertThat(reponse.getFeatures().get(0).getProperties().getCity()).isEqualTo("Paris");
        assertThat(reponse.getFeatures().get(0).getProperties().getPostcode()).isEqualTo("75001");
    }

    @Test
    void testRechercheQueryKO()
    {
        // Arrange
        mockBackEnd.enqueue(new MockResponse()
            .setResponseCode(404));

        APIAdresseResponse reponse = null;

        try
        {
            // Act
            reponse = apiClient.searchByQuery("Paris 85005").block();
        }
        catch (final Exception e)
        {
            // Assert
            assertThat(reponse).isNull();
            assertThat(e).isNotNull();
            assertThat(e).isInstanceOf(WebClientResponseException.class);
            assertThat(e).isInstanceOf(NotFound.class);
        }
    }

    @Test
    void testRechercheQueryLimit()
    {
        // Arrange
        final MockResponse mockResponse = new MockResponse()
            .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .setResponseCode(200)
            .setBody(REP_COMPLETE);
        // Prepare 60 responses
        for (int i = 0; i < 60; i++)
        {
            mockBackEnd.enqueue(mockResponse);
        }

        APIAdresseResponse reponse = null;

        // Call 60 times
        for (int i = 0; i < 60; i++)
        {
            // Act
            reponse = apiClient.searchByFullAddress(i + " rue de Provence", "Paris", "75005").block();
            // Assert
            assertThat(reponse).isNotNull();
        }
    }
}
