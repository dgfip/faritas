/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

class SejourDeclareValidationTest
{
    private Validator validator;

    @BeforeEach
    public void setup()
    {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    void testSejourEmpty()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("")
            .dateFinSejour("")
            .datePerception("")
            .adresse("")
            .codePostal("")
            .ville("")
            .sirenCommune("")
            .sirenCollectiviteDeliberante("")
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertFalse(violations.isEmpty());
        assertThat(violations).hasSize(18);
        assertThat(violations).extracting(ConstraintViolation::getMessage)
            .containsExactlyInAnyOrder(
                "La date de début de séjour est obligatoire",
                "La date de début de séjour est incorrecte",
                "La date de fin de séjour est obligatoire",
                "La date de fin de séjour est incorrecte",
                "La date de perception est obligatoire",
                "La date de perception est incorrecte",
                "Le code postal est obligatoire",
                "Le code postal est incorrect",
                "La ville est obligatoire",
                "Le SIREN de la commune est obligatoire",
                "Le SIREN de la commune est incorrect",
                "Le SIREN de la collectivité délibérante est obligatoire",
                "Le SIREN de la collectivité délibérante est incorrect",
                "La nature du logement est obligatoire",
                "La catégorie du logement est obligatoire",
                "Le nombre de nuits est obligatoire",
                "Le nombre de voyageurs est obligatoire",
                "Le montant total de la taxe de séjour est obligatoire");
    }

    @Test
    void testSejourValid()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("28/02/2024")
            .datePerception("05/02/2024")
            .adresse("22 Route de Carquefou")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertTrue(violations.isEmpty());
    }

    @Test
    void testSejourValidZipCodeLowError()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("28/02/2024")
            .datePerception("05/02/2024")
            .adresse("22 Route de Carquefou")
            .codePostal("00211")
            .ville("Sucé-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertFalse(violations.isEmpty());
        assertThat(violations).hasSize(1);
        assertThat(violations).extracting(ConstraintViolation::getMessage)
            .containsExactlyInAnyOrder("Le code postal est incorrect");
    }

    @Test
    void testSejourValidZipCodeHighError()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("28/02/2024")
            .datePerception("05/02/2024")
            .adresse("22 Route de Carquefou")
            .codePostal("99211")
            .ville("La CHapelle-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertFalse(violations.isEmpty());
        assertThat(violations).hasSize(1);
        assertThat(violations).extracting(ConstraintViolation::getMessage)
            .containsExactlyInAnyOrder("Le code postal est incorrect");
    }

    @Test
    void testSejourValidZipCodeRegexError()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/02/2024")
            .dateFinSejour("28/02/2024")
            .datePerception("05/02/2024")
            .adresse("22 Route de Carquefou")
            .codePostal("99 21")
            .ville("La CHapelle-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertFalse(violations.isEmpty());
        assertThat(violations).hasSize(1);
        assertThat(violations).extracting(ConstraintViolation::getMessage)
            .containsExactlyInAnyOrder("Le code postal est incorrect");
    }

    @DisplayName("Erreur d'ordre des dates du séjour")
    @Test
    void testSejourDateReverse()
    {
        final SejourDeclare sejour = SejourDeclare.builder()
            .dateDebutSejour("05/03/2024")
            .dateFinSejour("28/02/2024")
            .datePerception("05/02/2024")
            .adresse("22 Route de Carquefou")
            .codePostal("44240")
            .ville("Sucé-sur-Erdre")
            .sirenCommune("214403018")
            .sirenCollectiviteDeliberante("244500503")
            .natureLogement(5)
            .categorieLogement(12)
            .nombreNuits(5)
            .nombreVoyageurs(3)
            .nombreVoyageursExoneresMineurs(1)
            .prixParNuit(90.0)
            .montantTotalTaxeSejour(102.0)
            .build();

        final Set<ConstraintViolation<SejourDeclare>> violations = validator.validate(sejour);

        assertFalse(violations.isEmpty());
        assertThat(violations).hasSize(1);
        assertThat(violations).extracting(ConstraintViolation::getMessage)
            .containsExactlyInAnyOrder(
                "La date de début de séjour est après la date de fin de séjour");
    }
}
