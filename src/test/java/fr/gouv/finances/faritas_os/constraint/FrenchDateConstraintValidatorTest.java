/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.constraint;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FrenchDateConstraintValidatorTest
{
    private final FrenchDateConstraintValidator validator = new FrenchDateConstraintValidator();

    @DisplayName("Should return true when date is null")
    @Test
    void isValid_shouldReturnTrue_whenDateIsNull()
    {
        assertTrue(validator.isValid(null, null));
    }

    @DisplayName("Should return false when date is empty")
    @Test
    void isValid_shouldReturnFalse_whenDateIsEmpty()
    {
        assertFalse(validator.isValid("", null));
    }

    @DisplayName("Should return false when date doesn't match REGEXP")
    @Test
    void isValid_shouldReturnFalse_whenDateNotMatchRegex()
    {
        assertFalse(validator.isValid("15-55-4545", null));
    }

    @DisplayName("Should return false when date is too far is past")
    @Test
    void isValid_shouldReturnFalse_whenDateIsPast()
    {
        assertFalse(validator.isValid("25-12-2007", null));
    }

    @DisplayName("Should return false when date is too far in future")
    @Test
    void isValid_shouldReturnFalse_whenDateIsFuture()
    {
        final LocalDate now = LocalDate.now();
        final String dateStr =
            LocalDate.of(now.getYear() + 6, now.getMonth(), now.getDayOfMonth()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        assertFalse(validator.isValid(dateStr, null));
    }

    @DisplayName("Should return true when date is correct")
    @Test
    void isValid_shouldReturnTrue_whenDateIsCorrect()
    {
        final LocalDate now = LocalDate.now();
        final String dateStr = now.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        assertTrue(validator.isValid(dateStr, null));
    }
}
