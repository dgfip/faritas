/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.constraint;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FrenchZipCodeConstraintValidatorTest
{
    private final FrenchZipCodeConstraintValidator validator = new FrenchZipCodeConstraintValidator();

    @DisplayName("Should return false when ZIP is null")
    @Test
    void isValid_shouldReturnFalse_whenZipIsNull()
    {
        assertFalse(validator.isValid(null, null));
    }

    @DisplayName("Should return false when ZIP doesn't comply to REGEXP")
    @Test
    void isValid_shouldReturnFalse_whenZipIsNotInRegex()
    {
        assertFalse(validator.isValid("ABC", null));
    }

    @DisplayName("Should return false when ZIP is greater than MIN")
    @Test
    void isValid_shouldReturnFalse_whenZipIsLessThanMin()
    {
        assertFalse(validator.isValid("00254", null));
    }

    @DisplayName("Should return false when ZIP is greater than MAX")
    @Test
    void isValid_shouldReturnFalse_whenZipIsGreaterThanMax()
    {
        assertFalse(validator.isValid("99001", null));
    }

    @DisplayName("Should return true when ZIP is valid")
    @Test
    void isValid_shouldReturnTrue_whenZipIsCorrect()
    {
        assertTrue(validator.isValid("75001", null));
    }
}
