/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.utility;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class ParserUtilsTest
{
    @Test
    void testParseDecimalPoint()
    {
        assertEquals(15.5, ParserUtils.parseDecimal("15.5"));
    }

    @Test
    void testParseDecimalVirgule()
    {
        assertEquals(15.5, ParserUtils.parseDecimal("15,5"));
    }

    @Test
    void testParseDecimalErreur()
    {
        assertNull(ParserUtils.parseDecimal("A,B"));
    }

    @Test
    void testParseInteger()
    {
        assertEquals(15, ParserUtils.parseInteger("15"));
    }

    @Test
    void testParseIntegerErreur()
    {
        assertNull(ParserUtils.parseInteger("AB"));
    }

    @Test
    void testParseFrenchDateNull()
    {
        assertNull(ParserUtils.parseFrenchDate(null));
    }

    @Test
    void testParseFrenchDateVide()
    {
        assertNull(ParserUtils.parseFrenchDate(""));
    }

    @Test
    void testParseFrenchDateSlash()
    {
        final LocalDate frDate = ParserUtils.parseFrenchDate("15/07/2022");
        assertEquals(15, frDate.getDayOfMonth());
        assertEquals(7, frDate.getMonthValue());
        assertEquals(2022, frDate.getYear());
    }

    @Test
    void testParseFrenchDateErrorMois()
    {
        assertNull(ParserUtils.parseFrenchDate("07/15/2022"));
    }

    @Test
    void testParseFrenchDateTiret()
    {
        final LocalDate isoDate = ParserUtils.parseFrenchDate("2022-07-15");

        assertEquals(15, isoDate.getDayOfMonth());
        assertEquals(7, isoDate.getMonthValue());
        assertEquals(2022, isoDate.getYear());
    }

    @Test
    void testParseFrenchDateIsoErrorMois()
    {
        assertNull(ParserUtils.parseFrenchDate("2022-15-07"));
    }

    @Test
    void testParseFrenchDateErreur()
    {
        assertNull(ParserUtils.parseFrenchDate("17/12/98"));
    }

    @Test
    void testParseFrenchDateIso()
    {
        final LocalDate frDate = ParserUtils.parseFrenchDate("2022-07-15");
        assertEquals(15, frDate.getDayOfMonth());
        assertEquals(7, frDate.getMonthValue());
        assertEquals(2022, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel1()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("1 janvier 2024");
        assertEquals(1, frDate.getDayOfMonth());
        assertEquals(1, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel2()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("29 février 2024");
        assertEquals(29, frDate.getDayOfMonth());
        assertEquals(2, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel3()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("15 juillet 2024");
        assertEquals(15, frDate.getDayOfMonth());
        assertEquals(7, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel4()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("8 août 2024");
        assertEquals(8, frDate.getDayOfMonth());
        assertEquals(8, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel6()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("31 Décembre 2024");
        assertEquals(31, frDate.getDayOfMonth());
        assertEquals(12, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabel7()
    {
        final LocalDate frDate = ParserUtils.parseFrenchLabelDate("31 avril 2024");
        assertEquals(30, frDate.getDayOfMonth());
        assertEquals(4, frDate.getMonthValue());
        assertEquals(2024, frDate.getYear());
    }

    @Test
    void testParseFrenchDateLabelLocale()
    {
        assertNull(ParserUtils.parseFrenchLabelDate("31 may 2024"));
    }

    @Test
    void testParseFrenchDateLabelNull()
    {
        assertNull(ParserUtils.parseFrenchLabelDate(null));
    }

    @Test
    void testParseFrenchDateLabelVide()
    {
        assertNull(ParserUtils.parseFrenchLabelDate(""));
    }

    @Test
    void testParseValueOk()
    {
        assertEquals("sainte Marie", ParserUtils.parseValue(FaritasUtils.remplacerSteParSainte, "Ste Marie"));
    }

    @Test
    void testParseValueNoChange()
    {
        assertEquals("sainte Marie", ParserUtils.parseValue(FaritasUtils.remplacerSteParSainte, "sainte Marie"));
    }

    @Test
    void testParseValueNull()
    {
        assertNull(ParserUtils.parseValue(FaritasUtils.remplacerSteParSainte, null));
    }

    @Test
    void testParseValueVide()
    {
        assertNull(ParserUtils.parseValue(FaritasUtils.remplacerSteParSainte, ""));
    }
}
