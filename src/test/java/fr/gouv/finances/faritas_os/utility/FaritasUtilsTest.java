/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.utility;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import fr.gouv.finances.faritas_os.dto.AnomalieValideEnum;
import jakarta.ws.rs.core.Response;

class FaritasUtilsTest
{
    @Test
    void testNettoyerCommuneDe()
    {
        assertEquals("Paris", FaritasUtils.nettoyer("Commune de Paris"));
    }

    @Test
    void testNettoyerSt()
    {
        assertEquals("saintDenis", FaritasUtils.nettoyer("St Denis"));
    }

    @Test
    void testNettoyerSte()
    {
        assertEquals("sainteMarie", FaritasUtils.nettoyer("Ste Marie"));
    }

    @Test
    void testNettoyerTiret()
    {
        assertEquals("NortsurErdre", FaritasUtils.nettoyer("Nort-sur-Erdre"));
    }

    @Test
    void testNettoyerPoint()
    {
        assertEquals("VillePoint", FaritasUtils.nettoyer("Ville.Point"));
    }

    @Test
    void testNettoyerApostrophe()
    {
        assertEquals("Ploumanach", FaritasUtils.nettoyer("Ploumanac'h"));
    }

    @Test
    void testNettoyerAccents()
    {
        assertEquals("SucesurErdre", FaritasUtils.nettoyer("Sucé-sur-Erdre"));
    }

    @Test
    void testFormatCodePostalNum()
    {
        assertEquals("01500", FaritasUtils.formatCodePostal("1500"));
    }

    @Test
    void testFormatCodePostalAlpha()
    {
        assertEquals("B500", FaritasUtils.formatCodePostal("B500"));
    }

    @Test
    void testIsFrenchDateFormatNull()
    {
        assertFalse(FaritasUtils.isFrenchDateFormat(null));
    }

    @Test
    void testIsFrenchDateFormatISO()
    {
        assertFalse(FaritasUtils.isFrenchDateFormat("2020-01-15"));
    }

    @Test
    void testIsFrenchDateFormatFr()
    {
        assertTrue(FaritasUtils.isFrenchDateFormat("23/05/2010"));
    }

    @Test
    void testFormatTaxe()
    {
        assertEquals(12.56, FaritasUtils.formatTaxe(12.56));
    }

    @Test
    void testFormatPriceNull()
    {
        assertEquals("", FaritasUtils.formatPrice(null));
    }

    @Test
    void testFormatPriceValid()
    {
        assertEquals("12,56", FaritasUtils.formatPrice(12.56));
    }

    @Test
    void testFormatDateNull()
    {
        assertEquals("", FaritasUtils.formatDate(null));
    }

    @Test
    void testFormatDateValid()
    {
        final LocalDate dt = LocalDate.of(2024, 05, 15);
        assertEquals("15/05/2024", FaritasUtils.formatDate(dt));
    }

    @Test
    void testFormatHeureDepotOk()
    {
        assertEquals("18:25:05.00", FaritasUtils.formatHeureDeDepot(LocalDateTime.of(2023, 4, 12, 18, 25, 5)));
    }

    @Test
    void testFormatHeureDepotKo()
    {
        assertEquals("", FaritasUtils.formatHeureDeDepot(null));
    }

    @Test
    void testFormatPeriodeOk()
    {
        assertEquals("S1-2023", FaritasUtils.formatPeriode("2023-S1"));
    }

    @Test
    void testFormatPeriodeNull()
    {
        assertEquals("", FaritasUtils.formatPeriode(null));
    }

    @Test
    void testFormatPeriodeVide()
    {
        assertEquals("", FaritasUtils.formatPeriode(""));
    }

    @Test
    void testCleanFromNullforNull()
    {
        assertEquals(0, FaritasUtils.cleanFromNull(null));
    }

    @Test
    void testCleanFromNullforTen()
    {
        assertEquals(10, FaritasUtils.cleanFromNull(10));
    }

    @Test
    void testSirenValideForNull()
    {
        assertThrows(IllegalArgumentException.class, (() -> {
            FaritasUtils.isSirenValide(null);
        }));
    }

    @Test
    void testSirenValideForVide()
    {
        assertThrows(IllegalArgumentException.class, (() -> {
            FaritasUtils.isSirenValide("");
        }));
    }

    @Test
    void testSirenValideForString()
    {
        assertThrows(IllegalArgumentException.class, (() -> {
            FaritasUtils.isSirenValide("abc");
        }));
    }

    @Test
    void testSirenValideForSiren()
    {
        assertDoesNotThrow(() -> {
            FaritasUtils.isSirenValide("214420015");
        });
    }

    @Test
    void testBadRequestResponseForIllegallArgumentException()
    {
        final Response rep = FaritasUtils.getBadRequestResponse(new IllegalArgumentException("Erreur"));
        assertEquals("Erreur", rep.getEntity());
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), rep.getStatus());
    }

    @Test
    void testFormatMessageAnomalie()
    {
        final AnomalieDto ano = AnomalieDto.creerAnomalie(AnomalieValideEnum.RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE);

        assertEquals("rattachement incertain a cette deliberante", FaritasUtils.formatMessageAnomalie(ano));
    }
}
