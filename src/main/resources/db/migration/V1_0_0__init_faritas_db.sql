-- =============================== --
-- Table: collectivite_deliberante --
-- =============================== --

DROP TABLE IF EXISTS collectivite_deliberante CASCADE;

CREATE TABLE IF NOT EXISTS collectivite_deliberante
(
    annee char(4) NOT NULL,
    siren char(9) NOT NULL,
    nom varchar NOT NULL,
    code_postal char(5) NOT NULL,
    regime_taxation_par_nature varchar,
    CONSTRAINT collectivite_deliberante_pk PRIMARY KEY (annee, siren)
);

COMMENT ON TABLE collectivite_deliberante
    IS 'Table des délibérations annuelles des collectivités délibérantes';

COMMENT ON COLUMN collectivite_deliberante.annee
    IS 'Année de la délibération de la collectivité déliberante';

COMMENT ON COLUMN collectivite_deliberante.siren
    IS 'SIREN de la collectivité déliberante';

COMMENT ON COLUMN collectivite_deliberante.nom
    IS 'Nom de la collectivité déliberante';

COMMENT ON COLUMN collectivite_deliberante.code_postal
    IS 'Code postal de la collectivité déliberante';

COMMENT ON COLUMN collectivite_deliberante.regime_taxation_par_nature
    IS 'Liste des régimes de taxation de la délibération';

-- Index: collectivite_deliberante_annee_siren_uidx

DROP INDEX IF EXISTS collectivite_deliberante_annee_siren_uidx;

CREATE UNIQUE INDEX IF NOT EXISTS collectivite_deliberante_annee_siren_uidx
    ON collectivite_deliberante USING btree
    (annee ASC NULLS LAST, siren ASC NULLS LAST);

-- Index: collectivite_deliberante_siren_idx

DROP INDEX IF EXISTS collectivite_deliberante_siren_idx;

CREATE INDEX IF NOT EXISTS collectivite_deliberante_siren_idx
    ON collectivite_deliberante USING btree
    (siren ASC NULLS LAST)
    TABLESPACE pg_default;

-- =================== --
-- Table: collectivite --
-- =================== --

DROP TABLE IF EXISTS collectivite;

CREATE TABLE IF NOT EXISTS collectivite
(
    annee char(4) NOT NULL,
    siren char(9) NOT NULL,
    collectivite_deliberante_annee char(4) NOT NULL,
    collectivite_deliberante_siren char(9) NOT NULL,
    nom varchar,
    code_postal char(5),
    code_insee character varying(10),
    taxe_departementale_applicable boolean,
    taxe_regionale_applicable boolean,
    taxe_grande_vitesse_applicable boolean,
    taxe_idfm_applicable boolean,
    CONSTRAINT collectivite_pk PRIMARY KEY (siren, annee),
    CONSTRAINT collectivite_collectivite_deliberante_fk FOREIGN KEY (collectivite_deliberante_annee, collectivite_deliberante_siren)
        REFERENCES collectivite_deliberante (annee, siren) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

COMMENT ON TABLE collectivite
    IS 'Table des collectivités rattachées à une collectivité délibérante pour une année';

COMMENT ON COLUMN collectivite.annee
    IS 'Année de rattachement de la collectivité';

COMMENT ON COLUMN collectivite.siren
    IS 'SIREN de la collectivité';

COMMENT ON COLUMN collectivite.collectivite_deliberante_annee
    IS 'Année de rattachement à la collectivité déliberante';

COMMENT ON COLUMN collectivite.collectivite_deliberante_siren
    IS 'SIREN de la collectivité déliberante de rattachement';

COMMENT ON COLUMN collectivite.nom
    IS 'Nom de la collectivité';

COMMENT ON COLUMN collectivite.code_postal
    IS 'Code postal de la collectivité';

COMMENT ON COLUMN collectivite.code_insee
    IS 'Code INSEE de la collectivité';

COMMENT ON COLUMN collectivite.taxe_departementale_applicable
    IS 'Indicateur d''application de la taxe départementale';

COMMENT ON COLUMN collectivite.taxe_regionale_applicable
    IS 'Indicateur d''application de la taxe régionale';

COMMENT ON COLUMN collectivite.taxe_grande_vitesse_applicable
    IS 'Indicateur d''application de la taxe ligne à grande vitesse';

COMMENT ON COLUMN collectivite.taxe_idfm_applicable
    IS 'Indicateur d''application de la taxe Ile de France Mobilité';

-- Index: collectivite_annee_code_insee_idx

DROP INDEX IF EXISTS collectivite_annee_code_insee_idx;

CREATE INDEX IF NOT EXISTS collectivite_annee_code_insee_idx
    ON collectivite USING btree
    (annee ASC NULLS LAST, code_insee ASC NULLS LAST);

-- Index: collectivite_annee_code_postal_idx

DROP INDEX IF EXISTS collectivite_annee_code_postal_idx;

CREATE INDEX IF NOT EXISTS collectivite_annee_code_postal_idx
    ON collectivite USING btree
    (annee ASC NULLS LAST, code_postal ASC NULLS LAST);

-- Index: collectivite_annee_deliberante_idx

DROP INDEX IF EXISTS collectivite_annee_deliberante_idx;

CREATE INDEX IF NOT EXISTS collectivite_annee_deliberante_idx
    ON collectivite USING btree
    (annee ASC NULLS LAST, collectivite_deliberante_siren ASC NULLS LAST);

-- Index: collectivite_annee_nom_idx

DROP INDEX IF EXISTS collectivite_annee_nom_idx;

CREATE INDEX IF NOT EXISTS collectivite_annee_nom_idx
    ON collectivite USING btree
    (annee ASC NULLS LAST, nom ASC NULLS LAST);

-- Index: collectivite_annee_siren_uidx

DROP INDEX IF EXISTS collectivite_annee_siren_uidx;

CREATE UNIQUE INDEX IF NOT EXISTS collectivite_annee_siren_uidx
    ON collectivite USING btree
    (annee ASC NULLS LAST, siren ASC NULLS LAST);


-- ======================== --
-- Table: periode_tarifaire --
-- ======================== --

DROP TABLE IF EXISTS periode_tarifaire;

CREATE TABLE IF NOT EXISTS periode_tarifaire
(
    siren char(9) NOT NULL,
    date_debut date NOT NULL,
    date_fin date NOT NULL,
    tarif_pourcentage_categorie_19 numeric,
    tarifs_fixes_categories varchar,
    collectivite_deliberante_annee char(4) NOT NULL,
    collectivite_deliberante_siren char(9) NOT NULL,
    CONSTRAINT tarif_pk PRIMARY KEY (siren, date_debut),
    CONSTRAINT periode_tarifaire_collectivite_deliberante_fk FOREIGN KEY (collectivite_deliberante_annee, collectivite_deliberante_siren)
        REFERENCES collectivite_deliberante (annee, siren) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

COMMENT ON TABLE periode_tarifaire
    IS 'Table des périodes tarifaires des collectivités délibérantes par période';

COMMENT ON COLUMN periode_tarifaire.siren
    IS 'SIREN de la collectivité déliberante';

COMMENT ON COLUMN periode_tarifaire.date_debut
    IS 'Date début du tarif inclus';

COMMENT ON COLUMN periode_tarifaire.date_fin
    IS 'Date fin du tarif inclus';

COMMENT ON COLUMN periode_tarifaire.tarif_pourcentage_categorie_19
    IS 'Pourcentage de taxe pour la catégories 19';

COMMENT ON COLUMN periode_tarifaire.tarifs_fixes_categories
    IS 'Montants fixes de taxe pour les catégories 11 à 18';

COMMENT ON COLUMN periode_tarifaire.collectivite_deliberante_siren
    IS 'SIREN de la collectivité déliberante';

COMMENT ON COLUMN periode_tarifaire.collectivite_deliberante_annee
    IS 'Année de la collectivité déliberante';

-- Index: periode_tarifaire_collectivite_date_uidx

DROP INDEX IF EXISTS periode_tarifaire_collectivite_date_uidx;

CREATE UNIQUE INDEX IF NOT EXISTS periode_tarifaire_collectivite_date_uidx
    ON periode_tarifaire USING btree
    (siren ASC NULLS LAST, date_debut ASC NULLS LAST, date_fin ASC NULLS LAST);
