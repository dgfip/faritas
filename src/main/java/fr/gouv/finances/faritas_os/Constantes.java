/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os;

public final class Constantes
{
    /** Constante nom application */
    public static final String APPLI = "FARITAS";

    public static final int TAXE_LGV_ANNEE_DEBUT = 2023;

    public static final int TAXE_IDFM_ANNEE_DEBUT = 2024;

    public static final String SEPARATEUR_VIRGULE = ",";

    public static final String TARIF_SEPARATOR = ";";

    public static final String REGIME_SANS_TARIF = "Non rattachée à un tarif";

    public static final String REGIME_FORFAITAIRE = "Forfaitaire";

    public static final int CATEGORIE_19 = 19;

    public static final int PREMIERE_CATEGORIE = 11;

    public static final double TAXE_NEUTRE = 0.0;

    public static final int CENT = 100;

    public static final int NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE = 2;

    public static final double DIFFERENCE_AUTORISEE = 0.1;

    public static final int MILLESIME_2018_CATEGORIE_1 = 1;

    public static final int MILLESIME_2018_CATEGORIE_2 = 2;

    public static final int MILLESIME_2018_CATEGORIE_3 = 3;

    public static final int MILLESIME_2018_CATEGORIE_4 = 4;

    public static final int MILLESIME_2018_CATEGORIE_5 = 5;

    public static final int MILLESIME_2018_CATEGORIE_6 = 6;

    public static final int MILLESIME_2018_CATEGORIE_7 = 7;

    public static final int MILLESIME_2018_CATEGORIE_8 = 8;

    public static final int MILLESIME_2018_CATEGORIE_9 = 9;

    public static final int MILLESIME_2018_CATEGORIE_10 = 10;

    public static final int MILLESIME_2019_CATEGORIE_11 = 11;

    public static final int MILLESIME_2019_CATEGORIE_12 = 12;

    public static final int MILLESIME_2019_CATEGORIE_13 = 13;

    public static final int MILLESIME_2019_CATEGORIE_14 = 14;

    public static final int MILLESIME_2019_CATEGORIE_15 = 15;

    public static final int MILLESIME_2019_CATEGORIE_16 = 16;

    public static final int MILLESIME_2019_CATEGORIE_17 = 17;

    public static final int MILLESIME_2019_CATEGORIE_18 = 18;

    public static final int MILLESIME_2019_CATEGORIE_19 = 19;

    public static final String ANNEE_2018 = "2018";

    /** UNE_ERREUR_EST_SURVENUE - String,. */
    public static final String UNE_ERREUR_EST_SURVENUE = "Une erreur est survenue";

    public static final String BAD_REQUEST = "bad_request";

    public static final String DTHR_DEPOT_FMT = "yyyyMMddHHmmss";

    public static final String FRENCH_DATE_PATTERN = "dd/MM/uuuu";

    public static final String FRENCH_DATE_REGEXP = "^\\d{2}\\/\\d{2}\\/\\d{4}$";

    public static final String FRENCH_ZIP_CODE_REGEXP = "^\\d{5}$";

    public static final String SIREN_REGEXP = "^\\d{9}$";

    /**
     * Instanciation de constantes
     */
    private Constantes()
    {
        // private constructor.
    }
}