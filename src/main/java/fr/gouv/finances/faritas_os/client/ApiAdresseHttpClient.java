/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.client;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * ApiAdresseHttpClient.java - Client d'appel à la BAN pou contrôler les adresses.
 */
@Component
@Slf4j
public class ApiAdresseHttpClient
{
    /**
     * Appels à l'API Adresses simultanés
     */
    private static final Integer NOMBRE_MAXIMUM_APPELS_SIMULTANES = 10;

    /**
     * Délai entre chaque appel à l'API Adresses
     */
    private static final Integer TEMPS_ATTENTE_MINIMUM_ENTRE_CHAQUE_APPEL = 30;

    /**
     * Interval du nombre d'appel, pour afficher un message de traitement en cours
     */
    private static final int MODULO_50 = 50;

    /**
     * 1000 ms.
     */
    private static final Integer ONE_SECONDE = 1000;

    private final AtomicInteger compteur;

    private final AtomicInteger compteurDeRequete;

    @Autowired
    private WebClient webClient;

    @Value("${ban.api.url}")
    private String searchUrl;

    /**
     * Constructeur de la classe ApiAdresseHttpClient.java
     */
    public ApiAdresseHttpClient()
    {
        compteur = new AtomicInteger(0);
        compteurDeRequete = new AtomicInteger(0);
    }

    /**
     * Constructeur de la classe ApiAdresseHttpClient.java
     *
     * @param url Url de l'API
     */
    public ApiAdresseHttpClient(String url)
    {
        compteur = new AtomicInteger(0);
        compteurDeRequete = new AtomicInteger(0);
        this.webClient = WebClient.create(url);
    }

    /**
     * Recherche de l'adresse au niveau ville
     *
     * @param ville Ville
     * @param codePostal Code postal
     * @return Résultat de recherche
     */
    public Mono<APIAdresseResponse> searchByCityAndPostCode(String ville, String codePostal)
    {
        final String query = ville.replace(" ", "+");
        return getData(searchUrl + query + "&postcode=" + codePostal + "&type=municipality&limit=3");
    }

    /**
     * Recherche de l'adresse au niveau rue
     *
     * @param adresse Adresse
     * @param ville Ville
     * @param codePostal Code postal
     * @return Résultat de recherche
     */
    public Mono<APIAdresseResponse> searchByFullAddress(String adresse, String ville, String codePostal)
    {
        final String query = (adresse + " " + ville).replace(" ", "+");
        return getData(searchUrl + query + "&postcode=" + codePostal + "&type=street&limit=1");
    }

    /**
     * Recherche de l'adresse au niveau ville
     *
     * @param query requète
     * @return Résultat de recherche
     */
    public Mono<APIAdresseResponse> searchByQuery(String query)
    {
        final String replacedQuery = query.replace(" ", "+");
        return getData(searchUrl + replacedQuery + "&type=municipality&limit=1");
    }

    /**
     * Recherche de l'adresse au niveau ville
     *
     * @param query requète
     * @return Résultat de recherche
     */
    public Mono<APIAdresseResponse> searchByQueryFull(String query)
    {
        final String replacedQuery = query.replace(" ", "+");
        return getData(searchUrl + replacedQuery + "&limit=3");
    }

    /**
     * Appel de l'API et traitement du résultat
     *
     * @param endpoint API
     * @return Résultat de la requète
     */
    private Mono<APIAdresseResponse> getData(String endpoint)
    {
        final LocalDateTime debutTraitement = LocalDateTime.now();
        waitForServiceAvailability();
        log.debug("Appel BAN {}", endpoint);

        return this.webClient.get().uri(endpoint).accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(APIAdresseResponse.class)
            .map(a -> {
                compteur.decrementAndGet();
                return a;
            })
            .onErrorResume(a -> {
                compteur.decrementAndGet();
                log.warn("Erreur dans la recherche adresse pour {} : {}. Durée de traitement : {}", endpoint, a.getMessage(),
                    Duration.between(debutTraitement, LocalDateTime.now()));
                return Mono.error(a);
            })
            .doFinally(signalType -> {
                log.debug("Fin Appel BAN {}. Durée de traitement {}", endpoint, Duration.between(debutTraitement, LocalDateTime.now()));
                // Toutes les 50 requêtes, on affiche le message
                if (compteurDeRequete.incrementAndGet() % MODULO_50 == 0)
                {
                    log.debug("Script toujours en cours d'exécution...");
                }
            });
    }

    /**
     * Nous n'avons pas encore le droit d'appeler l'API Adresse (BAN) autant de fois que nous voulons (en même temps)
     * Cette fonction nous permet donc d'espacer nos appels pour ne pas nous faire rejeter Le pré remplissage des
     * données OCSITAN se fait en un peu plus d'1h mais au moins, il se fait
     */
    private void waitForServiceAvailability()
    {
        while (compteur.get() >= NOMBRE_MAXIMUM_APPELS_SIMULTANES)
        {
            while (compteur.get() > NOMBRE_MAXIMUM_APPELS_SIMULTANES)
            {
                log.debug("waiting {} ", compteur.get());
                sleep(ONE_SECONDE);
            }
        }

        compteur.incrementAndGet();

        sleep(TEMPS_ATTENTE_MINIMUM_ENTRE_CHAQUE_APPEL);
    }

    /**
     * Pause
     *
     * @param tempsEnMillis Temps d'attente
     */
    private void sleep(Integer tempsEnMillis)
    {
        try
        {
            Thread.sleep(tempsEnMillis);
        }
        catch (final InterruptedException e)
        {
            log.warn("Interruption de la recherche d'adresse...", e);
            Thread.currentThread().interrupt();
            throw new ApiAdresseException("Interruption de la recherche d'adresse", e);
        }
    }
}
