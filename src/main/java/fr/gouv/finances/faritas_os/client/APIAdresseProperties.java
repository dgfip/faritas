/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.client;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
/**
 * APIAdresseProperties.java - Properties for a address
 */
public class APIAdresseProperties
{
    @JsonProperty
    String label;

    @JsonProperty
    Double score;

    @JsonProperty
    String id;

    @JsonProperty
    String type;

    @JsonProperty
    String name;

    @JsonProperty
    String postcode;

    @JsonProperty
    String citycode;

    @JsonProperty
    Double x;

    @JsonProperty
    Double y;

    @JsonProperty
    Integer population;

    @JsonProperty
    String city;

    @JsonProperty
    String context;

    @JsonProperty
    Double importance;

}
