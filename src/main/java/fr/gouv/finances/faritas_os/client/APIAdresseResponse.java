/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.client;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonIgnoreProperties({"type", "version", "attribution", "licence", "filters", "limit"})

/**
 * APIAdresseResponse.java - Response from BAN API
 */
@Data
public class APIAdresseResponse
{
    @JsonProperty
    private List<APIAdresseFeature> features;

    @JsonProperty
    private String query;
}
