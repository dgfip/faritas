/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonIgnoreProperties({"type", "geometry"})

/**
 * APIAdresseFeature.java - Object for a address
 */
@Data
public class APIAdresseFeature
{
    @JsonProperty
    private APIAdresseProperties properties;

    public Integer getPopulation()
    {
        return properties.population;
    }
}
