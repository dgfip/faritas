/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.client;

public class ApiAdresseException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public ApiAdresseException(String message)
    {
        super(message);
    }

    public ApiAdresseException(String message, Throwable e)
    {
        super(message, e);
    }
}
