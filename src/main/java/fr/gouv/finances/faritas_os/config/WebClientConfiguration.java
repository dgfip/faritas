/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.netty.transport.ProxyProvider;

@Configuration
public class WebClientConfiguration
{
    @Value("${proxy.host}")
    private String proxyHost;

    @Value("${proxy.port}")
    private String proxyPort;

    @Bean
    public WebClient webClient()
    {
        if (proxyHost != null && !"".equals(proxyHost))
        {
            // Proxy
            final reactor.netty.http.client.HttpClient httpClient = reactor.netty.http.client.HttpClient.create()
                .proxy(proxy -> proxy.type(ProxyProvider.Proxy.HTTP)
                    .host(proxyHost)
                    .port(Integer.parseInt(proxyPort)));
            final ReactorClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
            return WebClient.builder()
                .clientConnector(connector)
                .build();
        }
        // Pas de Proxy
        final reactor.netty.http.client.HttpClient httpClient = reactor.netty.http.client.HttpClient.create();
        final ReactorClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        return WebClient.builder()
            .clientConnector(connector)
            .build();
    }

}
