/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.config;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.context.annotation.Configuration;

import jakarta.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/faritas/*")
public class JerseyResourceConfig extends ResourceConfig
{
    public JerseyResourceConfig()
    {
        // API projet
        packages(true, "fr.gouv.finances.faritas_os.api");
        register(MultiPartFeature.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }
}
