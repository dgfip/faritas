/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import fr.gouv.finances.faritas_os.Constantes;
import lombok.Builder;
import lombok.Data;

/**
 * SejourAnalyse.java - Détail d'un séjour après analyse
 */
@Data
@Builder
public class SejourAnalyse
{
    @JsonFormat(pattern = Constantes.FRENCH_DATE_PATTERN, timezone = "Europe/Paris")
    private LocalDate dateDebutSejour;

    @JsonFormat(pattern = Constantes.FRENCH_DATE_PATTERN, timezone = "Europe/Paris")
    private LocalDate dateFinSejour;

    @JsonFormat(pattern = Constantes.FRENCH_DATE_PATTERN, timezone = "Europe/Paris")
    private LocalDate datePerception;

    private String adresse;

    private String codePostal;

    private String codeInsee;

    private String ville;

    private String sirenCommune;

    private String sirenCollectiviteDeliberante;

    private Integer natureLogement;

    private Integer categorieLogement;

    private Integer nombreNuits;

    private Integer nombreVoyageurs;

    private Integer nombreVoyageursExoneresMineurs;

    private Integer nombreVoyageursExoneresUrgence;

    private Integer nombreVoyageursExoneresSocial;

    private Integer nombreVoyageursExoneresSaisonniers;

    private Integer nombreVoyageursExoneresAutres;

    private Double prixParNuit;

    private Double montantTotalTaxeSejour;

    private Integer nombreNuitees;

    private Integer nombreNuiteesAssujetties;

    private Double prixParNuitee;

    private String collectiviteDeliberanteSiren;

    private String collectiviteDeliberanteAnnee;

    private String collectiviteDeliberanteNom;

    private String collectiviteDeliberanteCodePostal;

    private List<AnomalieDto> listDAnomalies;

    private String regime;

    private String sirenRattachement;

    private Double montantTaxeDeSejourCalculee;

    private Double tarifParNuiteeAttendu; // utilisé dans le calcul de la taxe de séjour
}
