/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PeriodeTarifaireDto
{
    private String siren;

    private LocalDate dateDebut;

    private LocalDate dateFin;

    private Double tarifPourcentageCategorie19;

    private List<Double> tarifsFixesCategories;

    private CollectiviteDeliberanteDto collectiviteDeliberante;
}
