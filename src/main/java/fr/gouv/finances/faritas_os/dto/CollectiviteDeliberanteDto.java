/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.util.Arrays;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CollectiviteDeliberanteDto
{
    private String annee;

    private String siren;

    private String nom;

    private String codePostal;

    private List<CollectiviteDto> listeDeCollectivites;

    private List<PeriodeTarifaireDto> listePeriodesTarifaires;

    private List<String> regimesTaxation;

    public void ajouterMultipleCollectivites(List<CollectiviteDto> listeNouvellesCollectivites)
    {
        listeDeCollectivites.addAll(listeNouvellesCollectivites);
    }

    public void ajouterMultipleCollectivites(CollectiviteDto... listeNouvellesCollectivites)
    {
        listeDeCollectivites.addAll(Arrays.asList(listeNouvellesCollectivites));
    }
}
