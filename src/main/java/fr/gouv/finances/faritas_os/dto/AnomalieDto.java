/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class AnomalieDto implements Serializable
{
    /**
     * serialVersionUID - long, DOCUMENTEZ_MOI
     */
    private static final long serialVersionUID = 6595107263722810666L;

    private final UUID id;

    private final AnomalieValideEnum message;

    public static AnomalieDto creerAnomalie(AnomalieValideEnum message)
    {
        return new AnomalieDto(UUID.randomUUID(), message);
    }

    public static AnomalieDto creerAnomalie(UUID id, AnomalieValideEnum message)
    {
        return new AnomalieDto(id, message);
    }
}
