/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

public enum AnomalieValideEnum
{
    // @formatter:off
    AUCUN_RESULTAT_POUR_CETTE_COMMUNE_ET_CE_CODE_POSTAL("AUCUN_RESULTAT_POUR_CETTE_COMMUNE_ET_CE_CODE_POSTAL"),
    DATE_DEBUT_SEJOUR_FORMAT_INVALIDE("DATE_DEBUT_SEJOUR_FORMAT_INVALIDE"),
    DATE_PERCEPTION_FORMAT_INVALIDE("DATE_PERCEPTION_FORMAT_INVALIDE"),
    ADRESSE_MANQUANTE("ADRESSE_MANQUANTE"),
    NOMBRE_DE_NUITS_INCOHERENT("NOMBRE_DE_NUITS_INCOHERENT"),
    TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE("TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE"),
    NOMBRE_DE_VOYAGEURS_MANQUANT("NOMBRE_DE_VOYAGEURS_MANQUANT"),
    CATEGORIE_DU_LOGEMENT_INVALIDE("CATEGORIE_DU_LOGEMENT_INVALIDE"),
    CATEGORIE_19_ET_PRIX_MANQUANT("CATEGORIE_19_ET_PRIX_MANQUANT"),
    REGIME_FORFAITAIRE_APPLICABLE("REGIME_FORFAITAIRE_APPLICABLE"),
    RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE("RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE");
    // @formatter:on

    private final String label;

    private AnomalieValideEnum(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }

    public static AnomalieValideEnum valueOfLabel(String label)
    {
        for (final AnomalieValideEnum anomalie : values())
        {
            if (anomalie.getLabel().equals(label))
            {
                return anomalie;
            }
        }
        return null;
    }
}
