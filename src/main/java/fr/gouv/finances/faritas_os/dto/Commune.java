/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.util.Locale;

import fr.gouv.finances.faritas_os.utility.FaritasUtils;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Commune
{
    private final String nom;

    private final String codePostal;

    public boolean estEquivalentA(String nom, String codePostal)
    {
        return this.codePostal.equals(codePostal) && areNomsEquivalents(nom);
    }

    private boolean areNomsEquivalents(String nom)
    {
        final String nomNettoye = FaritasUtils.nettoyer(this.nom).toLowerCase(Locale.FRANCE);
        return nomNettoye.equalsIgnoreCase(FaritasUtils.nettoyer(nom));
    }
}
