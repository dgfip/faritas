/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

public enum RegimeTaxation
{
    // @formatter:off
    REGIME_REEL("Réel"),
    REGIME_FORFAITAIRE("Forfaitaire"),
    REGIME_NON_ATTACHE("Non rattachée à un tarif");
    // @formatter:on

    private final String label;

    private RegimeTaxation(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }

}
