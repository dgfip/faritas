/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CollectiviteDto
{
    private String annee;

    private String siren;

    private String nom;

    private String codePostal;

    private Boolean taxeAdditionnelleDepartementale;

    private Boolean taxeAdditionnelleRegionale;

    private Boolean taxeAdditionnelleGrandeVitesse;

    private Boolean taxeAdditionnelleIdfm;

    private String codeInsee;

    private CollectiviteDeliberanteDto collectiviteDeliberante;

    public Boolean isTaxeDepartementaleApplicable()
    {
        return taxeAdditionnelleDepartementale;
    }

    public Boolean isTaxeRegionaleApplicable()
    {
        return taxeAdditionnelleRegionale;
    }

    public Boolean isTaxeGrandeVitesseApplicable()
    {
        return taxeAdditionnelleGrandeVitesse;
    }

    public Boolean isTaxeIdfmApplicable()
    {
        return taxeAdditionnelleIdfm;
    }
}
