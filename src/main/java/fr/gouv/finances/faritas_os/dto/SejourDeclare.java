/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import fr.gouv.finances.faritas_os.constraint.DateExtendedValidation;
import fr.gouv.finances.faritas_os.constraint.FrenchDateConstraint;
import fr.gouv.finances.faritas_os.constraint.FrenchZipCodeConstraint;
import fr.gouv.finances.faritas_os.utility.FaritasUtils;
import jakarta.validation.GroupSequence;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;
import lombok.Data;

/**
 * SejourDeclare.java - Détail d'un séjour
 */
@Data
@Builder
@GroupSequence({SejourDeclare.class, DateExtendedValidation.class})
public class SejourDeclare implements Serializable
{
    /**
     * serialVersionUID - long, DOCUMENTEZ_MOI
     */
    private static final long serialVersionUID = -1392241426561712667L;

    @NotEmpty(message = "La date de début de séjour est obligatoire")
    @FrenchDateConstraint(message = "La date de début de séjour est incorrecte")
    private String dateDebutSejour;

    @NotEmpty(message = "La date de fin de séjour est obligatoire")
    @FrenchDateConstraint(message = "La date de fin de séjour est incorrecte")
    private String dateFinSejour;

    @NotEmpty(message = "La date de perception est obligatoire")
    @FrenchDateConstraint(message = "La date de perception est incorrecte")
    private String datePerception;

    private String adresse;

    @NotEmpty(message = "Le code postal est obligatoire")
    @FrenchZipCodeConstraint(message = "Le code postal est incorrect")
    private String codePostal;

    @NotEmpty(message = "La ville est obligatoire")
    private String ville;

    @NotEmpty(message = "Le SIREN de la commune est obligatoire")
    @Pattern(regexp = "^\\d{9}$", message = "Le SIREN de la commune est incorrect")
    private String sirenCommune;

    @NotEmpty(message = "Le SIREN de la collectivité délibérante est obligatoire")
    @Pattern(regexp = "^\\d{9}$", message = "Le SIREN de la collectivité délibérante est incorrect")
    private String sirenCollectiviteDeliberante;

    @NotNull(message = "La nature du logement est obligatoire")
    @Min(value = 1, message = "La nature du logement est incorrecte")
    @Max(value = 11, message = "La nature du logement est incorrecte")
    private Integer natureLogement;

    @NotNull(message = "La catégorie du logement est obligatoire")
    @Min(value = 11, message = "La catégorie du logement est incorrecte")
    @Max(value = 19, message = "La catégorie du logement est incorrecte")
    private Integer categorieLogement;

    @NotNull(message = "Le nombre de nuits est obligatoire")
    @Min(value = 1, message = "Le nombre de nuits est incorrect")
    @Max(value = 366, message = "Le nombre de nuits est incorrect")
    private Integer nombreNuits;

    @NotNull(message = "Le nombre de voyageurs est obligatoire")
    @Min(value = 1, message = "Le nombre de voyageurs est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs est incorrect")
    private Integer nombreVoyageurs;

    @Min(value = 0, message = "Le nombre de voyageurs mineurs est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs mineurs est incorrect")
    private Integer nombreVoyageursExoneresMineurs;

    @Min(value = 0, message = "Le nombre de voyageurs pour urgence est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs pour urgence est incorrect")
    private Integer nombreVoyageursExoneresUrgence;

    @Min(value = 0, message = "Le nombre de voyageurs sociaux est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs sociaux est incorrect")
    private Integer nombreVoyageursExoneresSocial;

    @Min(value = 0, message = "Le nombre de voyageurs saisonniers est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs saisonniers est incorrect")
    private Integer nombreVoyageursExoneresSaisonniers;

    @Min(value = 0, message = "Le nombre de voyageurs exonérés autres est incorrect")
    @Max(value = 50, message = "Le nombre de voyageurs exonérés autres est incorrect")
    private Integer nombreVoyageursExoneresAutres;

    private Double prixParNuit;

    @NotNull(message = "Le montant total de la taxe de séjour est obligatoire")
    private Double montantTotalTaxeSejour;

    @JsonIgnore
    @AssertTrue(message = "La date de début de séjour est après la date de fin de séjour", groups = DateExtendedValidation.class)
    public boolean isDateFinSejourAfterDateDebutSejour()
    {
        final LocalDate startDate = LocalDate.parse(dateDebutSejour, FaritasUtils.FRENCH_DATE_FORMATTER);
        final LocalDate endDate = LocalDate.parse(dateFinSejour, FaritasUtils.FRENCH_DATE_FORMATTER);
        return !endDate.isBefore(startDate);
    }
}
