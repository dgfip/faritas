/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.api;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finances.faritas_os.component.ChargementOcsitan;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * OcsitanController.java - API pour le fichier des délibérations OCSITAN
 */
@Slf4j
@Path("/api")
@RestController
@RequiredArgsConstructor
public class OcsitanController
{
    private final ChargementOcsitan chargementOcsitan;

    @POST
    @Path("/chargement-ocsitan")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response remplirOcsitan(
        @NotEmpty(message = "L'année est obligatoire") @Pattern(regexp = "20[2-5]\\d", message = "L'année est incorrecte") @FormDataParam("annee") String anneeOcsitan,
        @FormDataParam("ocsitan") InputStream ocsitan,
        @NotNull(message = "Le fichier OCSITAN est obligatoire") @FormDataParam("ocsitan") FormDataContentDisposition ocsitanDetails)
        throws IOException
    {
        log.info("Nouveau fichier OCSITAN à charger.");

        final String output = String.format("Chargement du fichier OCSITAN pour l'année %s", anneeOcsitan);

        try
        {
            final java.nio.file.Path ocsitanPath = Files.createTempFile("ocsitan-", null);
            final long fileSize = Files.copy(ocsitan, ocsitanPath, StandardCopyOption.REPLACE_EXISTING);

            if (fileSize == 0)
            {
                log.info("Le fichier OCSITAN est vide.");
                return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Le fichier OCSITAN est vide")
                    .build();
            }

            chargementOcsitan.chargerDonneesOcsitan(anneeOcsitan, ocsitanPath);

            return Response
                .status(Response.Status.ACCEPTED)
                .entity(output)
                .build();
        }
        catch (final ReadOcsitanException e)
        {
            log.info("Erreur lors du chargement du fichier OCSITAN : {} ", e.getMessage());
            return Response
                .status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Erreur lors du chargement du fichier OCSITAN.")
                .build();
        }
    }
}
