/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finances.faritas_os.component.SegmenterUnSejour;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.dto.SejourDeclare;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * TaxeSejourController.java - API de calcul de la taxe de séjour
 */
@Path("/api")
@RestController
@Slf4j
@RequiredArgsConstructor
public class SejourController
{
    private final SegmenterUnSejour segmenterUnSejour;

    /**
     * Soumission d'un séjour pour contrôle
     *
     * @param sejour Séjour à controler
     * @return Séjour analysé
     */
    @POST
    @Path("/controle-sejour")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response controleSejour(@Valid final SejourDeclare sejour)
    {
        log.info("Traitement d'un sejour");

        final SejourAnalyse sejourAnalyse = segmenterUnSejour.traitementSejourDeclare(sejour);

        return Response
            .status(Response.Status.OK)
            .entity(sejourAnalyse)
            .build();
    }

    /**
     * Soumission d'une liste de séjours pour contrôle
     *
     * @param sejours Liste de séjours à controler
     * @return Liste des séjours analysés
     */
    @POST
    @Path("/controle-liste-sejours")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response controleSejours(
        @RequestBody @Size(min = 1, max = 100, message = "Le nombre de séjours doit être entre {min} et {max}") final List<@Valid SejourDeclare> sejours)
    {
        log.info("Traitement d'une liste sejour");

        final List<SejourAnalyse> resultats = new ArrayList<>();

        for (final SejourDeclare sejour : sejours)
        {
            resultats.add(segmenterUnSejour.traitementSejourDeclare(sejour));
        }

        return Response
            .status(Response.Status.OK)
            .entity(resultats)
            .build();
    }
}
