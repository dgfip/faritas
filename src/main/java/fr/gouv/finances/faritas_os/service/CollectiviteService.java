/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import java.util.List;

import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.exception.CollectiviteNonTrouveeException;

/**
 * CollectiviteService.java - Traitement de collectivité
 */
public interface CollectiviteService
{
    /**
     * Recherche des collectivités pour une année et un code postal
     *
     * @param annee Année du séjour
     * @param codePostal Code postal du séjour
     * @return Liste des collectivités trouvées
     */
    List<CollectiviteDto> listerParAnneeEtCodePostal(String annee, String codePostal);

    /**
     * Recherche une collectivité pour une année et un code INSEE
     *
     * @param annee Année du séjour
     * @param codeInsee code INSEE de la collectivité
     * @return Collectivité trouvée
     * @throws CollectiviteNonTrouveeException Pas de collectivité trouvée
     */
    CollectiviteDto trouverParAnneeEtCodeInsee(String annee, String codeInsee) throws CollectiviteNonTrouveeException;

    /**
     * Recherche du SIREN de la collectivité délibérante
     *
     * @param siren SIREN de la collectivité
     * @param annee Année du séjour
     * @return SIREN de la collectivité délibérante
     */
    String getDeliberanteFromSirenCommuneAndAnnee(String siren, Integer annee);

    /**
     * Recherche de la collectivité
     *
     * @param siren SIREN de la collectivité
     * @param annee Année du séjour
     * @return Collectivité
     */
    CollectiviteDto trouverCollectiviteParSirenEtAnnee(String siren, Integer annee);
}
