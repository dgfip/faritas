/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service.impl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.entity.Collectivite;
import fr.gouv.finances.faritas_os.entity.CollectiviteDeliberante;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;
import fr.gouv.finances.faritas_os.exception.CollectiviteNonTrouveeException;
import fr.gouv.finances.faritas_os.repository.CollectiviteRepository;
import fr.gouv.finances.faritas_os.service.CollectiviteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * CollectiviteServiceImpl.java - Accès à une collectivité
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CollectiviteServiceImpl implements CollectiviteService
{
    private final CollectiviteRepository collectiviteRepository;

    @Override
    public List<CollectiviteDto> listerParAnneeEtCodePostal(String annee, String codePostal)
    {
        final List<Collectivite> listeDeCollectivite =
            collectiviteRepository.findAllByAnneeAndCodePostalOrderByNom(annee, codePostal);

        return convertirListeEntityToDto(listeDeCollectivite);
    }

    @Override
    public CollectiviteDto trouverParAnneeEtCodeInsee(String annee, String codeInsee) throws CollectiviteNonTrouveeException
    {// filtrer par année aussi
        final Optional<Collectivite> optionalCollectivite = collectiviteRepository.findByAnneeAndCodeInsee(annee, codeInsee);

        if (!optionalCollectivite.isPresent())
        {
            throw new CollectiviteNonTrouveeException();
        }

        final Collectivite collectivite = optionalCollectivite.get();
        return convertToDto(collectivite);
    }

    private List<CollectiviteDto> convertirListeEntityToDto(List<Collectivite> listeDeCollectivite)
    {
        return listeDeCollectivite.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    private CollectiviteDto convertToDto(Collectivite entity)
    {
        return CollectiviteDto.builder()
            .siren(entity.getSiren())
            .annee(entity.getAnnee())
            .nom(entity.getNom())
            .codePostal(entity.getCodePostal())
            .codeInsee(entity.getCodeInsee())
            .taxeAdditionnelleDepartementale(entity.getTaxeDepartementaleApplicable())
            .taxeAdditionnelleRegionale(entity.getTaxeRegionaleApplicable())
            .taxeAdditionnelleGrandeVitesse(entity.getTaxeGrandeVitesseApplicable())
            .taxeAdditionnelleIdfm(entity.getTaxeIdfmApplicable())
            .collectiviteDeliberante(CollectiviteDeliberante.convertToDto(entity.getCollectiviteDeliberante()))
            .build();
    }

    @Override
    public String getDeliberanteFromSirenCommuneAndAnnee(String siren, Integer annee)
    {
        final LocalDateTime debutTraitement = LocalDateTime.now();
        log.info("Récupération siren deliberante pour la collectivite {} et l'année {}.", siren, annee);
        final CompositePrimaryKey pk = new CompositePrimaryKey(siren, String.valueOf(annee));
        final Optional<Collectivite> optionalCollectivite = collectiviteRepository.findById(pk);

        if (!optionalCollectivite.isPresent())
        {
            return null;
        }
        log.info("Récupération siren deliberante pour la collectivite {}. Durée du traitement : {}.", siren,
            Duration.between(debutTraitement, LocalDateTime.now()));
        return optionalCollectivite.get().getCollectiviteDeliberante().getSiren();
    }

    @Override
    public CollectiviteDto trouverCollectiviteParSirenEtAnnee(String siren, Integer annee)
    {

        final Optional<Collectivite> collectiviteDeLaPeriode =
            collectiviteRepository.findBySirenAndAnnee(siren, Integer.toString(annee));
        if (!collectiviteDeLaPeriode.isPresent())
        {
            log.info("la collectivité avec le siren {} et année {} est introuvable !", siren, annee);
            return null;
        }

        return convertToDto(collectiviteDeLaPeriode.get());
    }

}
