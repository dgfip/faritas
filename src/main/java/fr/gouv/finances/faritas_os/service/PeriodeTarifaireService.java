/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import java.time.LocalDate;
import java.util.List;

import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;

/**
 * PeriodeTarifaireServiceImpl - Gestion des périodes tarifaires.
 */
public interface PeriodeTarifaireService
{
    /**
     * Recherche des périodes tarifaires s'appliquant au séjour
     *
     * @param siren SIREN de la collectivité délibérante
     * @param dateDebutSejour Date de début de séjour
     * @param dateFinSejour Date de fin de séjour
     * @return Liste des périodes tarifaires pour le séjour
     */
    List<PeriodeTarifaireDto> getPeriodeTarifairePourDeliberanteEtDate(String siren, LocalDate dateDebutSejour,
        LocalDate dateFinSejour);

}
