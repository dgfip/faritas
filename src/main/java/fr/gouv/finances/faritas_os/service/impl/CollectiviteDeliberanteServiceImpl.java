/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.entity.Collectivite;
import fr.gouv.finances.faritas_os.entity.CollectiviteDeliberante;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.repository.CollectiviteDeliberanteRepository;
import fr.gouv.finances.faritas_os.service.CollectiviteDeliberanteService;
import lombok.RequiredArgsConstructor;

/**
 * CollectiviteDeliberanteServiceImpl.java - Traitement d'une collectivité délibérante
 */
@Service
@RequiredArgsConstructor
public class CollectiviteDeliberanteServiceImpl implements CollectiviteDeliberanteService
{
    private final Logger log = LoggerFactory.getLogger(CollectiviteDeliberanteServiceImpl.class);

    private final CollectiviteDeliberanteRepository collectiviteDeliberanteRepository;

    @Override
    @Modifying
    @Transactional
    public void enregistrer(List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes)
    {
        log.debug("Ajout de {} nouvelles collectivités délibérantes", listeDeCollectivitesDeliberantes.size());
        final List<CollectiviteDeliberante> listeCollectiviteDeliberanteASauvegarder = listeDeCollectivitesDeliberantes
            .stream().map(this::convertToEntity).collect(Collectors.toList());
        collectiviteDeliberanteRepository.saveAll(listeCollectiviteDeliberanteASauvegarder);
        log.debug("Ajout effectué");
    }

    @Override
    public void enregistrer(CollectiviteDeliberanteDto collectiviteDeliberanteDto)
    {
        log.debug("Enregistrement de la collectivité délibérante {}-{}", collectiviteDeliberanteDto.getAnnee(),
            collectiviteDeliberanteDto.getSiren());
        final CollectiviteDeliberante collectiviteDeliberante = convertToEntity(collectiviteDeliberanteDto);
        collectiviteDeliberanteRepository.save(collectiviteDeliberante);
        log.debug("Ajout effectué");
    }

    private CollectiviteDeliberante convertToEntity(CollectiviteDeliberanteDto dto)
    {
        final CollectiviteDeliberante entity = CollectiviteDeliberante.builder()
            .siren(dto.getSiren())
            .annee(dto.getAnnee())
            .nom(dto.getNom())
            .codePostal(dto.getCodePostal())
            .regimesTaxationParNature(String.join(";", dto.getRegimesTaxation()))
            .build();

        final List<PeriodeTarifaire> listePeriodeTarifaire = dto.getListePeriodesTarifaires().stream()
            .map(periode -> PeriodeTarifaire.builder()
                .siren(periode.getSiren())
                .dateDebut(periode.getDateDebut())
                .dateFin(periode.getDateFin())
                .tarifPourcentageCategorie19(periode.getTarifPourcentageCategorie19())
                .tarifsFixesCategories(String.join(Constantes.TARIF_SEPARATOR,
                    periode.getTarifsFixesCategories().stream().map(Object::toString).collect(Collectors.toList())))
                .collectiviteDeliberante(entity)
                .build())
            .collect(Collectors.toList());

        final List<Collectivite> listeCollectivite = dto.getListeDeCollectivites().stream()
            .map(collectivite -> Collectivite.builder()
                .siren(collectivite.getSiren())
                .annee(collectivite.getAnnee())
                .nom(collectivite.getNom())
                .codePostal(collectivite.getCodePostal())
                .codeInsee(collectivite.getCodeInsee())
                .taxeDepartementaleApplicable(collectivite.isTaxeDepartementaleApplicable())
                .taxeRegionaleApplicable(collectivite.isTaxeRegionaleApplicable())
                .taxeGrandeVitesseApplicable(collectivite.isTaxeGrandeVitesseApplicable())
                .collectiviteDeliberante(entity)
                .build())
            .collect(Collectors.toList());
        entity.setListeCollectivite(listeCollectivite);
        entity.setListePeriodesTarifaires(listePeriodeTarifaire);
        return entity;
    }

    @Override
    public void supprimerTout()
    {
        log.debug("Suppression de toutes les collectivités délibérantes");
        collectiviteDeliberanteRepository.deleteAll();
        log.debug("Suppression effectuée");
    }

    @Override
    public CollectiviteDeliberanteDto getDeliberanteFromSirenAndAnnee(String sirenDeliberante, Integer yearDebutSejour)
    {
        final Optional<CollectiviteDeliberante> entity =
            collectiviteDeliberanteRepository.findById(new CompositePrimaryKey(sirenDeliberante, String.valueOf(yearDebutSejour)));
        if (entity.isPresent())
        {
            return CollectiviteDeliberante.convertToDto(entity.get());
        }
        return null;
    }

    @Override
    public Boolean isDeliberanteFound(String sirenCollectiviteDeliberante)
    {
        final List<CollectiviteDeliberante> listeDeCollectiviteDeliberante =
            collectiviteDeliberanteRepository.findBySiren(sirenCollectiviteDeliberante);
        return !listeDeCollectiviteDeliberante.isEmpty();
    }
}
