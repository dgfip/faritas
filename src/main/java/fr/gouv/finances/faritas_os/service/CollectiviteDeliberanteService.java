/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import java.util.List;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;

/**
 * CollectiviteDeliberanteService.java - Traitement de collectivité délibérante
 */
public interface CollectiviteDeliberanteService
{
    /**
     * Enregistement d'une liste de collectivités délibérantes
     *
     * @param listeDeCollectivitesDeliberantes Liste de collectivités délibérantes
     */
    void enregistrer(List<CollectiviteDeliberanteDto> listeDeCollectivitesDeliberantes);

    /**
     * Enregistrement d'une collectivité délibérante
     *
     * @param collectiviteDeliberanteDto collectivité délibérante
     */
    void enregistrer(CollectiviteDeliberanteDto collectiviteDeliberanteDto);

    /**
     * Suppression des collectivités délibérantes
     */
    void supprimerTout();

    /**
     * Lecture d'une collectivité délibérante par siren et annee
     *
     * @param sirenDeliberante SIREN
     * @param yearDebutSejour Année
     * @return
     */
    CollectiviteDeliberanteDto getDeliberanteFromSirenAndAnnee(String sirenDeliberante, Integer yearDebutSejour);

    /**
     * Vérification de la présence d'une collectivité délibérante
     *
     * @param sirenCollectiviteDeliberante SIREN
     * @return True si la collectivité existe
     */
    Boolean isDeliberanteFound(String sirenCollectiviteDeliberante);
}
