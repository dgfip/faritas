/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import java.io.InputStream;
import java.util.List;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.entity.ocsitan.Tarif;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;

/**
 * OcsitanReaderService.java - Traitement Fichier OCSITAN
 */
public interface OcsitanReaderService
{
    /**
     * Chargement d'un fichier Ocsitan en mémoire.
     *
     * @param anneeOcsitan Année du fichier OCSITAN
     * @param fichier fichier OCSITAN
     * @return Liste des collectivités délibérantes à charger.
     * @throws ReadOcsitanException Erreur lors du traitement de l'OCSITAN
     */
    List<CollectiviteDeliberanteDto> recupererCollectivitesDeliberantes(String anneeOcsitan, InputStream fichier)
        throws ReadOcsitanException;

    /**
     * Conversion des catégories de tarif des millesimes 2018 vers celles des millesimes 2019
     *
     * @param tarifsDeliberation Tarifs des catégories du millésime 2018
     * @return Tarifs avec les catégories du millésime 2019
     */
    List<Tarif> convertTarifsFrom2018(List<Tarif> tarifsDeliberation);
}
