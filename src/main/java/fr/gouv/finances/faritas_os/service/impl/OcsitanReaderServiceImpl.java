/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.entity.ocsitan.CollectiviteDeliberanteOcsitan;
import fr.gouv.finances.faritas_os.entity.ocsitan.DeliberationOcsitan;
import fr.gouv.finances.faritas_os.entity.ocsitan.OcsitanObject;
import fr.gouv.finances.faritas_os.entity.ocsitan.Periode;
import fr.gouv.finances.faritas_os.entity.ocsitan.Tarif;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import fr.gouv.finances.faritas_os.parser.OcsitanXmlParser;
import fr.gouv.finances.faritas_os.service.OcsitanReaderService;
import fr.gouv.finances.faritas_os.utility.ParserUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * OcsitanReaderServiceImpl - Lecture d'un fichier de délibérations de taxe de séjours OCSITAN
 */
@Service
@Slf4j
public class OcsitanReaderServiceImpl implements OcsitanReaderService
{
    private final OcsitanXmlParser parser = new OcsitanXmlParser();

    @Override
    public List<CollectiviteDeliberanteDto> recupererCollectivitesDeliberantes(String anneeOcsitan, InputStream fichier)
        throws ReadOcsitanException
    {
        log.debug("Chargement du fichier OCSITAN pour l'année {}", anneeOcsitan);

        try (fichier)
        {
            final List<CollectiviteDeliberanteDto> collectiviteDeliberanteList = new ArrayList<>();
            final OcsitanObject ocsitan = parser.parseXml(fichier);

            ocsitan.getDeliberations().forEach(deliberation -> {

                int posDeliberante = -1;
                for (int i = 0; i < collectiviteDeliberanteList.size(); i++)
                {
                    if (collectiviteDeliberanteList.get(i).getSiren()
                        .equals(deliberation.getSaisie().getCollectiviteDeliberante().getSiren()))
                    {
                        posDeliberante = i;
                        break;
                    }
                }

                final CollectiviteDeliberanteDto deliberante = mapXmlDelibrationToCollectiviteDeliberante(deliberation, anneeOcsitan);
                if (posDeliberante < 0)
                {
                    // Ajout deliberante
                    collectiviteDeliberanteList.add(deliberante);
                }
                else
                {
                    log.info("Remplacement délibération en double pour {} - SIREN {} - millésime {}",
                        deliberation.getSaisie().getCollectiviteDeliberante().getNom(),
                        deliberation.getSaisie().getCollectiviteDeliberante().getSiren(), deliberation.getMillesime());
                    collectiviteDeliberanteList.set(posDeliberante, deliberante);
                }
            });

            log.debug("end mapping deliberation to collectivite deliberante ...");
            return collectiviteDeliberanteList;
        }
        catch (SAXException | IOException | ParserConfigurationException e)
        {
            log.error("Erreur lors de l'analyse du fichier OCSITAN pourl'année {}", anneeOcsitan, e);
            throw new ReadOcsitanException("Erreur lors du traitement du fichier OCSITAN de l'année " + anneeOcsitan);
        }
    }

    private CollectiviteDeliberanteDto mapXmlDelibrationToCollectiviteDeliberante(DeliberationOcsitan deliberation, String anneeOcsitan)
    {
        final CollectiviteDeliberanteOcsitan deliberanteOcsitan = deliberation.getSaisie().getCollectiviteDeliberante();

        final CollectiviteDeliberanteDto deliberante = CollectiviteDeliberanteDto.builder()
            .siren(deliberanteOcsitan.getSiren())
            .annee(anneeOcsitan)
            .nom(deliberanteOcsitan.getNom())
            .codePostal(deliberanteOcsitan.getCodePostal())
            .regimesTaxation(deliberation.getRegimes().stream().map(this::verifierSiNonRattacheAuTarif).toList())
            .listePeriodesTarifaires(
                recuperationPeriodesTarifaire(deliberation.getPeriodes(), deliberanteOcsitan.getSiren(), deliberation.getMillesime(),
                    anneeOcsitan))
            .build();

        final List<CollectiviteDto> collectivites = new ArrayList<>();

        // Pour chaque collectivité, on vérifie qu'elle n'existe pas déjà dans la liste
        deliberation.getCollectivites().forEach(
            collectiviteXml -> {
                // Est-ce que la collectivité a déjà été enregistrée
                final boolean collectiviteExiste =
                    collectivites.stream().map(CollectiviteDto::getSiren).anyMatch(collectiviteXml.getSiren()::equals);

                // Elle n'existe pas, on l'ajoute à la liste
                if (!collectiviteExiste)
                {
                    String cpColl = null;
                    if (!collectiviteXml.getCodePostal().isEmpty())
                    {
                        cpColl = collectiviteXml.getCodePostal();
                    }

                    final CollectiviteDto collectivite = CollectiviteDto.builder()
                        .siren(collectiviteXml.getSiren())
                        .annee(anneeOcsitan)
                        .nom(collectiviteXml.getNom())
                        .codePostal(cpColl)
                        .taxeAdditionnelleDepartementale(deliberation.isTaxeAdditionnelleDepartementale())
                        .taxeAdditionnelleRegionale(deliberation.isTaxeAdditionnelleRegionale())
                        .taxeAdditionnelleGrandeVitesse(deliberation.isTaxeAdditionnelleLGV())
                        .build();
                    collectivites.add(collectivite);
                }
                else
                {
                    log.warn("La collectivité {} est en double. SIREN={} - Code Postal={}", collectiviteXml.getNom(),
                        collectiviteXml.getSiren(), collectiviteXml.getCodePostal());
                }
            });

        // Si il n'y a pas de collectivité dans la délibérante, on met la délibérante comme collectivité
        if (CollectionUtils.isEmpty(collectivites))
        {
            final CollectiviteDto collectivite = CollectiviteDto.builder()
                .siren(deliberation.getSaisie().getCollectiviteDeliberante().getSiren())
                .annee(anneeOcsitan)
                .nom(deliberation.getSaisie().getCollectiviteDeliberante().getNom())
                .codePostal(deliberation.getSaisie().getCollectiviteDeliberante().getCodePostal().isEmpty() ? null
                    : deliberation.getSaisie().getCollectiviteDeliberante().getCodePostal())
                .taxeAdditionnelleDepartementale(deliberation.isTaxeAdditionnelleDepartementale())
                .taxeAdditionnelleRegionale(deliberation.isTaxeAdditionnelleRegionale())
                .taxeAdditionnelleGrandeVitesse(deliberation.isTaxeAdditionnelleLGV())
                .build();
            collectivites.add(collectivite);
        }

        // On ajoute la liste des collectivités pour la delibérante
        deliberante.setListeDeCollectivites(collectivites);
        return deliberante;
    }

    /**
     * Traitement des périodes tarifaires
     *
     * @param periodes Periodes
     * @param sirenDeliberante SIREN de la collectivité délibérante
     * @param millesime Millesime des catégories 2018 et 2019.
     * @return Liste des périodes tarifaires.
     */
    private List<PeriodeTarifaireDto> recuperationPeriodesTarifaire(List<Periode> periodes, String sirenDeliberante, String millesime,
        String anneeOcsitan)
    {

        final List<PeriodeTarifaireDto> tarifsDeliberation = new ArrayList<>();
        periodes.forEach(periode -> {

            final LocalDate dtDebut = getDate(sirenDeliberante, anneeOcsitan, periode.getDateDebut());
            final LocalDate dtFin = getDate(sirenDeliberante, anneeOcsitan, periode.getDateFin());
            log.debug("dtDebut : " + dtDebut + " dtFin : " + dtFin + " sirenDeliberante : " + sirenDeliberante + " anneeOcsitan : "
                + anneeOcsitan);
            // si date de debut est superieur a date de fin, l'usage du millisime pour définir une periode la rends
            // incohérente
            // nous splittons donc cette periode pour la rendre plus cohérente.
            if (dtDebut.isAfter(dtFin))
            {
                log.debug("periode à splitter");
                tarifsDeliberation.addAll(splitPeriodeTarifaire(periode, anneeOcsitan, sirenDeliberante, millesime));
            }
            else
            {
                log.debug("periode normale");
                final PeriodeTarifaireDto tarif = buildPeriodTarifaire(sirenDeliberante, dtDebut, dtFin, periode, millesime);

                // Est-ce que la periodeTarifaire a déjà été enregistrée ?
                int posTarif = -1;
                for (int i = 0; i < tarifsDeliberation.size(); i++)
                {
                    if (tarifsDeliberation.get(i).getDateDebut().equals(tarif.getDateDebut()))
                    {
                        posTarif = i;
                        break;
                    }
                }

                if (posTarif < 0)
                {
                    tarifsDeliberation.add(tarif);
                }
                else
                {
                    // On remplace la periode
                    log.warn("La periode tarifaire pour {} du {} au {} existe déjà, on la remplace.", sirenDeliberante, dtDebut, dtFin);
                    tarifsDeliberation.set(posTarif, tarif);
                }
            }
        });

        return tarifsDeliberation;
    }

    /**
     * Découper une période tarifaire dont les dates créent une incohérence en base si une date de début vient après une
     * date de fin. Exemple : la période du 01/11 au 30/04 donne 2 périodes 01/11 - 31/12 et 01/01 - 30/04.
     *
     * @param periode Periode concerné
     * @param anneeOcsitan année du fichier ocsitan
     * @param millesime
     * @return #{PeriodeTarifaire}
     */
    private List<PeriodeTarifaireDto> splitPeriodeTarifaire(Periode periode, String anneeOcsitan, String sirenDeliberante, String millesime)
    {
        // La première période correspond à la date de début jusqu'à la fin de l'année
        final LocalDate dateDebutPremierePeriode = getDate(sirenDeliberante, anneeOcsitan, periode.getDateDebut());
        final LocalDate dateFinPremierePeriode = LocalDate.of(dateDebutPremierePeriode.getYear(), 12, 31);

        // La seconde période correspond au début de l'année jusqu'à la date de fin
        final LocalDate dateFinDeuxiemePeriode = getDate(sirenDeliberante, anneeOcsitan, periode.getDateFin());
        final LocalDate dateDebutDeuxiemePeriode = LocalDate.of(dateFinDeuxiemePeriode.getYear(), 1, 1);

        return List.of(
            buildPeriodTarifaire(sirenDeliberante, dateDebutPremierePeriode, dateFinPremierePeriode, periode, millesime),
            buildPeriodTarifaire(sirenDeliberante, dateDebutDeuxiemePeriode, dateFinDeuxiemePeriode, periode, millesime));
    }

    /**
     * Récuperer une liste de tarif au format Double pour la base de donnée
     *
     * @param tarifs liste des tarifs à transformer
     */
    private List<Double> getListTarifDouble(List<Tarif> tarifs)
    {
        return tarifs
            .stream()
            .filter(tarif -> tarif.getCategorieId() != Constantes.MILLESIME_2019_CATEGORIE_19)
            .map(Tarif::getValue)
            .toList();
    }

    /**
     * récuperer la liste des tarif
     *
     * @param periode la periode concerné.
     * @param milliseme milliseme
     */
    private List<Tarif> getTarifOcsitan(Periode periode, String milliseme)
    {
        List<Tarif> tarifsOcsitan = periode.getTarifs();

        if (milliseme.equals(Constantes.ANNEE_2018))
        {
            tarifsOcsitan = convertTarifsFrom2018(periode.getTarifs());
        }
        return tarifsOcsitan;
    }

    /**
     * construire un objet de type PeriodeTarifaire
     *
     * @param sirenDeliberante siren de la collectivité déliberante
     * @param dtDebut date de debut de la période tarifaire
     * @param dtFin date de fin de la période tarifaire
     * @param periode Periode concerné
     * @param millesime
     * @return #{PeriodeTarifaire}
     */
    private PeriodeTarifaireDto buildPeriodTarifaire(String sirenDeliberante, LocalDate dtDebut, LocalDate dtFin, Periode periode,
        String millesime)
    {
        final List<Tarif> tarifsOcsitan = getTarifOcsitan(periode, millesime);

        double valeurTarifCategorie19 = 0.0;

        final Optional<Tarif> tarifCategorie19 = tarifsOcsitan
            .stream()
            .filter(t -> t.getCategorieId() == Constantes.CATEGORIE_19)
            .findFirst();

        if (tarifCategorie19.isPresent())
        {
            valeurTarifCategorie19 = tarifCategorie19.get().getValue();
        }

        final List<Double> tarifsFixesCategories = getListTarifDouble(tarifsOcsitan);
        return PeriodeTarifaireDto.builder()
            .siren(sirenDeliberante)
            .dateDebut(dtDebut)
            .dateFin(dtFin)
            .tarifPourcentageCategorie19(valeurTarifCategorie19)
            .tarifsFixesCategories(tarifsFixesCategories)
            .build();
    }

    /**
     * recuperer une date a partir d'un string et gestion des date avec ou sans millésime
     *
     * @param sirenDeliberante siren de la collectivité déliberante
     * @param anneeOcsitan l'année sur le fichier occitan ( anné de l'application des délibération )
     * @param periode la date en string recupéré sur le tag <periode></periode>, dateDebut/dateFin
     * @return Tarifs avec les catégories du millésime 2019
     */
    private LocalDate getDate(String sirenDeliberante, String anneeOcsitan, String periode)
    {
        LocalDate dtDebut;
        if (periode.matches("^\\d{1,2}\\s\\S+\\s\\d{4}$"))
        {
            log.warn("La date de début {} de la periode tarifaire pour {} contient l'année.", periode, sirenDeliberante);
            final LocalDate dtParsed = ParserUtils.parseFrenchLabelDate(periode);
            dtDebut = LocalDate.of(Integer.parseInt(anneeOcsitan), dtParsed.getMonth(), dtParsed.getDayOfMonth());
        }
        else
        {
            dtDebut = ParserUtils.parseFrenchLabelDate(periode + " " + anneeOcsitan);
        }
        return dtDebut;
    }

    /**
     * Conversion des tarifs des millésimes 2018 vers ceux des millésimes 2019
     *
     * @param tarifsDeliberation Tarifs du millésime 2018
     * @return Tarifs du millésime 2019
     */
    @Override
    public List<Tarif> convertTarifsFrom2018(List<Tarif> tarifsDeliberation)
    {
        final List<Tarif> tarifsDeliberation2019 = new ArrayList<>();

        for (final Tarif element : tarifsDeliberation)
        {
            final Tarif tarif = convertirTarif2018To2019(element);
            if (tarif != null)
            {
                tarifsDeliberation2019.add(tarif);
            }
        }
        return tarifsDeliberation2019;
    }

    private String verifierSiNonRattacheAuTarif(String regime)
    {
        if ("Nature d'hébergement non attachée à une catégorie tarifaire au titre de ce millésime".equalsIgnoreCase(regime))
        {
            regime = "Non rattachée à un tarif";
        }
        return regime;
    }

    private Tarif convertirTarif2018To2019(Tarif tarif2018)
    {

        if (tarif2018 != null)
        {
            final Tarif tarif2019 = new Tarif();
            tarif2019.setValue(tarif2018.getValue());

            switch (tarif2018.getCategorieId())
            {
                case Constantes.MILLESIME_2018_CATEGORIE_1:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_11);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_2:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_12);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_3:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_13);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_4:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_14);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_5:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_15);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_6:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_16);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_9:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_17);
                    break;
                case Constantes.MILLESIME_2018_CATEGORIE_10:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_18);
                    break;
                default:
                    tarif2019.setCategorieId(Constantes.MILLESIME_2019_CATEGORIE_19);
                    tarif2019.setValue(0.0);
                    break;
            }
            return tarif2019;
        }
        return null;
    }
}
