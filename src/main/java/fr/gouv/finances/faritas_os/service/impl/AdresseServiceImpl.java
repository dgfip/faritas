/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service.impl;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.springframework.stereotype.Service;

import fr.gouv.finances.faritas_os.client.APIAdresseFeature;
import fr.gouv.finances.faritas_os.client.APIAdresseProperties;
import fr.gouv.finances.faritas_os.client.APIAdresseResponse;
import fr.gouv.finances.faritas_os.client.ApiAdresseHttpClient;
import fr.gouv.finances.faritas_os.service.AdresseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * AdresseServiceImpl.java - Recherche des adresses par appel à l'API BAN
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AdresseServiceImpl implements AdresseService
{
    private static final Double SCORE_MINIMUM_ACCEPTABLE = 0.45;

    private static final Double SCORE_MINIMUM_POPULATION = 0.2;

    private final ApiAdresseHttpClient apiAdresseHttpClient;

    @Override
    public Mono<String> recupererCodeInsee(String ville, String codePostal)
    {
        log.debug("Récupérer information d'une ville par ville [{}] et code postal [{}].", ville, codePostal);
        return apiAdresseHttpClient.searchByCityAndPostCode(ville, codePostal)
            .map(this::constructVilleInformationFromJsonMapAPIAdresseEnFiltrantParPopulation)
            .onErrorResume(e -> apiAdresseHttpClient.searchByQuery(codePostal + " " + ville)
                .map(constructVilleInformationFromJsonMapApiAdresseEnFiltrantParScore(codePostal)))
            .onErrorReturn("");
    }

    @Override
    public Mono<String> recupererCodeInsee(String ville, String codePostal, String adresse)
    {
        log.debug("Récupérer information d'une ville par adresse [{}], ville [{}] et code postal [{}].", adresse, ville, codePostal);
        return apiAdresseHttpClient.searchByCityAndPostCode(ville, codePostal)
            .map(this::constructVilleInformationFromJsonMapAPIAdresseEnFiltrantParPopulation)
            .onErrorResume(e -> apiAdresseHttpClient.searchByQueryFull(adresse + " " + codePostal + " " + ville)
                .map(constructVilleInformationFromJsonMapApiAdresseEnFiltrantParScore(codePostal)))
            .onErrorReturn("");
    }

    private String constructVilleInformationFromJsonMapAPIAdresseEnFiltrantParPopulation(APIAdresseResponse apiAdresseResponse)
    {
        final APIAdresseProperties properties = apiAdresseResponse.getFeatures()
            .stream()
            .max(Comparator.comparing(APIAdresseFeature::getPopulation))
            .orElseThrow(NoSuchElementException::new).getProperties();

        if (properties.getScore() < SCORE_MINIMUM_POPULATION)
        {
            log.info("Récupérer information pour {} - {}. Score < {}.", properties.getPostcode(), properties.getCity(),
                SCORE_MINIMUM_POPULATION);
            throw new NoSuchElementException();
        }

        return properties.getCitycode();
    }

    private Function<APIAdresseResponse, String> constructVilleInformationFromJsonMapApiAdresseEnFiltrantParScore(
        String codePostal)
    {
        return jsonMapAPIAdresse -> {
            final APIAdresseProperties properties = extractProperties(jsonMapAPIAdresse);

            if (!isResultatAssezJuste(codePostal, properties))
            {
                log.info("Récupérer information pour {} - {}. Score > {}.", properties.getPostcode(), properties.getCity(),
                    SCORE_MINIMUM_ACCEPTABLE);
                throw new NoSuchElementException();
            }

            return properties.getCitycode();
        };
    }

    private APIAdresseProperties extractProperties(APIAdresseResponse jsonMapAPIAdresse)
    {
        if (jsonMapAPIAdresse.getFeatures().isEmpty())
        {
            log.info("Absence de réponse BAN pour la query {}.", jsonMapAPIAdresse.getQuery());
            throw new NoSuchElementException();
        }

        return jsonMapAPIAdresse.getFeatures().get(0).getProperties();
    }

    private boolean isResultatAssezJuste(String codePostal, APIAdresseProperties properties)
    {
        return codePostal.equals(properties.getCitycode()) ||
            (isScoreSuffisant(properties.getScore()) && sontDansLeMemeDepartement(codePostal, properties));
    }

    private boolean isScoreSuffisant(Double score)
    {
        return score > SCORE_MINIMUM_ACCEPTABLE;
    }

    private boolean sontDansLeMemeDepartement(String codePostal, APIAdresseProperties properties)
    {
        return extractDepartement(properties.getPostcode()).equals(extractDepartement(codePostal));
    }

    private String extractDepartement(String codePostal)
    {
        return codePostal.substring(0, 2);
    }
}
