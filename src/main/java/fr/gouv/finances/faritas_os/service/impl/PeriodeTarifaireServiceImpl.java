/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service.impl;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.repository.PeriodeTarifaireRepository;
import fr.gouv.finances.faritas_os.service.PeriodeTarifaireService;
import fr.gouv.finances.faritas_os.utility.ParserUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * PeriodeTarifaireServiceImpl - Gestion des périodes tarifaires.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PeriodeTarifaireServiceImpl implements PeriodeTarifaireService
{

    private final PeriodeTarifaireRepository periodeTarifaireRepository;

    @Override
    public List<PeriodeTarifaireDto> getPeriodeTarifairePourDeliberanteEtDate(String sirenDeliberante, LocalDate dateDebutSejour,
        LocalDate dateFinSejour)
    {
        List<PeriodeTarifaireDto> listePeriodeTarifaireDto = new ArrayList<>();
        final LocalDateTime debutTraitement = LocalDateTime.now();
        log.info("Récupération des périodes tarifaires pour la délibérante {}.", sirenDeliberante);

        // sirenDeliberante
        if (dateDebutSejour.isEqual(dateFinSejour) || dateFinSejour.isAfter(dateDebutSejour))
        {
            final List<PeriodeTarifaire> listPeriodeTarifaire =
                periodeTarifaireRepository.findAllBySirenAndDatesSejour(sirenDeliberante, dateDebutSejour, dateFinSejour);
            log.info("Retour appel repository périodes tarifaires pour la délibérante {}. Durée de traitement : {}", sirenDeliberante,
                Duration.between(debutTraitement, LocalDateTime.now()));

            listePeriodeTarifaireDto = listPeriodeTarifaire.stream()
                .map(model -> PeriodeTarifaireDto.builder()
                    .dateDebut(model.getDateDebut())
                    .dateFin(model.getDateFin())
                    .siren(model.getSiren())
                    .tarifPourcentageCategorie19(model.getTarifPourcentageCategorie19())
                    .tarifsFixesCategories(
                        Arrays.stream(model.getTarifsFixesCategories().split(Constantes.TARIF_SEPARATOR)).map(ParserUtils::parseDecimal)
                            .collect(Collectors.toList()))
                    .build())
                .collect(Collectors.toList());
        }
        log.info("Fin récupération des périodes tarifaires pour la délibérante {}. Durée de traitement : {}", sirenDeliberante,
            Duration.between(debutTraitement, LocalDateTime.now()));
        return listePeriodeTarifaireDto;
    }

}
