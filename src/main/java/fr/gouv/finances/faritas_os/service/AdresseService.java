/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.service;

import reactor.core.publisher.Mono;

public interface AdresseService
{
    Mono<String> recupererCodeInsee(String ville, String codePostal);

    Mono<String> recupererCodeInsee(String ville, String codePostal, String adresse);
}
