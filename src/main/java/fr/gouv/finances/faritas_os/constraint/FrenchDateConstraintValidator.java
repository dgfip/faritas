/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.constraint;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.utility.FaritasUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;

/**
 * Validation des dates de séjour
 */
@Slf4j
public class FrenchDateConstraintValidator implements ConstraintValidator<FrenchDateConstraint, String>
{
    @Override
    public boolean isValid(String dateStr, ConstraintValidatorContext context)
    {
        // Dans la cas ou la date est null on n'applique pas cette contrainte.
        if (dateStr == null)
        {
            return true;
        }

        if (dateStr.matches(Constantes.FRENCH_DATE_REGEXP))
        {
            try
            {
                final LocalDate dt = LocalDate.parse(dateStr, FaritasUtils.FRENCH_DATE_FORMATTER);
                final LocalDate now = LocalDate.now();
                final int moins5ans = now.getYear() - 5;
                final int plus5ans = now.getYear() + 5;
                if (!dt.isBefore(LocalDate.of(moins5ans, 1, 1)) && !dt.isAfter(LocalDate.of(plus5ans, 12, 31)))
                {
                    return true;
                }
            }
            catch (final DateTimeParseException e)
            {
                log.info("Erreur dans le format de la date {}", dateStr);
            }
        }
        return false;
    }
}
