/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.PARAMETER, ElementType.FIELD})
@Constraint(validatedBy = FrenchZipCodeConstraintValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FrenchZipCodeConstraint
{
    static String FRENCH_ZIP_CODE_IS_NOT_VALID = "French ZIP code is not valid";

    String message() default FRENCH_ZIP_CODE_IS_NOT_VALID;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}