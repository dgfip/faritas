/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.constraint;

import fr.gouv.finances.faritas_os.Constantes;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class FrenchZipCodeConstraintValidator implements ConstraintValidator<FrenchZipCodeConstraint, String>
{
    /** Code postal minimum (01xxx) */
    private static final int CP_MIN = 1000;

    /** Code postal maximum (COM = 98xxx) */
    private static final int CP_MAX = 99000;

    @Override
    public boolean isValid(String zipCode, ConstraintValidatorContext context)
    {
        if (zipCode == null || !zipCode.matches(Constantes.FRENCH_ZIP_CODE_REGEXP))
        {
            return false;
        }

        final int cp = Integer.parseInt(zipCode);
        return (cp >= CP_MIN && cp < CP_MAX);
    }
}
