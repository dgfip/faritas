/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Saisie.java
 */
@NoArgsConstructor
@Getter
@Setter
public class Saisie
{
    /** Collectivité délibérante concernée */
    private CollectiviteDeliberanteOcsitan collectiviteDeliberante;
}
