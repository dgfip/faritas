/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PeriodeTarifairePK.java - Clé de la table PeriodeTarifaire
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PeriodeTarifairePK implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String siren;

    private LocalDate dateDebut;

}
