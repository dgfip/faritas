/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * OcsitanObject.java
 */
@NoArgsConstructor
@Getter
@Setter
public class OcsitanObject
{
    /** Liste des délibérations des collectivités délibérantes */
    private List<DeliberationOcsitan> deliberations;
}
