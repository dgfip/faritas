/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CollectiviteOcsitan.java
 */
@NoArgsConstructor
@Getter
@Setter
public class CollectiviteOcsitan
{
    private String siren;

    private String nom;

    private String codePostal;
}
