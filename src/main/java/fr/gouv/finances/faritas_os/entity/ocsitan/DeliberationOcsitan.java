/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * OcsitanDeliberation.java
 */
@Getter
@Setter
@NoArgsConstructor
public class DeliberationOcsitan
{
    private Saisie saisie;

    private String millesime;

    private boolean taxeAdditionnelleDepartementale;

    private boolean taxeAdditionnelleRegionale;

    private boolean taxeAdditionnelleLGV;

    private double loyerMaximumNuit;

    private double loyerMaximumSemaine;

    private double loyerMaximumMois;

    private List<CollectiviteOcsitan> collectivites;

    private List<Periode> periodes = new ArrayList<>();

    private List<String> regimes;
}
