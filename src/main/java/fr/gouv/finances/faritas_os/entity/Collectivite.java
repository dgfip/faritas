/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Table Collectivite
 */
@Entity(name = "collectivite")
@IdClass(CompositePrimaryKey.class)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Collectivite
{
    @Id
    @Column(name = "annee", nullable = false, length = 4)
    private String annee;

    @Id
    @Column(name = "siren", nullable = false, length = 9)
    private String siren;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "code_postal", length = 5)
    private String codePostal;

    @Column(name = "code_insee", length = 10)
    private String codeInsee;

    @Column(name = "taxe_departementale_applicable")
    private Boolean taxeDepartementaleApplicable;

    @Column(name = "taxe_regionale_applicable")
    private Boolean taxeRegionaleApplicable;

    @Column(name = "taxe_grande_vitesse_applicable")
    private Boolean taxeGrandeVitesseApplicable;

    @Column(name = "taxe_idfm_applicable")
    private Boolean taxeIdfmApplicable;

    @ManyToOne
    @JoinColumn(name = "collectivite_deliberante_annee", referencedColumnName = "annee")
    @JoinColumn(name = "collectivite_deliberante_siren", referencedColumnName = "siren")
    private CollectiviteDeliberante collectiviteDeliberante;
}
