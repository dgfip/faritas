/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CompositePrimaryKey.java - Clé des tables CollectiviteDeliberante et Collectivite
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompositePrimaryKey implements Serializable
{
    private static final long serialVersionUID = 1L;

    private String siren;

    private String annee;
}
