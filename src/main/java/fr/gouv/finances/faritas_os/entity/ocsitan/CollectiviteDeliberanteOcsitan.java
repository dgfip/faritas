/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CollectiviteDeliberanteOcsitan.java
 */
@NoArgsConstructor
@Getter
@Setter
public class CollectiviteDeliberanteOcsitan
{
    private String siren;

    private String nom;

    private String codePostal;

}
