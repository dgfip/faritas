/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.utility.ParserUtils;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Table CollectiviteDeliberante
 */
@Entity(name = "collectivite_deliberante")
@IdClass(CompositePrimaryKey.class)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CollectiviteDeliberante
{
    @Id
    @Column(name = "annee", nullable = false, length = 4)
    private String annee;

    @Id
    @Column(name = "siren", nullable = false, length = 9)
    private String siren;

    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "code_postal", nullable = false, length = 5)
    private String codePostal;

    @Column(name = "regime_taxation_par_nature")
    private String regimesTaxationParNature;

    @OneToMany(mappedBy = "collectiviteDeliberante", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Collectivite> listeCollectivite;

    @OneToMany(mappedBy = "collectiviteDeliberante", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PeriodeTarifaire> listePeriodesTarifaires;

    public static CollectiviteDeliberanteDto convertToDto(CollectiviteDeliberante entity)
    {
        return CollectiviteDeliberanteDto.builder()
            .siren(entity.getSiren())
            .annee(entity.getAnnee())
            .nom(entity.getNom())
            .codePostal(entity.getCodePostal())
            .listePeriodesTarifaires(entity.getListePeriodesTarifaires().stream()
                .map(periodeModel -> PeriodeTarifaireDto.builder()
                    .siren(periodeModel.getSiren())
                    .dateDebut(periodeModel.getDateDebut())
                    .dateFin(periodeModel.getDateFin())
                    .tarifPourcentageCategorie19(periodeModel.getTarifPourcentageCategorie19())
                    .tarifsFixesCategories(
                        Arrays.stream(periodeModel.getTarifsFixesCategories().split(Constantes.TARIF_SEPARATOR))
                            .map(ParserUtils::parseDecimal)
                            .collect(Collectors.toList()))
                    .build())
                .collect(Collectors.toList()))
            .regimesTaxation(Arrays.asList(entity.getRegimesTaxationParNature().split(Constantes.TARIF_SEPARATOR)))
            .build();
    }
}
