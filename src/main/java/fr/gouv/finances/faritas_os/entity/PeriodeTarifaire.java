/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * PeriodeTarifaire.java - Table PeriodeTarifaire
 */
@Entity
@Table(name = "periode_tarifaire")
@IdClass(PeriodeTarifairePK.class)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PeriodeTarifaire
{
    @Id
    @Column(name = "siren", nullable = false, length = 9)
    private String siren;

    @Id
    @Column(name = "date_debut", nullable = false)
    private LocalDate dateDebut;

    @Column(name = "date_fin", nullable = false)
    private LocalDate dateFin;

    @Column(name = "tarif_pourcentage_categorie_19")
    private Double tarifPourcentageCategorie19;

    @Column(name = "tarifs_fixes_categories")
    private String tarifsFixesCategories;

    @ManyToOne
    @JoinColumn(name = "collectivite_deliberante_annee", referencedColumnName = "annee")
    @JoinColumn(name = "collectivite_deliberante_siren", referencedColumnName = "siren")
    private CollectiviteDeliberante collectiviteDeliberante;
}
