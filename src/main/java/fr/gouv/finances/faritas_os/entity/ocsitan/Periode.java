/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.entity.ocsitan;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Periode.java
 */
@NoArgsConstructor
@Getter
@Setter
public class Periode
{
    private String dateDebut;

    private String dateFin;

    private List<Tarif> tarifs;
}
