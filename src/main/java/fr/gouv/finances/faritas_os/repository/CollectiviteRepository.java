/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.gouv.finances.faritas_os.entity.Collectivite;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;

/**
 * CollectiviteRepository.java - Accès à la table Collectivite
 */
public interface CollectiviteRepository extends JpaRepository<Collectivite, CompositePrimaryKey>
{
    /**
     * Recherche des collectivites pour une année et un code postal
     *
     * @param annee Année du séjour
     * @param codePostal Code postal du lieu de séjour
     * @return Liste de collectivite
     */
    List<Collectivite> findAllByAnneeAndCodePostalOrderByNom(String annee, String codePostal);

    /**
     * Recherche de la collectivite pour l'année et le code INSEE
     *
     * @param annee Année du séjour
     * @param codeInsee Code INSEE du lieu de séjour
     * @return collectivite
     */
    Optional<Collectivite> findByAnneeAndCodeInsee(String annee, String codeInsee);

    /**
     * Recherche de la collectivite délibérante associée pour l'année et le SIREN de la collectivite
     *
     * @param siren SIREN de la collectivité
     * @param annee Année du séjour
     * @return SIREN de la collectivite délibérante
     */
    String findCollectiviteDeliberanteSirenByAnneeAndSiren(String siren, String annee);

    /**
     * Recherche de la collectivite pour l'année et le SIREN
     *
     * @param siren SIREN de la collectivité
     * @param annee Année du séjour
     * @return collectivité
     */
    Optional<Collectivite> findBySirenAndAnnee(String siren, String annee);
}
