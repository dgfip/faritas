/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.gouv.finances.faritas_os.entity.PeriodeTarifaire;
import fr.gouv.finances.faritas_os.entity.PeriodeTarifairePK;

/**
 * PeriodeTarifaireRepository.java - Accès à la table PeriodeTarifaire
 */
public interface PeriodeTarifaireRepository extends JpaRepository<PeriodeTarifaire, PeriodeTarifairePK>
{
    /**
     * Récupération des périodes tarifaires pour une collectivité délibérante
     *
     * @param siren SIREN de la collectivité délibérante
     * @return Liste des périodes tarifaires
     */
    List<PeriodeTarifaire> findBySiren(String siren);

    /**
     * Récupération des périodes tarifaires pour une collectivité délibérante triées par date de début de période
     *
     * @param siren SIREN de la collectivité délibérante
     * @return Liste des périodes tarifaires
     */
    List<PeriodeTarifaire> findBySirenOrderByDateDebut(String siren);

    /**
     * Récupération des périodes tarifaires applicables pour le séjour
     *
     * @param siren Siren de la collectivité déliberante
     * @param dateDebutSejour Date de début de séjour
     * @param dateFinSejour Date de fin de séjour
     * @return Liste des périodes applicables
     */
    @Query("select p from PeriodeTarifaire p "
        + "where p.siren = :siren and p.dateDebut < :dateFinSejour and p.dateFin >= :dateDebutSejour "
        + "order by p.dateDebut")
    List<PeriodeTarifaire> findAllBySirenAndDatesSejour(@Param("siren") String siren,
        @Param("dateDebutSejour") LocalDate dateDebutSejour,
        @Param("dateFinSejour") LocalDate dateFinSejour);
}
