/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.gouv.finances.faritas_os.entity.CollectiviteDeliberante;
import fr.gouv.finances.faritas_os.entity.CompositePrimaryKey;

public interface CollectiviteDeliberanteRepository extends JpaRepository<CollectiviteDeliberante, CompositePrimaryKey>
{
    List<CollectiviteDeliberante> findAllByListeCollectiviteCodePostalOrderByNom(String codePostal);

    Optional<CollectiviteDeliberante> findByListeCollectiviteCodeInseeOrderByNom(String codeInsee);

    List<CollectiviteDeliberante> findBySiren(String sirenCollectiviteDeliberante);

    List<CollectiviteDeliberante> findBySirenOrderByAnneeDesc(String siren);

}
