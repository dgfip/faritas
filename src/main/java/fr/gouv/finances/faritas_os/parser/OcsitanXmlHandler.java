/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.parser;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.gouv.finances.faritas_os.entity.ocsitan.CollectiviteDeliberanteOcsitan;
import fr.gouv.finances.faritas_os.entity.ocsitan.CollectiviteOcsitan;
import fr.gouv.finances.faritas_os.entity.ocsitan.DeliberationOcsitan;
import fr.gouv.finances.faritas_os.entity.ocsitan.OcsitanObject;
import fr.gouv.finances.faritas_os.entity.ocsitan.Periode;
import fr.gouv.finances.faritas_os.entity.ocsitan.Saisie;
import fr.gouv.finances.faritas_os.entity.ocsitan.Tarif;
import lombok.extern.slf4j.Slf4j;

/**
 * OcsitanXmlHandler - Traitement des élémnts XML du ficher OCSITAN
 */
@Slf4j
public class OcsitanXmlHandler extends DefaultHandler
{
    private OcsitanObject ocsitan;

    boolean isCollectiviteDeliberanteParsed = false;

    boolean isCollectiviteParsed = false;

    boolean isDeliberationParsed = false;

    private DeliberationOcsitan deliberation;

    private CollectiviteOcsitan collectivite;

    private CollectiviteDeliberanteOcsitan collectiviteDeliberante;

    private Periode periode;

    private Tarif tarif;

    private final StringBuilder buffer = new StringBuilder();

    public OcsitanObject getOcsitan()
    {
        return this.ocsitan;
    }

    @Override
    public void startDocument() throws SAXException
    {
        log.debug("start parsing xml file...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {

        if ("ocsitan".equalsIgnoreCase(localName))
        {
            this.ocsitan = new OcsitanObject();
        }
        else if ("deliberations".equalsIgnoreCase(localName))
        {
            this.ocsitan.setDeliberations(new ArrayList<>());
        }
        else if ("deliberation".equalsIgnoreCase(localName))
        {
            deliberation = new DeliberationOcsitan();
            deliberation.setMillesime("");
            isDeliberationParsed = true;
        }
        else if ("saisie".equalsIgnoreCase(localName))
        {
            deliberation.setSaisie(new Saisie());
        }
        else if ("collectiviteDeliberante".equalsIgnoreCase(localName))
        {
            collectiviteDeliberante = new CollectiviteDeliberanteOcsitan();
            collectiviteDeliberante.setSiren(attributes.getValue("siren"));
            deliberation.getSaisie().setCollectiviteDeliberante(collectiviteDeliberante);
            isCollectiviteDeliberanteParsed = true;
        }
        else if ("collectivites".equalsIgnoreCase(localName))
        {
            deliberation.setCollectivites(new ArrayList<>());
        }
        else if ("collectivite".equalsIgnoreCase(localName))
        {
            collectivite = new CollectiviteOcsitan();
            collectivite.setSiren(attributes.getValue("siren"));
            isCollectiviteParsed = true;
        }
        else if ("periodes".equalsIgnoreCase(localName))
        {
            deliberation.setPeriodes(new ArrayList<>());
        }
        else if ("periode".equalsIgnoreCase(localName))
        {
            periode = new Periode();
        }
        else if ("tarifs".equalsIgnoreCase(localName))
        {
            periode.setTarifs(new ArrayList<>());
        }
        else if ("tarif".equalsIgnoreCase(localName))
        {
            final String cat = attributes.getValue("categorieId");
            if (cat != null)
            {
                tarif = new Tarif();
                tarif.setCategorieId(Integer.parseInt(cat));
            }
        }
        else if ("regimes".equalsIgnoreCase(localName))
        {
            deliberation.setRegimes(new ArrayList<>());
        }

        buffer.setLength(0);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        super.endElement(uri, localName, qName);
        if (localName.equalsIgnoreCase("nom") && isCollectiviteDeliberanteParsed)
        {
            deliberation.getSaisie().getCollectiviteDeliberante().setNom(buffer.toString());
        }
        else if (localName.equalsIgnoreCase("nom") && isCollectiviteParsed)
        {
            collectivite.setNom(buffer.toString());
        }
        else if (localName.equalsIgnoreCase("codepostal") && isCollectiviteDeliberanteParsed)
        {
            deliberation.getSaisie().getCollectiviteDeliberante().setCodePostal(buffer.toString());
            isCollectiviteDeliberanteParsed = false;
        }
        else if (localName.equalsIgnoreCase("codepostal") && isCollectiviteParsed)
        {
            collectivite.setCodePostal(buffer.toString());
            isCollectiviteParsed = false;
        }
        else if ("millesime".equalsIgnoreCase(localName) && isDeliberationParsed)
        {
            deliberation.setMillesime(buffer.toString());
        }
        else if ("loyerMaximumNuit".equalsIgnoreCase(localName) && isDeliberationParsed)
        {
            deliberation.setLoyerMaximumNuit(Double.parseDouble(buffer.toString()));
        }
        else if ("loyerMaximumSemaine".equalsIgnoreCase(localName) && isDeliberationParsed)
        {
            deliberation.setLoyerMaximumSemaine(Double.parseDouble(buffer.toString()));
        }
        else if ("loyerMaximumMois".equalsIgnoreCase(localName) && isDeliberationParsed)
        {
            deliberation.setLoyerMaximumMois(Double.parseDouble(buffer.toString()));
        }
        else if (localName.equalsIgnoreCase("collectivitedeliberante"))
        {
            deliberation.getSaisie().setCollectiviteDeliberante(collectiviteDeliberante);
        }
        else if (localName.equalsIgnoreCase("deliberation"))
        {
            this.ocsitan.getDeliberations().add(deliberation);
            isDeliberationParsed = false;
        }
        else if (localName.equalsIgnoreCase("collectivite"))
        {
            deliberation.getCollectivites().add(collectivite);
        }
        else if ("taxeAdditionnelleDepartementale".equalsIgnoreCase(localName))
        {
            deliberation.setTaxeAdditionnelleDepartementale(Boolean.parseBoolean(buffer.toString()));
        }
        else if ("taxeAdditionnelleRegionale".equalsIgnoreCase(localName))
        {
            deliberation.setTaxeAdditionnelleRegionale(Boolean.parseBoolean(buffer.toString()));
        }
        else if ("taxeAdditionnelleLGV".equalsIgnoreCase(localName))
        {
            deliberation.setTaxeAdditionnelleLGV(Boolean.parseBoolean(buffer.toString()));
        }
        else if (localName.equalsIgnoreCase("periode"))
        {
            deliberation.getPeriodes().add(periode);
        }
        else if (localName.equalsIgnoreCase("dateDebut"))
        {
            periode.setDateDebut(buffer.toString());
        }
        else if (localName.equalsIgnoreCase("dateFin"))
        {
            periode.setDateFin(buffer.toString());
        }
        else if (localName.equalsIgnoreCase("tarif"))
        {
            tarif.setValue(Double.valueOf(buffer.toString()));
            periode.getTarifs().add(tarif);
        }
        else if (localName.equalsIgnoreCase("regime"))
        {
            deliberation.getRegimes().add(buffer.toString());
        }
    }

    @Override
    public void endDocument() throws SAXException
    {
        log.debug("end parsing xml file...");
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        final String value = new String(ch, start, length).trim();
        if (value.length() == 0)
        {
            return;
        }
        buffer.append(value);
    }
}
