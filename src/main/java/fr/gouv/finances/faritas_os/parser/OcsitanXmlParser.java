/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.parser;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import fr.gouv.finances.faritas_os.entity.ocsitan.OcsitanObject;

/**
 * OcsitanXmlParser - Parser XML du ficher OCSITAN
 */
public class OcsitanXmlParser
{
    private final OcsitanXmlHandler handler = new OcsitanXmlHandler();

    /**
     * Parsing du fichier OCSITAN
     *
     * @param in Fichier OCSITAN
     * @return Objet OCSITAN
     * @throws SAXException Problème de parsing
     * @throws IOException Problème de lecture du fichier
     * @throws ParserConfigurationException Problème de configuration du parser
     */
    public OcsitanObject parseXml(InputStream in) throws SAXException, IOException, ParserConfigurationException
    {
        final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setNamespaceAware(true);
        final SAXParser parser = parserFactory.newSAXParser();
        final XMLReader reader = parser.getXMLReader();
        reader.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        reader.setContentHandler(this.handler);
        final InputSource source = new InputSource(in);
        reader.parse(source);
        return handler.getOcsitan();
    }
}
