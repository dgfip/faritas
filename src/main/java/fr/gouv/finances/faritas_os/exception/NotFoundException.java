/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

/**
 * NonFoundException.java - Exception en cas de donnée inconnue
 */
public class NotFoundException extends Exception
{
    /**
     * serialVersionUID - long
     */
    private static final long serialVersionUID = -5354328668597019576L;

    public NotFoundException(String message)
    {
        super(message);
    }

    public NotFoundException(String message, Exception e)
    {
        super(message, e);
    }
}
