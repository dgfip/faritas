/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

/**
 * ReadOcsitanException.java - Exception lors du traitement OCSITAN
 */
public class ReadOcsitanException extends Exception
{
    /**
     * serialVersionUID - long, DOCUMENTEZ_MOI
     */
    private static final long serialVersionUID = -5354328668597019576L;

    public ReadOcsitanException(String message)
    {
        super(message);
    }

    public ReadOcsitanException(String message, Exception e)
    {
        super(message, e);
    }
}
