/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class InvalidSejourException extends WebApplicationException
{
    /**
     * serialVersionUID - long, DOCUMENTEZ_MOI
     */
    private static final long serialVersionUID = -320962104581166171L;

    public InvalidSejourException()
    {
        super("Données du séjour incorrectes", Response.Status.NOT_ACCEPTABLE);
    }

    public InvalidSejourException(String message)
    {
        super(message, Response.Status.NOT_ACCEPTABLE);
    }

}
