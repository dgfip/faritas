/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

/**
 * CollectiviteNonTrouveeException.java - Exception en cas de collectivité inconnue
 */
public class CollectiviteNonTrouveeException extends NotFoundException
{
    /**
     * serialVersionUID - long
     */
    private static final long serialVersionUID = 3013441783581310571L;

    public CollectiviteNonTrouveeException()
    {
        super("Aucune collectivité n'a été trouvée.");
    }
}
