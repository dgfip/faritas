/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class SegmentationFallbackException extends WebApplicationException
{
    /**
     * serialVersionUID - long
     */
    private static final long serialVersionUID = -1214716055911210094L;

    public SegmentationFallbackException()
    {
        super("Le fallback de la récupération de la collectivite délibérante a échoué", Response.Status.NOT_ACCEPTABLE);
    }
}
