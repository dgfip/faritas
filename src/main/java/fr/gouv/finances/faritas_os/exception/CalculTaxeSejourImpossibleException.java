/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.exception;

import java.util.List;

import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public class CalculTaxeSejourImpossibleException extends WebApplicationException
{
    /**
     * serialVersionUID - long, DOCUMENTEZ_MOI
     */
    private static final long serialVersionUID = -320962104581166171L;

    private final List<AnomalieDto> anomalies;

    public CalculTaxeSejourImpossibleException(List<AnomalieDto> anomalies)
    {
        super("Calcul de la taxe de séjour impossible", Response.Status.NOT_ACCEPTABLE);
        this.anomalies = anomalies;
    }

    public List<AnomalieDto> getAnomalies()
    {
        return anomalies;
    }
}
