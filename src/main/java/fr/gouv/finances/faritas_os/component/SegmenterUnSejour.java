/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import static java.lang.Math.abs;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Predicate;

import org.springframework.stereotype.Component;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import fr.gouv.finances.faritas_os.dto.AnomalieValideEnum;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.Commune;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.dto.SejourDeclare;
import fr.gouv.finances.faritas_os.exception.CalculTaxeSejourImpossibleException;
import fr.gouv.finances.faritas_os.exception.CollectiviteNonTrouveeException;
import fr.gouv.finances.faritas_os.exception.SegmentationFallbackException;
import fr.gouv.finances.faritas_os.service.AdresseService;
import fr.gouv.finances.faritas_os.service.CollectiviteDeliberanteService;
import fr.gouv.finances.faritas_os.service.CollectiviteService;
import fr.gouv.finances.faritas_os.utility.FaritasUtils;
import fr.gouv.finances.faritas_os.utility.ParserUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * SegmenterUnSejour.java - Affectation d'un séjour à une collectivité
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class SegmenterUnSejour
{
    private final CollectiviteService collectiviteService;

    private final CollectiviteDeliberanteService collectiviteDeliberanteService;

    private final AdresseService adresseService;

    private final MoteurDeCalculDeLaTaxeDeSejour moteurDeCalculDeLaTaxeDeSejour;

    /**
     * Transformation d'une ligne de déclaration brute en déclaration segmentée
     *
     * @param periode Période de déclaration
     * @param sejour Ligne brute
     * @return {@link SejourAnalyse}
     */
    public SejourAnalyse traitementSejourDeclare(SejourDeclare sejour)
    {
        log.info("Début traitement du séjour");

        final SejourAnalyse sejourAnalyse = segmenterAPartirDe(sejour);

        // handle siren rattachement
        final String sirenCollectiviteDeliberante = sejourAnalyse.getSirenCollectiviteDeliberante();

        if (sirenCollectiviteDeliberante != null && !sirenCollectiviteDeliberante.isEmpty())
        {
            // chercher ce siren dans toutes les bases d ocsitan
            final Boolean deliberanteFound = collectiviteDeliberanteService.isDeliberanteFound(sirenCollectiviteDeliberante);
            if (Boolean.TRUE.equals(deliberanteFound))
            {
                sejourAnalyse.setSirenRattachement(sirenCollectiviteDeliberante);
            }
            else
            {
                sejourAnalyse.setSirenRattachement(sejourAnalyse.getCollectiviteDeliberanteSiren());
            }

        }
        else
        {
            sejourAnalyse.setSirenRattachement(sejourAnalyse.getCollectiviteDeliberanteSiren());
        }

        log.info("Fin traitement du séjour");
        return sejourAnalyse;
    }

    /**
     * Recherche de l'année de segmentation en fonction de la date de fin de séjour
     *
     * @param sejour Un séjour brut
     * @return L'année à utiliser pour les recherches de délibération
     */
    private String getAnneeDeSegmentation(SejourDeclare sejour)
    {
        final LocalDate dateFinSejour = LocalDate.parse(sejour.getDateFinSejour(), FaritasUtils.FRENCH_DATE_FORMATTER);

        // La date de fin de séjour est présente et exploitable
        if (dateFinSejour != null)
        {
            // Si la date de fin de séjour se termine au 1er janvier, on prend l'année précédente comme référence
            if (dateFinSejour.isEqual(LocalDate.of(dateFinSejour.getYear(), 1, 1)))
            {
                return Integer.toString(dateFinSejour.getYear() - 1);
            }
            return Integer.toString(dateFinSejour.getYear());
        }

        return null;
    }

    private SejourAnalyse segmenterAPartirDe(SejourDeclare sejour)
    {
        log.debug("Début segmentation du séjour");
        final LocalDateTime debutTraitement = LocalDateTime.now();
        SejourAnalyse sejourAnalyse;

        // always add filter by annee
        final String anneeDeSegmentation = getAnneeDeSegmentation(sejour);

        try
        {
            // Recherche des collectivités dans les collectivités de l'année qui ont le même code postal.
            final List<CollectiviteDto> listeDeCollectivites = this.collectiviteService
                .listerParAnneeEtCodePostal(anneeDeSegmentation, sejour.getCodePostal());

            // Recherche de la première collectivité avec le code postal et un nom similaire
            final var collectivite = recupererCollectiviteCorrespondantASejourDeclare(sejour, listeDeCollectivites);

            sejourAnalyse = segmenterSejour(sejour, collectivite, new ArrayList<>());
        }
        catch (final CollectiviteNonTrouveeException error)
        {
            log.info("Collectivité non trouvée pour le code postal {} et l'année {}. Appel segmentation de secours.",
                sejour.getCodePostal(), anneeDeSegmentation);
            final Callable<CollectiviteDto> segmentationDeSecours = getSegmentationDeSecours(sejour, anneeDeSegmentation);
            sejourAnalyse = segmenterGraceALaSegmentationDeSecours(sejour, segmentationDeSecours);
        }

        log.debug("Fin segmentation du séjour. Durée de traitement : {}.",
            Duration.between(debutTraitement, LocalDateTime.now()).toString());
        return sejourAnalyse;
    }

    /**
     * Analyse et attribution du séjour à une collectivité
     *
     * @param sejour Séjour
     * @param collectivite Collectivité
     * @param listDAnomalies Anomalies détectées
     * @return Séjour attribué
     */
    public SejourAnalyse segmenterSejour(SejourDeclare sejour,
        CollectiviteDto collectivite, List<AnomalieDto> listDAnomalies)
    {
        log.debug("Début Création du séjour analysé.");
        final LocalDateTime debutTraitement = LocalDateTime.now();

        final SejourAnalyse sejourAnalyse = creerSejourSegmenteAPartirSejourBrut(sejour, collectivite, listDAnomalies);

        calculSegmentation(collectivite, sejourAnalyse);

        log.debug("Fin Création du séjour analysé. Durée de traitement {}.",
            Duration.between(debutTraitement, LocalDateTime.now()).toString());
        return sejourAnalyse;
    }

    private SejourAnalyse creerSejourSegmenteAPartirSejourBrut(SejourDeclare sejour,
        CollectiviteDto collectivite, List<AnomalieDto> listDAnomalies)
    {
        return SejourAnalyse.builder()
            .dateDebutSejour(ParserUtils.parseValue(ParserUtils::parseFrenchDate, sejour.getDateDebutSejour()))
            .dateFinSejour(ParserUtils.parseValue(ParserUtils::parseFrenchDate, sejour.getDateFinSejour()))
            .datePerception(ParserUtils.parseValue(ParserUtils::parseFrenchDate, sejour.getDatePerception()))
            .ville(sejour.getVille())
            .adresse(sejour.getAdresse())
            .codePostal(sejour.getCodePostal())
            .codeInsee(collectivite.getCodeInsee())
            .sirenCommune(sejour.getSirenCommune())
            .sirenCollectiviteDeliberante(sejour.getSirenCollectiviteDeliberante())
            .natureLogement(sejour.getNatureLogement())
            .categorieLogement(sejour.getCategorieLogement())
            .nombreNuits(sejour.getNombreNuits())
            .nombreVoyageurs(sejour.getNombreVoyageurs())
            .nombreVoyageursExoneresMineurs(sejour.getNombreVoyageursExoneresMineurs())
            .nombreVoyageursExoneresUrgence(sejour.getNombreVoyageursExoneresUrgence())
            .nombreVoyageursExoneresSocial(sejour.getNombreVoyageursExoneresSocial())
            .nombreVoyageursExoneresSaisonniers(sejour.getNombreVoyageursExoneresSaisonniers())
            .nombreVoyageursExoneresAutres(sejour.getNombreVoyageursExoneresAutres())
            .prixParNuit(sejour.getPrixParNuit())
            .montantTotalTaxeSejour(sejour.getMontantTotalTaxeSejour())
            .prixParNuitee(deduirePrixParNuitee(sejour))
            .nombreNuitees(deduireNombreDeNuitees(sejour))
            .nombreNuiteesAssujetties(deduireNombreNuiteesAssujetties(sejour))
            .collectiviteDeliberanteSiren(collectivite.getCollectiviteDeliberante().getSiren())
            .collectiviteDeliberanteAnnee(collectivite.getCollectiviteDeliberante().getAnnee())
            .collectiviteDeliberanteNom(collectivite.getCollectiviteDeliberante().getNom())
            .collectiviteDeliberanteCodePostal(collectivite.getCollectiviteDeliberante().getCodePostal())
            .listDAnomalies(listDAnomalies)
            .build();
    }

    private void calculSegmentation(CollectiviteDto collectivite, SejourAnalyse sejourAnalyse)
    {
        log.debug("Début calcul segmentation du sejour.");
        final LocalDateTime debutTraitement = LocalDateTime.now();
        try
        {
            final Double taxeDeSejourCalculee = moteurDeCalculDeLaTaxeDeSejour.calculer(sejourAnalyse,
                collectivite);
            sejourAnalyse.setMontantTaxeDeSejourCalculee(taxeDeSejourCalculee);
            // ajouter le tarif par nuitee attendu
            // sejourAnalyse
            // .setTarifParNuiteeAttendu(MoteurDeCalculDeLaTaxeDeSejour.calculerTarifAUtiliser(sejourAnalyse,
            // collectivite));

            log.debug("Ajout des anomalies pour le séjour.");
            if (isTaxeDeSejourCalculeeTropDifferenteDeLaTaxeDeSejourDeclaree(sejourAnalyse))
            {
                log.debug("Ajout Anomalie TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE pour le séjour");
                sejourAnalyse.getListDAnomalies().add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE));
            }
            if (isTaxeDeSejourCollecteePourRegimeForfaitaire(sejourAnalyse))
            {
                log.debug("Ajout Anomalie REGIME_FORFAITAIRE_APPLICABLE pour le séjour.");
                sejourAnalyse.getListDAnomalies().add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.REGIME_FORFAITAIRE_APPLICABLE));
            }
            if (sejourAnalyse.getCategorieLogement() != null
                && sejourAnalyse.getCategorieLogement() == Constantes.CATEGORIE_19
                && sejourAnalyse.getPrixParNuitee() == null)
            {
                log.debug("Ajout Anomalie CATEGORIE_19_ET_PRIX_MANQUANT pour le séjour.");
                sejourAnalyse.getListDAnomalies().add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.CATEGORIE_19_ET_PRIX_MANQUANT));
            }
            if (isSirenDeliberanteSimilarToSirenCalculated(sejourAnalyse))
            {
                log.debug("Ajout Anomalie RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE pour le séjour.");
                sejourAnalyse.getListDAnomalies().add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.RATTACHEMENT_INCERTAIN_A_CETTE_DELIBERANTE));
            }
            if (!isDureeSejourEgalNombreDeNuits(sejourAnalyse))
            {
                log.debug("Ajout Anomalie NOMBRE_DE_NUITS_INCOHERENT pour le séjour.");
                sejourAnalyse.getListDAnomalies().add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.NOMBRE_DE_NUITS_INCOHERENT));
            }
            log.debug("Fin ajout des anomalies pour le séjour. Temps de traitement: {}.",
                Duration.between(debutTraitement, LocalDateTime.now()).toString());
        }
        catch (final CalculTaxeSejourImpossibleException exception)
        {
            log.warn("Exception lors de l'ajout des anomalies", exception);
            sejourAnalyse.getListDAnomalies().addAll(exception.getAnomalies());
        }
        log.debug("Fin calcul segmentation du séjour. Temps de traitement: {}.",
            Duration.between(debutTraitement, LocalDateTime.now()).toString());
    }

    private Integer deduireNombreDeNuitees(SejourDeclare sejour)
    {
        if (sejour.getNombreNuits() != null && sejour.getNombreVoyageurs() != null)
        {
            return sejour.getNombreNuits() * sejour.getNombreVoyageurs();
        }
        return null;
    }

    /**
     * Calcul du nombre de nuitées assujetties à la TS
     *
     * @param sejour Séjour
     * @return Nombre de nuitées
     */
    public Integer deduireNombreNuiteesAssujetties(SejourDeclare sejour)
    {
        if (sejour.getNombreNuits() == null || sejour.getNombreVoyageurs() == null)
        {
            return null;

        }
        final int nbNuits = sejour.getNombreNuits();
        final int nbVoyageurs = sejour.getNombreVoyageurs();

        int nbMineur = 0;
        if (sejour.getNombreVoyageursExoneresMineurs() != null)
        {
            nbMineur = sejour.getNombreVoyageursExoneresMineurs();
        }
        int nbUrgence = 0;
        if (sejour.getNombreVoyageursExoneresUrgence() != null)
        {
            nbUrgence = sejour.getNombreVoyageursExoneresUrgence();
        }
        int nbSocial = 0;
        if (sejour.getNombreVoyageursExoneresSocial() != null)
        {
            nbSocial = sejour.getNombreVoyageursExoneresSocial();
        }
        int nbSaisonnier = 0;
        if (sejour.getNombreVoyageursExoneresSaisonniers() != null)
        {
            nbSaisonnier = sejour.getNombreVoyageursExoneresSaisonniers();
        }
        int nbAutres = 0;
        if (sejour.getNombreVoyageursExoneresAutres() != null)
        {
            nbAutres = sejour.getNombreVoyageursExoneresAutres();
        }

        final int nombreNuiteesAssujetties =
            (nbNuits * nbVoyageurs) - (nbNuits * (nbMineur + nbUrgence + nbSocial + nbSaisonnier + nbAutres));

        if (nombreNuiteesAssujetties < 0)
        {
            return 0;
        }
        return nombreNuiteesAssujetties;
    }

    private Double deduirePrixParNuitee(SejourDeclare sejour)
    {
        if (sejour.getPrixParNuit() != null && sejour.getNombreVoyageurs() != null)
        {
            return sejour.getPrixParNuit() / sejour.getNombreVoyageurs();
        }

        return null;
    }

    private boolean isSirenDeliberanteSimilarToSirenCalculated(SejourAnalyse sejourAnalyse)
    {
        return !sejourAnalyse.getSirenCollectiviteDeliberante().isEmpty()
            && sejourAnalyse.getCollectiviteDeliberanteSiren() != null
            && !sejourAnalyse.getCollectiviteDeliberanteSiren().equals(sejourAnalyse.getSirenCollectiviteDeliberante());
    }

    private boolean isDureeSejourEgalNombreDeNuits(SejourAnalyse sejour)
    {
        final var nombreNuits = Math.toIntExact(
            ChronoUnit.DAYS.between(sejour.getDateDebutSejour(), sejour.getDateFinSejour()));
        return nombreNuits == sejour.getNombreNuits();
    }

    private boolean isTaxeDeSejourCollecteePourRegimeForfaitaire(SejourAnalyse sejourAnalyse)
    {
        return sejourAnalyse.getMontantTotalTaxeSejour() > 0.0
            && sejourAnalyse.getRegime() != null
            && ("Forfaitaire".equalsIgnoreCase(sejourAnalyse.getRegime())
                || "Non rattachée à un tarif".equalsIgnoreCase(sejourAnalyse.getRegime()));
    }

    private SejourAnalyse segmenterGraceALaSegmentationDeSecours(SejourDeclare sejour,
        Callable<CollectiviteDto> segmentationFallback)
    {
        final List<AnomalieDto> anomaliesDeSegmentation = new ArrayList<>();
        CollectiviteDto collectivite;

        try
        {
            collectivite = segmentationFallback.call();
        }
        catch (final Exception e)
        {
            log.info("Collectivité {} non trouvée en secours : {}.", sejour.getSirenCommune(), e.getMessage());
            String collectiviteDeliberanteNom;

            if (sejour.getMontantTotalTaxeSejour() == null || sejour.getMontantTotalTaxeSejour() == 0.0)
            {
                collectiviteDeliberanteNom = "{{ COLLECTIVITE_NON_TROUVEE_MONTANT_NUL }}";
            }
            else
            {
                collectiviteDeliberanteNom = "{{ COLLECTIVITE_NON_TROUVEE_MONTANT_POSITIF }}";
                anomaliesDeSegmentation.add(
                    AnomalieDto.creerAnomalie(AnomalieValideEnum.AUCUN_RESULTAT_POUR_CETTE_COMMUNE_ET_CE_CODE_POSTAL));
            }
            final var collectiviteDeliberante = CollectiviteDeliberanteDto.builder()
                .siren(null)
                .annee(null)
                .nom(collectiviteDeliberanteNom)
                .codePostal("")
                .listePeriodesTarifaires(new ArrayList<>())
                .regimesTaxation(new ArrayList<>())
                .build();
            collectivite = CollectiviteDto.builder()
                .siren(null)
                .annee(null)
                .nom(null)
                .codePostal(null)
                .taxeAdditionnelleDepartementale(null)
                .taxeAdditionnelleRegionale(null)
                .collectiviteDeliberante(collectiviteDeliberante)
                .build();
        }

        return segmenterSejour(sejour, collectivite, anomaliesDeSegmentation);
    }

    private boolean isTaxeDeSejourCalculeeTropDifferenteDeLaTaxeDeSejourDeclaree(SejourAnalyse sejourAnalyse)
    {
        if (sejourAnalyse.getMontantTaxeDeSejourCalculee() == null)
        {
            return false;
        }

        try
        {
            final double differenceEntreLesDeuxTaxes = abs(sejourAnalyse.getMontantTaxeDeSejourCalculee()
                - sejourAnalyse.getMontantTotalTaxeSejour());
            return differenceEntreLesDeuxTaxes > Constantes.DIFFERENCE_AUTORISEE;
        }
        catch (final Exception e)
        {
            log.warn("Erreur lors du calcul de la différence entre taxe calculée {} et celle déclarée {} pour le séjour.",
                sejourAnalyse.getMontantTaxeDeSejourCalculee(), sejourAnalyse.getMontantTotalTaxeSejour(), e);
            return true;
        }
    }

    private CollectiviteDto recupererCollectiviteCorrespondantASejourDeclare(SejourDeclare sejour,
        List<CollectiviteDto> listeDeCollectivites)
        throws CollectiviteNonTrouveeException
    {
        if (listeDeCollectivites.isEmpty())
        {
            log.debug("Aucune collectivité le code postal {}", sejour.getCodePostal());
            throw new CollectiviteNonTrouveeException();
        }

        final var communeSejourDeclare = Commune.builder()
            .nom(FaritasUtils.nettoyer(sejour.getVille()))
            .codePostal(sejour.getCodePostal())
            .build();
        final Optional<CollectiviteDto> optionalCollectivite = listeDeCollectivites.stream()
            .filter(estEquivalentA(communeSejourDeclare)).findFirst();

        if (!optionalCollectivite.isPresent())
        {
            log.debug("Pas de collectivité trouvée pour {} - {}", sejour.getVille(), sejour.getCodePostal());
            throw new CollectiviteNonTrouveeException();
        }

        return optionalCollectivite.get();
    }

    private Predicate<CollectiviteDto> estEquivalentA(Commune communeSejourDeclare)
    {
        return collectivite -> communeSejourDeclare.estEquivalentA(collectivite.getNom(),
            collectivite.getCodePostal());
    }

    private Callable<CollectiviteDto> getSegmentationDeSecours(SejourDeclare ligneSejour, String annee)
    {
        return () -> {
            try
            {
                log.debug("Segmentation de secours. Recherche de la ville {} - {} - {}", ligneSejour.getAdresse(),
                    ligneSejour.getCodePostal(), ligneSejour.getVille());
                final var codeInsee = adresseService.recupererCodeInsee(ligneSejour.getVille(),
                    ligneSejour.getCodePostal(), ligneSejour.getAdresse()).block();
                if ("".equals(codeInsee))
                {
                    log.info("Segmentation de secours échouée (code INSEE vide) pour le séjour avec {} - {}.",
                        ligneSejour.getCodePostal(), ligneSejour.getVille());
                    throw new SegmentationFallbackException();
                }
                return collectiviteService.trouverParAnneeEtCodeInsee(annee, codeInsee);
            }
            catch (final Exception e)
            {
                log.info("Segmentation de secours échouée (exception) pour le séjour avec {} - {} : {}",
                    ligneSejour.getCodePostal(), ligneSejour.getVille(), e.getMessage());
                throw new SegmentationFallbackException();
            }
        };
    }
}
