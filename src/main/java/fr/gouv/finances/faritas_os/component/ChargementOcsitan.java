/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.exception.ReadOcsitanException;
import fr.gouv.finances.faritas_os.service.AdresseService;
import fr.gouv.finances.faritas_os.service.CollectiviteDeliberanteService;
import fr.gouv.finances.faritas_os.service.OcsitanReaderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * ChargementOcsitan.java - Traitement d'un fichier OCSITAN pour alimenter la base de données FARITAS.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ChargementOcsitan
{
    private final OcsitanReaderService ocsitanReader;

    private final AdresseService adresseService;

    private final CollectiviteDeliberanteService collectiviteDeliberanteService;

    @Async
    public void chargerDonneesOcsitan(String anneeOcsitan, Path ocsitanPath) throws ReadOcsitanException, IOException
    {
        log.info("Début du chargement du fichier OCSITAN pour l'annéee {}", anneeOcsitan);

        final List<CollectiviteDeliberanteDto> listeDeCollectiviteDeliberrante =
            ocsitanReader.recupererCollectivitesDeliberantes(anneeOcsitan, Files.newInputStream(ocsitanPath));

        final Map<String, CollectiviteDeliberanteDto> mapDeCollectiviteDeliberante = new HashMap<>();

        listeDeCollectiviteDeliberrante.forEach(collectiviteDeliberante -> {
            collectiviteDeliberante.getListeDeCollectivites()
                .forEach(collectivite -> adresseService.recupererCodeInsee(collectivite.getNom(), collectivite.getCodePostal())
                    .subscribe(collectivite::setCodeInsee));

            if (mapDeCollectiviteDeliberante.containsKey(collectiviteDeliberante.getSiren()))
            {
                mapDeCollectiviteDeliberante.get(collectiviteDeliberante.getSiren()).getListeDeCollectivites()
                    .addAll(collectiviteDeliberante.getListeDeCollectivites());
            }
            else
            {
                mapDeCollectiviteDeliberante.put(collectiviteDeliberante.getSiren(), collectiviteDeliberante);
            }
        });

        final List<CollectiviteDeliberanteDto> listeNouvelleCollectiviteDeliberante =
            mapDeCollectiviteDeliberante.values().stream().toList();

        collectiviteDeliberanteService.enregistrer(listeNouvelleCollectiviteDeliberante);

        log.info("Fin du chargement du fichier OCSITAN pour l'annéee {}", anneeOcsitan);
    }
}
