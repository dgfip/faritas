/*
 * Copyright (c) 2024 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.faritas_os.component;

import static org.apache.commons.math3.util.Precision.round;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import fr.gouv.finances.faritas_os.dto.AnomalieValideEnum;
import fr.gouv.finances.faritas_os.dto.CollectiviteDeliberanteDto;
import fr.gouv.finances.faritas_os.dto.CollectiviteDto;
import fr.gouv.finances.faritas_os.dto.PeriodeTarifaireDto;
import fr.gouv.finances.faritas_os.dto.SejourAnalyse;
import fr.gouv.finances.faritas_os.exception.CalculTaxeSejourImpossibleException;
import fr.gouv.finances.faritas_os.service.CollectiviteService;
import fr.gouv.finances.faritas_os.service.PeriodeTarifaireService;
import fr.gouv.finances.faritas_os.utility.FaritasUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * MoteurDeCalculDeLaTaxeDeSejour.java - Vérification de la taxe de séjour
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class MoteurDeCalculDeLaTaxeDeSejour
{
    @Value("${app.taxe.departementale}")
    private Double taxeDepartementale;

    @Value("${app.taxe.regionale}")
    private Double taxeRegionale;

    @Value("${app.taxe.grandeVitesse}")
    private Double taxeGrandeVitesse;

    @Value("${app.taxe.listDepartementsLGV}")
    private String listeDepartementsLGV;

    @Value("${app.taxe.idfm}")
    private Double taxeIdfm;

    @Value("${app.taxe.listDepartementsIdfm}")
    private String listeDepartementsIdfm;

    private final CollectiviteService collectiviteService;

    private final PeriodeTarifaireService periodeTarifaireService;

    /**
     * Calcul de la taxe de séjour
     *
     * @param sejourAnalyse Donnée du séjour
     * @param collectivite Collectivité
     * @return Montant calculé
     * @throws CalculTaxeSejourImpossibleException Exception
     */
    public Double calculer(SejourAnalyse sejourAnalyse, CollectiviteDto collectivite)
        throws CalculTaxeSejourImpossibleException
    {
        log.debug("Début calculer la taxe de séjour.");
        final LocalDateTime debutTraitement = LocalDateTime.now();

        if (isUneSegmentationQuiAEchoue(collectivite.getCollectiviteDeliberante())
            || sejourAnalyse.getDateDebutSejour() == null)
        {
            log.debug("Fin calcul de la taxe de séjour : Impossible de calculer la taxe. Durée de traitement : ",
                Duration.between(debutTraitement, LocalDateTime.now()));
            return null;
        }

        verifierLesDonneesAvantLeCalcul(sejourAnalyse);

        // Récupérer la date de début de séjour
        final LocalDate dateDebutSejour = sejourAnalyse.getDateDebutSejour();

        // Récupérer la date de fin de séjour
        final LocalDate dateFinSejour = sejourAnalyse.getDateFinSejour();

        // Récupérer la liste des périodes tarifaires à utiliser en fonction des
        // collectivités délibérantes et des dates
        // du séjour
        final List<PeriodeTarifaireDto> periodesTarifaire = recupererLesPeriodesTarifaires(sejourAnalyse,
            collectivite.getSiren());

        double taxeTotale = 0.0;

        for (final PeriodeTarifaireDto periode : periodesTarifaire)
        {
            final LocalDate dateDebutPeriode = FaritasUtils.dateMaximum(dateDebutSejour, periode.getDateDebut());
            // Comme la date de fin de période est incluse, on ajoute 1 jour pour que le
            // nombre de nuits soit correct.
            final LocalDate dateFinPeriode = FaritasUtils.dateMinimum(dateFinSejour, periode.getDateFin().plusDays(1));
            final int nbNuitsPeriode = Math.toIntExact(ChronoUnit.DAYS.between(dateDebutPeriode, dateFinPeriode));
            // chercher la collectivite en utilisant -l'année de la période de taxation-:
            final CollectiviteDto collectiviteDeLaPeriode = collectiviteService
                .trouverCollectiviteParSirenEtAnnee(collectivite.getSiren(), dateDebutPeriode.getYear());
            if (collectiviteDeLaPeriode != null)
            {
                final double taxePeriode = calculateTaxe(sejourAnalyse, collectiviteDeLaPeriode, periode,
                    nbNuitsPeriode);
                taxeTotale = taxeTotale + taxePeriode;
            }
            else
            {
                // taxe reste à 0 pour ce cas
                log.debug("aucune collectivité n'est trouvée pour la date de début de période", dateDebutPeriode);
            }

        }

        log.debug("Fin calculer la taxe de séjour - montant calculé : {}. Durée de traitement : {}", taxeTotale,
            Duration.between(debutTraitement, LocalDateTime.now()));

        return taxeTotale;
    }

    private List<PeriodeTarifaireDto> recupererLesPeriodesTarifaires(SejourAnalyse sejour, String sirenCommune)
    {
        log.debug("Récupération des périodes tarifaires pour la collectivité {} et le séjour entre {} et {}.",
            sirenCommune,
            sejour.getDateDebutSejour(), sejour.getDateFinSejour());
        final LocalDateTime debutTraitement = LocalDateTime.now();

        List<PeriodeTarifaireDto> periodesTarifaires = new ArrayList<>();

        final int yearDebutSejour = sejour.getDateDebutSejour().getYear();
        final int yearFinDeSejour = sejour.getDateFinSejour().getYear();

        final String sirenDeliberanteDebut = collectiviteService.getDeliberanteFromSirenCommuneAndAnnee(sirenCommune,
            yearDebutSejour);

        // Si le séjour est à cheval sur 2 années, il peut y avoir un changement de
        // collectivité délibérante
        if (yearFinDeSejour != yearDebutSejour
            && sejour.getDateFinSejour().isAfter(LocalDate.of(yearFinDeSejour, 1, 1)))
        {
            log.debug("Séjour sur deux années {} - {}.", yearDebutSejour, yearFinDeSejour);
            final String sirenDeliberanteFin = collectiviteService.getDeliberanteFromSirenCommuneAndAnnee(sirenCommune,
                yearFinDeSejour);

            if (sirenDeliberanteDebut == null || sirenDeliberanteFin == null)
            {
                log.warn(
                    "Impossible de calculer la taxe de séjour pour le séjour du  {} au {} dans la collectivité {}. "
                        + "Collectivite non trouvée pour le séjour.",
                    sejour.getDateDebutSejour(), sejour.getDateFinSejour(), sirenCommune);
                return periodesTarifaires;
            }

            // On a un changement de délibérante entre le début et la fin du séjour
            if (!sirenDeliberanteDebut.equals(sirenDeliberanteFin))
            {
                log.debug("Séjour sur deux années {} - {} et deux collectivités délibérantes {} - {}.", yearDebutSejour,
                    yearFinDeSejour,
                    sirenDeliberanteDebut, sirenDeliberanteFin);
                // On récupère les périodes tarifaires pour la première délibérante entre le
                // début du séjour et la fin
                // de l'année
                final List<PeriodeTarifaireDto> periodesDeliberante1 = periodeTarifaireService
                    .getPeriodeTarifairePourDeliberanteEtDate(sirenDeliberanteDebut, sejour.getDateDebutSejour(),
                        LocalDate.of(yearDebutSejour, 12, 31).plusDays(1));
                periodesTarifaires.addAll(periodesDeliberante1);

                // On récupère les périodes tarifaires pour la seconde délibérante entre le
                // début de l'année et la fin
                // du séjour
                final List<PeriodeTarifaireDto> periodesDeliberante2 = periodeTarifaireService
                    .getPeriodeTarifairePourDeliberanteEtDate(
                        sirenDeliberanteFin, LocalDate.of(yearFinDeSejour, 1, 1), sejour.getDateFinSejour());
                periodesTarifaires.addAll(periodesDeliberante2);

                log.debug("Périodes trouvées : {}.", periodesTarifaires);
                return periodesTarifaires;
            }
        }

        // L'année n'a pas changée entre le début et la fin du séjour ou la collectivité
        // délibérante n'a pas changée
        // entre le début et la fin du séjour.
        periodesTarifaires = periodeTarifaireService.getPeriodeTarifairePourDeliberanteEtDate(sirenDeliberanteDebut,
            sejour.getDateDebutSejour(),
            sejour.getDateFinSejour());

        log.debug("Périodes trouvées : {}.", periodesTarifaires);
        log.debug(
            "Fin récupération des périodes tarifaires pour la collectivité {} et le séjour entre {} et {}. Durée de traitement : {}",
            sirenCommune, sejour.getDateDebutSejour(), sejour.getDateFinSejour(),
            Duration.between(debutTraitement, LocalDateTime.now()));

        return periodesTarifaires;
    }

    /**
     * Recherche du tarif à utiliser pour le séjour
     *
     * @param sejour Données du séjour
     * @param collectivite Collectivité du séjour
     * @return Tarif
     */
    public double calculerTarifAUtiliser(SejourAnalyse sejour, PeriodeTarifaireDto periode)
    {
        log.debug("Début calculerTarifAUtiliser pour le séjour.");

        double tarifAUtiliserDansLeCalcul = 0.0;
        Integer categorieDuLogement = sejour.getCategorieLogement();
        final Double prixParNuitee = sejour.getPrixParNuitee();

        if (categorieDuLogement == null && prixParNuitee != null && prixParNuitee > 0)
        {
            categorieDuLogement = Constantes.CATEGORIE_19;
        }

        // Pour les catégories de logement entre 11 et 18, on récupère le prix par
        // nuitée en table
        if (categorieDuLogement != null && categorieDuLogement >= Constantes.PREMIERE_CATEGORIE
            && categorieDuLogement < Constantes.CATEGORIE_19)
        {
            final int indiceTarifPourLaCategorie = getIndiceCategorie(categorieDuLogement);
            tarifAUtiliserDansLeCalcul = periode.getTarifsFixesCategories().get(indiceTarifPourLaCategorie);
        }

        // Pour la catégorie de logement 19, on calcule la taxe à partir du prix de la
        // nuitée
        if (categorieDuLogement != null && categorieDuLogement == Constantes.CATEGORIE_19 && prixParNuitee != null)
        {
            // resultat à arrondir
            final double resultat = round(prixParNuitee * periode.getTarifPourcentageCategorie19() / 100,
                Constantes.NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE);

            final Optional<Double> plafondMax = periode.getTarifsFixesCategories().stream()
                .max(Comparator.naturalOrder());

            // Contrôle du plafond
            double plafond = 0.0;
            if (plafondMax.isPresent())
            {
                plafond = plafondMax.get();
            }

            if (resultat >= plafond)
            {
                tarifAUtiliserDansLeCalcul = plafond;
            }
            else
            {
                tarifAUtiliserDansLeCalcul = resultat;
            }
        }
        log.debug("Fin calculerTarifAUtiliser pour le sejour - tarif = {}.", tarifAUtiliserDansLeCalcul);

        return tarifAUtiliserDansLeCalcul;
    }

    private double calculateTaxe(SejourAnalyse sejour,
        CollectiviteDto collectivite, PeriodeTarifaireDto periode, Integer nombreDeNuits)
    {
        log.debug("Debut calculateTaxe pour le séjour dans la collectivité {} et le nombre de nuits {}",
            collectivite.getSiren(), nombreDeNuits);

        final Double tarifAUtiliserDansLeCalcul = calculerTarifAUtiliser(sejour, periode);

        final Integer natureDuLogement = sejour.getNatureLogement();

        // vérifier régime si forfait ou reel pour la deliberante auquel ce sejour est
        // rattaché
        final CollectiviteDeliberanteDto deliberante = collectivite.getCollectiviteDeliberante();

        if (natureDuLogement != null && (!deliberante.getRegimesTaxation().isEmpty()))
        {
            final String regime = deliberante.getRegimesTaxation().get(natureDuLogement - 1);
            sejour.setRegime(regime);
            if (isRegimeForfaitaire(regime))
            {
                log.debug(
                    "Fin calculateTaxe pour le séjour dans la collectivité {} et le nombre de nuits {} : régime forfaitaire.",
                    collectivite.getSiren(), nombreDeNuits);
                return 0.0;
            }
        }

        final Integer categorieDuLogement = sejour.getCategorieLogement();
        final Integer nombreDeVoyageursNonExoneres = getNombreDeVoyageursNonExoneres(sejour);
        final Double prixParNuitee = sejour.getPrixParNuitee();

        if (prixParNuitee == null && (categorieDuLogement == null || categorieDuLogement == Constantes.CATEGORIE_19))
        {
            log.debug(
                "Fin calculateTaxe pour lséjour dans la collectivité {} et le nombre de nuits {} : Catégorie 19 - montant = {}.",
                collectivite.getSiren(), nombreDeNuits, sejour.getMontantTotalTaxeSejour());
            return sejour.getMontantTotalTaxeSejour();
        }

        // 1: Taxe communale
        final double montantTaxeCommunale = nombreDeNuits * nombreDeVoyageursNonExoneres * tarifAUtiliserDansLeCalcul;

        // 2: Taxe départementale
        final double taxeDepart = getTaxeDepartementale(collectivite);
        final double montantTaxeDepart = nombreDeNuits * nombreDeVoyageursNonExoneres
            * round(tarifAUtiliserDansLeCalcul * taxeDepart,
                Constantes.NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE);

        // 3: Taxe régionale
        final double taxeRegion = getTaxeRegionale(collectivite);
        final double montantTaxeReg = nombreDeNuits * nombreDeVoyageursNonExoneres
            * round(tarifAUtiliserDansLeCalcul * taxeRegion, Constantes.NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE);

        // 4: Taxe Grande vitesse prendre toujours l'année de la période en question
        final double taxeLGV = getTaxeGrandeVitesse(collectivite, Integer.parseInt(collectivite.getAnnee()));
        final double montantTaxeGV = nombreDeNuits * nombreDeVoyageursNonExoneres
            * round(tarifAUtiliserDansLeCalcul * taxeLGV, Constantes.NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE);

        // 5: Taxe Idfm
        final double taxeMobilite = getTaxeIdfm(collectivite, Integer.parseInt(collectivite.getAnnee()));
        final double montantTaxeIdfm = nombreDeNuits * nombreDeVoyageursNonExoneres
            * round(tarifAUtiliserDansLeCalcul * taxeMobilite, Constantes.NOMBRE_DE_CHIFFRES_APRES_LA_VIRGULE);

        final double montantTaxeDeSejour = FaritasUtils.formatTaxe(
            montantTaxeCommunale + montantTaxeDepart + montantTaxeReg + montantTaxeGV + montantTaxeIdfm);
        log.debug(
            "Fin calculateTaxe pour le séjour dans la collectivité {} et le nombre de nuits {} : Catégorie {} - montant={}.",
            collectivite.getSiren(), nombreDeNuits, categorieDuLogement, montantTaxeDeSejour);
        return montantTaxeDeSejour;
    }

    private double getTaxeIdfm(CollectiviteDto collectivite, int annee)
    {
        // la taxe Idfm sera présente dans le fichier Ocsitan à partir de l'année 2025
        // on regarde si l'alimentation a été bien effectuée manuellement en bdd (not
        // null) et est égale 1
        if (collectivite.isTaxeIdfmApplicable() != null && Boolean.TRUE.equals(collectivite.isTaxeIdfmApplicable()))
        {
            return taxeIdfm;
        }

        if (StringUtils.isNotBlank(listeDepartementsIdfm))
        {
            final List<String> listDepartementIdfm = Arrays
                .asList(listeDepartementsIdfm.split(Constantes.SEPARATEUR_VIRGULE));
            final boolean isDepartementsTaxeIdfm = listDepartementIdfm.stream()
                .anyMatch(codeDepartement -> collectivite.getCodePostal().startsWith(codeDepartement));

            // Pour le millésime 2024 et les suivants, ce champ est à 1 pour les
            // dépertements concernés ==> on applique
            // une taxe supplémentaire (IDFM)
            if (isDepartementsTaxeIdfm && (annee >= Constantes.TAXE_IDFM_ANNEE_DEBUT))
            {
                return taxeIdfm;
            }
        }

        return Constantes.TAXE_NEUTRE;
    }

    private boolean isRegimeForfaitaire(String regime)
    {
        return (Constantes.REGIME_FORFAITAIRE.equalsIgnoreCase(regime)
            || Constantes.REGIME_SANS_TARIF.equalsIgnoreCase(regime));
    }

    private boolean isUneSegmentationQuiAEchoue(CollectiviteDeliberanteDto collectiviteDeliberante)
    {
        return collectiviteDeliberante.getSiren() == null;
    }

    private void verifierLesDonneesAvantLeCalcul(SejourAnalyse sejour)
        throws CalculTaxeSejourImpossibleException
    {
        final List<AnomalieDto> anomalies = new ArrayList<>();

        if (sejour.getNombreVoyageurs() == null)
        {
            anomalies.add(AnomalieDto.creerAnomalie(AnomalieValideEnum.NOMBRE_DE_VOYAGEURS_MANQUANT));
        }

        if (!anomalies.isEmpty())
        {
            throw new CalculTaxeSejourImpossibleException(anomalies);
        }
    }

    private Integer getNombreDeVoyageursNonExoneres(SejourAnalyse sejour)
    {
        return sejour.getNombreVoyageurs()
            - FaritasUtils.cleanFromNull(sejour.getNombreVoyageursExoneresMineurs())
            - FaritasUtils.cleanFromNull(sejour.getNombreVoyageursExoneresSocial())
            - FaritasUtils.cleanFromNull(sejour.getNombreVoyageursExoneresUrgence())
            - FaritasUtils.cleanFromNull(sejour.getNombreVoyageursExoneresSaisonniers())
            - FaritasUtils.cleanFromNull(sejour.getNombreVoyageursExoneresAutres());
    }

    private int getIndiceCategorie(Integer categorie)
    {
        return categorie % Constantes.PREMIERE_CATEGORIE;
    }

    private double getTaxeDepartementale(CollectiviteDto collectivite)
    {
        if (Boolean.TRUE.equals(collectivite.isTaxeDepartementaleApplicable()))
        {
            return taxeDepartementale;
        }
        return Constantes.TAXE_NEUTRE;
    }

    private double getTaxeRegionale(CollectiviteDto collectivite)
    {
        if (Boolean.TRUE.equals(collectivite.isTaxeRegionaleApplicable()))
        {
            return taxeRegionale;
        }
        return Constantes.TAXE_NEUTRE;
    }

    private double getTaxeGrandeVitesse(CollectiviteDto collectivite, int annee)
    {
        // A partir d'Ocsitan 2024, il y a l'indicateur taxe GV
        if (Boolean.TRUE.equals(collectivite.isTaxeGrandeVitesseApplicable()))
        {
            return taxeGrandeVitesse;
        }

        if (StringUtils.isNotBlank(listeDepartementsLGV))
        {
            final List<String> listDepartementLGV = Arrays
                .asList(listeDepartementsLGV.split(Constantes.SEPARATEUR_VIRGULE));
            final boolean isDepartementsTaxeGV = listDepartementLGV.stream()
                .anyMatch(codeDepartement -> collectivite.getCodePostal().startsWith(codeDepartement));

            // A Partir de 2023 et pour les départements concernés, on applique une taxe
            // supplémentaire
            if (isDepartementsTaxeGV && (annee >= Constantes.TAXE_LGV_ANNEE_DEBUT))
            {
                return taxeGrandeVitesse;
            }
        }

        return Constantes.TAXE_NEUTRE;
    }
}
