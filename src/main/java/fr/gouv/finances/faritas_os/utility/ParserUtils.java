package fr.gouv.finances.faritas_os.utility;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.function.Function;

import fr.gouv.finances.faritas_os.Constantes;
import lombok.extern.slf4j.Slf4j;

/**
 * Parser.java - Classe utilitaire d'analyse
 */
@Slf4j
public final class ParserUtils
{
    private static final String CONVERSION_IMPOSSIBLE = "Conversion de {} impossible.";

    private ParserUtils()
    {
        // Constructeur privé
    }

    public static Double parseDecimal(String decimal)
    {
        try
        {
            return Double.parseDouble(decimal.replace(",", "."));
        }
        catch (final NumberFormatException e)
        {
            log.warn(CONVERSION_IMPOSSIBLE, decimal, e);
            return null;
        }
    }

    public static Integer parseInteger(String number)
    {
        try
        {
            return Integer.parseInt(number);
        }
        catch (final NumberFormatException e)
        {
            log.warn(CONVERSION_IMPOSSIBLE, number, e);
            return null;
        }
    }

    public static LocalDate parseFrenchDate(String date)
    {
        LocalDate result = null;
        if (date != null && !"".equals(date))
        {
            if (date.matches(Constantes.FRENCH_DATE_REGEXP))
            {
                try
                {
                    result = LocalDate.parse(date, FaritasUtils.FRENCH_DATE_FORMATTER);
                }
                catch (final DateTimeParseException e)
                {
                    log.warn(CONVERSION_IMPOSSIBLE, date, e);
                }
            }
            else if (date.matches("(\\d{4})-(\\d{2})-(\\d{2})"))
            {
                final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                try
                {
                    result = LocalDate.parse(date, formatter);
                }
                catch (final DateTimeParseException e)
                {
                    log.warn(CONVERSION_IMPOSSIBLE, date, e);
                }
            }
        }

        return result;
    }

    public static LocalDate parseFrenchLabelDate(String str)
    {
        LocalDate result = null;
        if (str != null && !"".equals(str))
        {
            final DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.FRANCE);
            try
            {
                result = LocalDate.parse(str.toLowerCase(Locale.FRENCH), formatter);
            }
            catch (final DateTimeParseException e)
            {
                log.warn(CONVERSION_IMPOSSIBLE, str, e);
            }
        }

        return result;
    }

    public static <T> T parseValue(Function<String, T> maFonction, String value)
    {
        if (value == null || value.length() == 0)
        {
            return null;
        }
        return maFonction.apply(value);
    }
}
