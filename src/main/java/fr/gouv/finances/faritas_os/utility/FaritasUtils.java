package fr.gouv.finances.faritas_os.utility;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Locale;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import fr.gouv.finances.faritas_os.Constantes;
import fr.gouv.finances.faritas_os.dto.AnomalieDto;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;

/**
 * FaritasUtils.java - Classe utilitaire
 */
@Slf4j
public final class FaritasUtils
{

    public static final String FARITAS_ESERVICE = "DECLARER TAXE DE SEJOUR";

    /**
     * supprimerPrefixeCommuneDe - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerPrefixeCommuneDe = value -> value.replaceAll("(?i)\\bcommune de\\b", "");

    /**
     * remplacerStParSaint - Nettoyage des chaines
     */
    public static final UnaryOperator<String> remplacerStParSaint = value -> value.replaceAll("(?i)\\bst\\b", "saint");

    /**
     * remplacerSteParSainte - Nettoyage des chaines
     */
    public static final UnaryOperator<String> remplacerSteParSainte = value -> value.replaceAll("(?i)\\bste\\b", "sainte");

    /**
     * supprimerLesApostrophes - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerLesApostrophes = value -> value.replace("'", "");

    /**
     * supprimerLesEspaces - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerLesEspaces = value -> value.replace(" ", "");

    /**
     * supprimerLesTirets - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerLesTirets = value -> value.replace("-", "");

    /**
     * supprimerLesPoints - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerLesPoints = value -> value.replace(".", "");

    /**
     * supprimerLesAccents - Nettoyage des chaines
     */
    public static final UnaryOperator<String> supprimerLesAccents = FaritasUtils::stripAccents;

    /**
     * Valeur de profil applicatif collectivite
     */
    public static final String PROFIL_FARITAS_COLLECTIVITE = "FARITAS;COLLECTIVITE";

    /**
     * Valeur de profil applicatif plateforme
     */

    public static final String PROFIL_FARITAS_PLATEFORME = "FARITAS;PLATEFORME";

    /**
     * Valeur de nom de profil applicatif collectivite
     */
    public static final String NOM_PROFIL_COLLECTIVITE = "COLLECTIVITE";

    /**
     *
     */
    public static final Pattern SIREN_PATTERN = Pattern.compile(Constantes.SIREN_REGEXP);

    public static final DateTimeFormatter FRENCH_DATE_FORMATTER = DateTimeFormatter
        .ofPattern(Constantes.FRENCH_DATE_PATTERN, Locale.FRANCE)
        .withResolverStyle(ResolverStyle.STRICT);

    /**
     * Constructeur de la classe FaritasUtils.java
     */
    private FaritasUtils()
    {
        // Constructeur privé
    }

    /**
     * Formattage du montant
     *
     * @param obj Montant
     * @return Chaine montant
     */
    public static String formatPrice(Double obj)
    {
        if (obj != null)
        {
            return obj.toString().replace(".", ",");
        }
        return "";
    }

    /**
     * Formattage de ladate
     *
     * @param dateVar Date
     * @return chaine date
     */
    public static String formatDate(LocalDate dateVar)
    {
        var result = "";
        if (dateVar != null)
        {
            final var formatter = FRENCH_DATE_FORMATTER;
            // format date variable
            result = dateVar.format(formatter);
        }
        return result;
    }

    /**
     * Conversion de la taxe
     *
     * @param taxe montant de la taxe
     * @return taxe modifiée
     */
    public static Double formatTaxe(Double taxe)
    {
        final var formatter = new DecimalFormat("0.00");
        final var decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        formatter.setDecimalFormatSymbols(decimalFormatSymbols);
        return Double.valueOf(formatter.format(taxe));
    }

    /**
     * Contrôle du format de date
     *
     * @param date Date
     * @return true si la date est au format JJ/MM/AAAA
     */
    public static boolean isFrenchDateFormat(String date)
    {
        if (date == null)
        {
            return false;
        }
        return date.matches("(\\d{2})/(\\d{2})/(\\d{4})");
    }

    /**
     * Nettoyage d'une chaine
     *
     * @param value Chaine de caractères
     * @return Chaine de caractères.
     */
    public static String nettoyer(String value)
    {
        return supprimerPrefixeCommuneDe
            .andThen(remplacerStParSaint)
            .andThen(remplacerSteParSainte)
            .andThen(supprimerLesApostrophes)
            .andThen(supprimerLesEspaces)
            .andThen(supprimerLesTirets)
            .andThen(supprimerLesPoints)
            .andThen(supprimerLesAccents)
            .apply(value);
    }

    /**
     * Conversion des caractères accentutées
     *
     * @param value Chaîne à convertir
     * @return Chaîne sans accents
     */
    public static String stripAccents(String value)
    {
        return Normalizer
            .normalize(value, Normalizer.Form.NFD)
            .replaceAll("[^\\p{ASCII}]", "");
    }

    /**
     * Formatage du code postal français
     *
     * @param codePostal Code Postal
     * @return Code postal sur 5 caractères.
     */
    public static String formatCodePostal(String codePostal)
    {
        if (codePostal.matches("^\\d+$"))
        {
            return String.format("%05d", Integer.parseInt(codePostal));
        }

        return codePostal;
    }

    /**
     * Formatage du message d'une Anomalie
     *
     * @param anomalieModel Anomalie de déclaration
     * @return le message en miniscule et sans underscore
     */
    public static String formatMessageAnomalie(AnomalieDto anomalie)
    {
        return anomalie.getMessage().getLabel().toLowerCase(Locale.FRANCE).replace("_", " ");
    }

    /**
     * Conversion d'un entier null en 0
     *
     * @param value Valeur
     * @return un entier ou 0 en cas de null
     */
    public static Integer cleanFromNull(Integer value)
    {
        if (value == null)
        {
            return 0;
        }

        return value;
    }

    /**
     * Formatage de l'heure de dépôt d'une déclaration
     *
     * @param dateCreation Heure de dépôt
     * @return Heure
     */
    public static String formatHeureDeDepot(LocalDateTime dateCreation)
    {
        if (dateCreation != null)
        {
            return dateCreation.format(DateTimeFormatter.ofPattern("HH:mm:ss.SS"));
        }
        return "";
    }

    /**
     * Inversion de la période de déclaration
     *
     * @param periode Période
     * @return Période inversée
     */
    public static String formatPeriode(String periode)
    {
        if (periode != null && !periode.isEmpty())
        {
            final String year = periode.split("-")[0];
            final String semester = periode.split("-")[1];
            return String.join("-", semester, year);
        }
        return "";
    }

    /**
     * verifier le format du Siren s'il est correct
     *
     * @param le siren à vérifié
     * @return boolean : si siren correspond au pattern
     * @throws SirenIncorrectException
     */
    public static void isSirenValide(String siren) throws IllegalArgumentException
    {
        if (siren == null || !SIREN_PATTERN.matcher(siren).matches())
        {
            throw new IllegalArgumentException("Le SIREN est incorrect");
        }
    }

    /**
     * Création d'une réponse HTTP Bad Request Error (400)
     *
     * @param e exception
     * @return Réponse HTTP
     */
    public static Response getBadRequestResponse(IllegalArgumentException e)
    {
        log.info(e.getMessage(), e);
        return Response
            .status(Response.Status.BAD_REQUEST)
            .entity(e.getMessage())
            .build();
    }

    /**
     * Choisir la première date par ordre chronologique
     *
     * @param date1 Une date
     * @param date2 Une autre date
     * @return La première date par ordre chronologique
     */
    public static LocalDate dateMinimum(LocalDate date1, LocalDate date2)
    {
        if (date1.isAfter(date2))
        {
            return date2;
        }
        return date1;
    }

    /**
     * Choisir la dernière date par ordre chronologique
     *
     * @param date1 Une date
     * @param date2 Une autre date
     * @return La dernière date par ordre chronologique
     */
    public static LocalDate dateMaximum(LocalDate date1, LocalDate date2)
    {
        if (date1.isBefore(date2))
        {
            return date2;
        }
        return date1;
    }
}
