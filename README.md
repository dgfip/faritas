# Faritas

## Description

Le projet FARITAS (**FA**ciliter le **R**ecouvrement de l'**I**mpôt et la **TA**xe de **S**éjour) a pour objectif d'offrir une API 
centralisée de calcul de la taxe de séjour pour les plateformes numériques de location saisonnière. 

Ce projet s'appuie sur le framework **Spring**.

Le projet est hébergé dans le GitLab de l'ADULLACT : https:/gitlab.adullact.net/dgfip/faritas. 

## Outils nécessaires 

* **GIT**
* **POSTGRESQL** en version 15.
* **JAVA** en version JDK 17
* **Eclipse**

NB: Eclipse peut être remplacé par IntelliJ ou VSCode/Codium.

## Installation du poste de développement

Dans un répertoire de votre ordinateur cloner le dépôt :

```bash
git clone https://gitlab.adullact.net/dgfip/faritas.git
```

Cela va créer le répertoire **faritas** et y copier les sources.

Danc eclipse, il faut importer le projet **faritas**.

## Exécution sur le poste de développement

Pour démarrer l'application, il faut faire un clic droit sur le projet **faritas** dans la fenêtre "Package Explorer", 
puis choisir l'entrée du menu "Run As" puis cliquer sur "Spring Boot App"

Le back est démarré avec un serveur d'application **Tomcat** embarqué et écoute sur le port 8080. 
L'URL d'accès est http://localhost:8080/faritas/api/ pour Postman ou SOAP-ui

## Utilisation

Avant de pourvoir utiliser l'application pour vérifier le montant de la taxe de séjour, il faut charger en base de données 
les délibérations des collectivités locales afin de pouvoir effectuer les calculs de taxe correctement. 

### Chargement des délibérations

Il faut récupérer sur le site de la taxe de séjour (https://www.impots.gouv.fr/taxe-de-sejour) l'ensembles des délibérations. 
Il s'agit d'un fichier XML généré une fois par an en fin d'année pour les taxes de l'année à venir.

Lien vers le fichier : https://www.impots.gouv.fr/sites/default/files/media/1_metier/2_professionnel/taxe_sejour/taxe_sejour_donnees_deliberations.zip

Le fichier doit être décompressé et transmis à l'application pour chargement. 

L'API utilisée est http://localhost:8080/faritas/api/chargement-ocsitan 

Il faut fournir l'année à laquelle correspondent les délibérations (par exemple : 2024) et le fichier à télécharger. 

Exemple de code avec la commande *curl* :

```
curl --request POST \
  --url http://localhost:8080/faritas/api/chargement-ocsitan \
  --header 'content-type: multipart/form-data' \
  --form annee=2024 \
  --form 'ocsitan=deliberations_2024.xml'
```

Le chargement peut prendre un certain temps, car les adresses des collectivités sont vérifiées vis à vis de la Base Adresse Nationale.
Il faut d'ailleurs s'assurer que l'accès à la BAN (https://adresse.data.gouv.fr/) est possible. 
Il peut être nécessaire de renseigner le **PROXY** dans le fichier application.properties : 

```
# Proxy pour les appels externes
proxy.host=proxy.example.net
proxy.port=8088
```

### Vérification de la taxe de séjour

Pour effectuer la vérification de du montant de taxe à verser pour un séjour, il faut faire un appel à l'API suivante :

http://localhost:8080/faritas/api/controle-sejour 

en fournissant la description du séjour dans un objet JSON.

Exemple d'appel en cURL :

```
curl --request POST \
  --url http://localhost:8080/faritas/api/controle-sejour \
  --data '{
  "dateDebutSejour": "05/02/2024",
  "dateFinSejour": "28/02/2024",
  "datePerception": "05/02/2024",
  "adresse": "14 rue Charles de Gaulle",
  "codePostal": "44240",
  "ville": "La Chapelle-sur-Erdre",
  "sirenCommune": "214400350",
  "sirenCollectiviteDeliberante": "244400404",
  "natureLogement": 5,
  "categorieLogement": 12,
  "nombreNuits": 5,
  "nombreVoyageurs": 3,
  "nombreVoyageursExoneresMineurs": 1,
  "prixParNuit": 90.0,
  "montantTotalTaxeSejour": 102.0
}'
```

En retour, on obtient un objet JSON qui reprend les informations fournies et les informations calculées par l'application :

```
{
  "dateDebutSejour": "05/02/2024",
  "dateFinSejour": "28/02/2024",
  "datePerception": "05/02/2024",
  "adresse": "14 rue Charles de Gaulle",
  "codePostal": "44240",
  "codeInsee": "44035",
  "ville": "La Chapelle-sur-Erdre",
  "sirenCommune": "214400350",
  "sirenCollectiviteDeliberante": "244400404",
  "natureLogement": 5,
  "categorieLogement": 12,
  "nombreNuits": 5,
  "nombreVoyageurs": 3,
  "nombreVoyageursExoneresMineurs": 1,
  "nombreVoyageursExoneresUrgence": null,
  "nombreVoyageursExoneresSocial": null,
  "nombreVoyageursExoneresSaisonniers": null,
  "nombreVoyageursExoneresAutres": null,
  "prixParNuit": 90,
  "montantTotalTaxeSejour": 102,
  "nombreNuitees": 15,
  "nombreNuiteesAssujetties": 10,
  "prixParNuitee": 30,
  "collectiviteDeliberanteSiren": "244400404",
  "collectiviteDeliberanteAnnee": "2024",
  "collectiviteDeliberanteNom": "MET NANTES METROPOLE",
  "collectiviteDeliberanteCodePostal": "44923",
  "listDAnomalies": [
    {
      "id": "79bd44de-034e-4416-9762-4b4136a418a8",
      "message": "TAXE_DE_SEJOUR_CALCULEE_DIFFERENTE_DE_CELLE_DECLAREE"
    }
  ],
  "regime": "Réel",
  "sirenRattachement": "244400404",
  "montantTaxeDeSejourCalculee": 166.98,
  "tarifParNuiteeAttendu": null
}
```
